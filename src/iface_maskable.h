/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef IFACE_MASKABLE_H
#define IFACE_MASKABLE_H

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

/* Maskable interface. */
#define VISU_TYPE_MASKABLE                (visu_maskable_get_type ())
#define VISU_MASKABLE(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_TYPE_MASKABLE, VisuMaskable))
#define VISU_IS_MASKABLE(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_TYPE_MASKABLE))
#define VISU_MASKABLE_GET_INTERFACE(inst) (G_TYPE_INSTANCE_GET_INTERFACE ((inst), VISU_TYPE_MASKABLE, VisuMaskableInterface))

typedef struct _VisuMaskableInterface VisuMaskableInterface;
typedef struct _VisuMaskable VisuMaskable; /* dummy object */

/**
 * VisuMaskable:
 *
 * Interface object.
 *
 * Since: 3.8
 */

/**
 * VisuMaskableInterface:
 * @parent: its parent.
 * @reset_visibility: a method used to reset to %TRUE all node visibility.
 *
 * The different routines common to objects implementing a #VisuMaskable interface.
 * 
 * Since: 3.8
 */
struct _VisuMaskableInterface
{
  GTypeInterface parent;

  gboolean (*reset_visibility)(VisuMaskable *maskable);
};

GType visu_maskable_get_type(void);

gboolean visu_maskable_resetVisibility(VisuMaskable *maskable);
void visu_maskable_visibilityChanged(VisuMaskable *maskable);

G_END_DECLS

#endif
