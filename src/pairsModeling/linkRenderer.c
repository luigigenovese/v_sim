/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016-2017)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016-2017)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "linkRenderer.h"

/**
 * SECTION:linkRenderer
 * @short_description: Defines a common class for #VisuPairLink renderers.
 * @Title: VisuPairLinkRenderer
 *
 * <para></para>
 */

struct _VisuPairLinkRendererPrivate
{
  gchar *id, *label, *descr;
};
  
enum {
  DIRTY_SIGNAL,
  LAST_SIGNAL
};
static guint _signals[LAST_SIGNAL] = { 0 };

enum {
  PROP_0,
  ID_PROP,
  LABEL_PROP,
  DESCR_PROP,
  N_PROPS
};
static GParamSpec *_properties[N_PROPS];

static void visu_pair_link_renderer_finalize(GObject* obj);
static void visu_pair_link_renderer_get_property(GObject* obj, guint property_id,
                                                 GValue *value, GParamSpec *pspec);
static void visu_pair_link_renderer_set_property(GObject* obj, guint property_id,
                                                 const GValue *value, GParamSpec *pspec);

/* Link_Renderer interface. */
G_DEFINE_TYPE_WITH_CODE(VisuPairLinkRenderer, visu_pair_link_renderer, VISU_TYPE_OBJECT,
                        G_ADD_PRIVATE(VisuPairLinkRenderer))

static void visu_pair_link_renderer_class_init(VisuPairLinkRendererClass *klass)
{
  G_OBJECT_CLASS(klass)->finalize = visu_pair_link_renderer_finalize;
  G_OBJECT_CLASS(klass)->get_property = visu_pair_link_renderer_get_property;
  G_OBJECT_CLASS(klass)->set_property = visu_pair_link_renderer_set_property;

  /**
   * VisuPairLinkRenderer::dirty:
   * @renderer: the object which emits the signal ;
   *
   * Gets emitted when some rendering parameter of @renderer has been changed.
   *
   * Since: 3.8
   */
  _signals[DIRTY_SIGNAL] =
    g_signal_new("dirty", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE,
                 0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0, NULL);

  /**
   * VisuPairLinkRenderer::id:
   *
   * A string defining this renderer, used for configuration files.
   *
   * Since: 3.8
   */
  _properties[ID_PROP] =
    g_param_spec_string("id", "Id", "id defining this renderer", "", G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  /**
   * VisuPairLinkRenderer::label:
   *
   * A string defining this renderer, used for UI.
   *
   * Since: 3.8
   */
  _properties[LABEL_PROP] =
    g_param_spec_string("label", "Label", "label of this renderer", "", G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  /**
   * VisuPairLinkRenderer::description:
   *
   * A string detailing this renderer, used for UI.
   *
   * Since: 3.8
   */
  _properties[DESCR_PROP] =
    g_param_spec_string("description", "Description", "description of this renderer",
                        "", G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  g_object_class_install_properties(G_OBJECT_CLASS(klass), N_PROPS, _properties);
}
static void visu_pair_link_renderer_init(VisuPairLinkRenderer *self)
{
  self->priv = visu_pair_link_renderer_get_instance_private(self);
  self->priv->id = (gchar*)0;
  self->priv->label = (gchar*)0;
  self->priv->descr = (gchar*)0;
}
static void visu_pair_link_renderer_get_property(GObject* obj, guint property_id,
                                                 GValue *value, GParamSpec *pspec)
{
  VisuPairLinkRenderer *self = VISU_PAIR_LINK_RENDERER(obj);
  DBG_fprintf(stderr, "Link Renderer: get property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case ID_PROP:
      g_value_set_string(value, self->priv->id);
      DBG_fprintf(stderr, "%s.\n", g_value_get_string(value));
      break;
    case LABEL_PROP:
      g_value_set_string(value, self->priv->label);
      DBG_fprintf(stderr, "%s.\n", g_value_get_string(value));
      break;
    case DESCR_PROP:
      g_value_set_string(value, self->priv->descr);
      DBG_fprintf(stderr, "%s.\n", g_value_get_string(value));
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_pair_link_renderer_set_property(GObject* obj, guint property_id,
                                                 const GValue *value, GParamSpec *pspec)
{
  VisuPairLinkRenderer *self = VISU_PAIR_LINK_RENDERER(obj);
  DBG_fprintf(stderr, "Link Renderer: set property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case ID_PROP:
      DBG_fprintf(stderr, "%s.\n", g_value_get_string(value));
      self->priv->id = g_value_dup_string(value);
      break;
    case LABEL_PROP:
      DBG_fprintf(stderr, "%s.\n", g_value_get_string(value));
      self->priv->label = g_value_dup_string(value);
      break;
    case DESCR_PROP:
      DBG_fprintf(stderr, "%s.\n", g_value_get_string(value));
      self->priv->descr = g_value_dup_string(value);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_pair_link_renderer_finalize(GObject *obj)
{
  VisuPairLinkRenderer *self = VISU_PAIR_LINK_RENDERER(obj);

  g_free(self->priv->id);
  g_free(self->priv->label);
  g_free(self->priv->descr);
  G_OBJECT_CLASS(visu_pair_link_renderer_parent_class)->finalize(obj);
}

/**
 * visu_pair_link_renderer_start:
 * @renderer: a #VisuPairLinkRenderer object.
 * @data: a #VisuPairLink object.
 * @ele1: a #VisuElementRenderer object.
 * @ele2: a #VisuElementRenderer object.
 *
 * Calls the start() method of @renderer class. This method is
 * actually called each time a different @data pair is drawn.
 *
 * Since: 3.8
 **/
void visu_pair_link_renderer_start(VisuPairLinkRenderer *renderer, VisuPairLink *data,
                                   VisuElementRenderer *ele1, VisuElementRenderer *ele2)
{
  VisuPairLinkRendererClass *klass;

  klass = VISU_PAIR_LINK_RENDERER_GET_CLASS(renderer);
  if (klass->start)
    klass->start(renderer, data, ele1, ele2);
}
/**
 * visu_pair_link_renderer_stop:
 * @renderer: a #VisuPairLinkRenderer object.
 * @data: a #VisuPairLink object.
 *
 * Calls the stop() method of @renderer class. It is actually called
 * each time a different @data pair is finished.
 *
 * Since: 3.8
 **/
void visu_pair_link_renderer_stop(VisuPairLinkRenderer *renderer, VisuPairLink *data)
{
  VisuPairLinkRendererClass *klass;

  klass = VISU_PAIR_LINK_RENDERER_GET_CLASS(renderer);
  if (klass->stop)
    klass->stop(renderer, data);
}
/**
 * visu_pair_link_renderer_draw:
 * @renderer: a #VisuPairLinkRenderer object.
 * @iter: a #VisuPairLinkIter object.
 *
 * Calls the draw() method of @renderer for the current pair defined
 * by @iter.
 *
 * Since: 3.8
 **/
void visu_pair_link_renderer_draw(VisuPairLinkRenderer *renderer,
                                  const VisuPairLinkIter *iter)
{
  VISU_PAIR_LINK_RENDERER_GET_CLASS(renderer)->draw(renderer, iter);
}
/**
 * visu_pair_link_renderer_setGlView:
 * @renderer: a #VisuPairLinkRenderer object.
 * @view: a #VisuGlView object.
 *
 * If @renderer class has a set_view() method, it binds @view to @renderer.
 *
 * Since: 3.8
 *
 * Returns: TRUE if value is actually cahnged.
 **/
gboolean visu_pair_link_renderer_setGlView(VisuPairLinkRenderer *renderer,
                                           VisuGlView *view)
{
  VisuPairLinkRendererClass *klass;

  klass = VISU_PAIR_LINK_RENDERER_GET_CLASS(renderer);
  if (klass->set_view)
    return klass->set_view(renderer, view);
  return FALSE;
}

/**
 * visu_pair_link_renderer_emitDirty:
 * @renderer: a #VisuPairLinkRenderer object.
 *
 * Emits the "dirty" signal of @renderer.
 *
 * Since: 3.8
 **/
void visu_pair_link_renderer_emitDirty(VisuPairLinkRenderer *renderer)
{
  g_signal_emit(renderer, _signals[DIRTY_SIGNAL], 0);
}
