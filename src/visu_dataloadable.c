/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "visu_dataloadable.h"

#include <glib/gstdio.h>
#include <sys/stat.h>

#include "visu_commandLine.h"
#include "visu_dataatomic.h"
#include "visu_dataspin.h"

/**
 * SECTION: visu_dataloadable
 * @short_description: a base class for all loadable representation of
 * #VisuData objects.
 *
 * <para>#VisuData is a memory representation of node data. This class
 * is defining common methods for all #VisuDataLoadable object that
 * provide disk method to load #VisuData objects.</para>
 */


enum {
  PROP_0,
  PROP_N_SOURCES,
  PROP_LABEL,
  PROP_LOADING,
  PROP_STATUS,
  PROP_AUTO_REFRESH,
  PROP_REFRESH_PERIOD,
  N_PROPS
};
static GParamSpec *_properties[N_PROPS];

/**
 * VisuDataLoadable:
 *
 * An opaque structure storing #VisuDataLoadable objects.
 *
 * Since: 3.8
 */
struct _VisuDataLoadablePrivate
{
  guint nFiles;

  gboolean loading;
  gchar *status;

  gboolean autoRefresh;
  guint refreshPeriod, refreshId;
  time_t *lastReadTime;
};

static void visu_data_loadable_finalize(GObject* obj);
static void visu_data_loadable_get_property(GObject* obj, guint property_id,
                                            GValue *value, GParamSpec *pspec);
static void visu_data_loadable_set_property(GObject* obj, guint property_id,
                                            const GValue *value, GParamSpec *pspec);
static gboolean _reload(VisuDataLoadable *loadable);

G_DEFINE_TYPE_WITH_CODE(VisuDataLoadable, visu_data_loadable, VISU_TYPE_DATA,
                        G_ADD_PRIVATE(VisuDataLoadable))

static void visu_data_loadable_class_init(VisuDataLoadableClass *klass)
{
  G_OBJECT_CLASS(klass)->finalize     = visu_data_loadable_finalize;
  G_OBJECT_CLASS(klass)->get_property = visu_data_loadable_get_property;
  G_OBJECT_CLASS(klass)->set_property = visu_data_loadable_set_property;

  /**
   * VisuDataLoadable::n-files:
   *
   * Number of input files.
   *
   * Since: 3.8
   */
  _properties[PROP_N_SOURCES] =
    g_param_spec_uint("n-files", "N files", "number of input files",
                      1, 10, 1, G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
  /**
   * VisuDataLoadable::label:
   *
   * A string that can be displayed representing the file names.
   *
   * Since: 3.8
   */
  _properties[PROP_LABEL] =
    g_param_spec_string("label", "Label", "representation of the filenames",
                        _("No input files"), G_PARAM_READABLE);
  /**
   * VisuDataLoadable::loading:
   *
   * TRUE during the loading of a file.
   *
   * Since: 3.8
   */
  _properties[PROP_LOADING] =
    g_param_spec_boolean("loading", "Loading", "TRUE when a file is loading",
                         FALSE, G_PARAM_READABLE);
  /**
   * VisuDataLoadable::status:
   *
   * A string describing the current status of the load process.
   *
   * Since: 3.8
   */
  _properties[PROP_STATUS] =
    g_param_spec_string("status", "Status", "loading status", "",
                       G_PARAM_READABLE);
  /**
   * VisuDataLoadable::auto-refresh:
   *
   * TRUE if any modification on input file should produce a refresh.
   *
   * Since: 3.8
   */
  _properties[PROP_AUTO_REFRESH] =
    g_param_spec_boolean("auto-refresh", "Auto refresh", "automatically reload on modification",
                         FALSE, G_PARAM_READWRITE);
  /**
   * VisuDataLoadable::refresh-period:
   *
   * Polling period for automatic reload in seconds.
   *
   * Since: 3.8
   */
  _properties[PROP_REFRESH_PERIOD] =
    g_param_spec_uint("refresh-period", "Refresh period", "Refresh period in seconds",
                      1, 3600, 1, G_PARAM_READWRITE);

  g_object_class_install_properties(G_OBJECT_CLASS(klass), N_PROPS, _properties);
}
static void visu_data_loadable_init(VisuDataLoadable *obj)
{
  obj->priv = visu_data_loadable_get_instance_private(obj);
  obj->priv->status = (gchar*)0;
  obj->priv->refreshId = 0;
  obj->priv->autoRefresh = FALSE;
  obj->priv->refreshPeriod = 1;
  obj->priv->lastReadTime = (time_t*)0;
}
static void visu_data_loadable_get_property(GObject* obj, guint property_id,
                                            GValue *value, GParamSpec *pspec)
{
  VisuDataLoadable *self = VISU_DATA_LOADABLE(obj);

  switch (property_id)
    {
    case PROP_N_SOURCES:
      g_value_set_uint(value, self->priv->nFiles);
      break;
    case PROP_LABEL:
      g_value_set_static_string(value, _("No file"));
      break;
    case PROP_LOADING:
      g_value_set_boolean(value, self->priv->loading);
      break;
    case PROP_STATUS:
      if (self->priv->loading)
        g_value_set_string(value, self->priv->status);
      else
        g_value_set_string(value, (const gchar*)0);
      break;
    case PROP_AUTO_REFRESH:
      g_value_set_boolean(value, self->priv->autoRefresh);
      break;
    case PROP_REFRESH_PERIOD:
      g_value_set_uint(value, self->priv->refreshPeriod);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_data_loadable_set_property(GObject* obj, guint property_id,
                                            const GValue *value, GParamSpec *pspec)
{
  VisuDataLoadable *self = VISU_DATA_LOADABLE(obj);

  switch (property_id)
    {
    case PROP_N_SOURCES:
      self->priv->nFiles = g_value_get_uint(value);
      self->priv->lastReadTime = g_malloc0(sizeof(time_t) * self->priv->nFiles);
      break;
    case PROP_AUTO_REFRESH:
      if (g_value_get_boolean(value) == self->priv->autoRefresh)
        return;
      self->priv->autoRefresh = g_value_get_boolean(value);
      if (self->priv->refreshId && !self->priv->autoRefresh)
        {
          g_source_remove(self->priv->refreshId);
          self->priv->refreshId = 0;
        }
      if (!self->priv->refreshId && self->priv->autoRefresh)
        self->priv->refreshId = g_timeout_add_seconds(self->priv->refreshPeriod,
                                                      (GSourceFunc)_reload, obj);
      break;
    case PROP_REFRESH_PERIOD:
      if (g_value_get_uint(value) ==  self->priv->refreshPeriod)
        return;
      self->priv->refreshPeriod = g_value_get_uint(value);
      if (self->priv->refreshId)
        {
          g_source_remove(self->priv->refreshId);
          self->priv->refreshId = 0;
        }
      if (self->priv->autoRefresh)
        self->priv->refreshId = g_timeout_add_seconds(self->priv->refreshPeriod,
                                                      (GSourceFunc)_reload, obj);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_data_loadable_finalize(GObject* obj)
{
  VisuDataLoadable *self;

  self = VISU_DATA_LOADABLE(obj);

  g_free(self->priv->status);
  if (self->priv->refreshId)
    g_source_remove(self->priv->refreshId);
  g_free(self->priv->lastReadTime);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_data_loadable_parent_class)->finalize(obj);
}

static GQuark quark = (GQuark)0;
/**
 * visu_data_loadable_getErrorQuark:
 * 
 * Internal function to handle error.
 *
 * Returns: a #GQuark for #VisuDataLoadable method errors.
 */
GQuark visu_data_loadable_getErrorQuark(void)
{
  if (!quark)
    quark = g_quark_from_static_string("visu_data_loadable");

  return quark;
}

/* void visu_data_loadable_addFilename(VisuDataLoadable *self, const gchar *filename, */
/*                                     guint fileType, VisuDataLoadable *format) */
/* { */
/*   g_return_if_fail(VISU_IS_DATA_LOADABLE(self)); */
  
/*   VISU_DATA_LOADABLE_GET_INTERFACE(self)->addFilename(self, filename, fileType, format); */
/* } */
/* const gchar* visu_data_loadable_getFilename(const VisuDataLoadable *self, */
/*                                             guint fileType, VisuDataLoadable **format) */
/* { */
/*   g_return_val_if_fail(VISU_IS_DATA_LOADABLE(self), (const gchar*)0); */

/*   return VISU_DATA_LOADABLE_GET_INTERFACE(self)->getFilename(self, fileType, format); */
/* } */

static gboolean _reload(VisuDataLoadable *loadable)
{
  struct stat statBuf;
  gboolean res;
  const gchar *file;
  guint i;
  GError *error;

  for (i = 0; i < loadable->priv->nFiles; i++)
    {
      file = VISU_DATA_LOADABLE_GET_CLASS(loadable)->getFilename(loadable, i,
                                                                 (VisuDataLoader**)0);
      g_return_val_if_fail(file, FALSE);

      if (!stat(file, &statBuf) && statBuf.st_ctime > loadable->priv->lastReadTime[i])
        {
          error = (GError*)0;
          res = visu_data_loadable_reload(loadable, (GCancellable*)0, &error);
          return loadable->priv->autoRefresh && res;
        }
    }
  return loadable->priv->autoRefresh;
}

/**
 * visu_data_loadable_checkFile:
 * @self: a #VisuDataLoadable object.
 * @fileType: a file type.
 * @error: an error location.
 *
 * Tests if the provided file for a given @fileType is a valid file.
 *
 * Since: 3.8
 *
 * Returns: TRUE if the file set for @fileType is a valid file.
 **/
gboolean visu_data_loadable_checkFile(VisuDataLoadable *self,
                                      guint fileType, GError **error)
{
  const gchar *file;
#if GLIB_MINOR_VERSION > 5
  struct stat buf;
#endif

  g_return_val_if_fail(!error || !*error, FALSE);

  file = VISU_DATA_LOADABLE_GET_CLASS(self)->getFilename(self, fileType,
                                                         (VisuDataLoader**)0);
  if (!file)
    {
      *error = g_error_new(VISU_ERROR_DATA_LOADABLE, DATA_LOADABLE_ERROR_FILE,
                           _("No filename available."));
      return FALSE;
    }
  
  if (!g_file_test(file, G_FILE_TEST_IS_REGULAR))
    {
      *error = g_error_new(VISU_ERROR_DATA_LOADABLE, DATA_LOADABLE_ERROR_FILE,
                           _("File '%s' is not a regular file or may not exist."),
                           file);
      return FALSE;
    }
#if GLIB_MINOR_VERSION > 5
  if (!g_stat(file, &buf) && buf.st_size == 0)
    {
      *error = g_error_new(VISU_ERROR_DATA_LOADABLE, DATA_LOADABLE_ERROR_FILE,
                           _("File '%s' is empty."), file);
      return FALSE;
    }
#endif
  return TRUE;
}

#define USE_TASK 0
#if GLIB_MINOR_VERSION > 35 && USE_TASK == 1
static void _load(GTask *task _U_, VisuDataLoadable *self, gpointer data, GCancellable *cancel)
{
  GError *error;

  error = (GError*)0;
  VISU_DATA_LOADABLE_GET_CLASS(self)->load(self, GPOINTER_TO_INT(data),
                                           cancel, &error);
  if (error)
    g_task_return_error(task, error);
  else
    g_task_return_boolean(task, TRUE);
}
#endif

/**
 * visu_data_loadable_load:
 * @self: a #VisuDataLoadable object.
 * @iSet: an integer.
 * @cancel: (allow-none): a cancellation object.
 * @error: an error location.
 *
 * Call the load class method of @self. Also store the last read time
 * for each file of @self.
 *
 * Since: 3.8
 *
 * Returns: TRUE if @delf is successfully loaded.
 **/
gboolean visu_data_loadable_load(VisuDataLoadable *self, guint iSet,
                                 GCancellable *cancel, GError **error)
{
  gboolean res;
  guint i;
  const gchar *file;
  struct stat statBuf;
#if GLIB_MINOR_VERSION > 35 && USE_TASK == 1
  GTask *task;
#endif

  g_return_val_if_fail(VISU_IS_DATA_LOADABLE(self), FALSE);
  g_return_val_if_fail(!self->priv->loading &&
                       VISU_DATA_LOADABLE_GET_CLASS(self)->load, FALSE);
  g_return_val_if_fail(!visu_node_array_getNNodes(VISU_NODE_ARRAY(self)), FALSE);

  self->priv->loading = TRUE;
  g_object_notify_by_pspec(G_OBJECT(self), _properties[PROP_LOADING]);

#if GLIB_MINOR_VERSION > 35 && USE_TASK == 1
  task = g_task_new(self, cancel, NULL, NULL);
  g_task_set_task_data(task, GINT_TO_POINTER(iSet), (GDestroyNotify)0);
  g_task_run_in_thread_sync(task, (GTaskThreadFunc)_load);
  res = GPOINTER_TO_INT(g_task_propagate_pointer(task, error));
  g_object_unref(task);
#else
  res = VISU_DATA_LOADABLE_GET_CLASS(self)->load(self, iSet, cancel, error);
#endif
  
  self->priv->loading = FALSE;
  g_object_notify_by_pspec(G_OBJECT(self), _properties[PROP_LOADING]);

  if (res)
    for (i = 0; i < self->priv->nFiles; i++)
      {
        file = VISU_DATA_LOADABLE_GET_CLASS(self)->getFilename(self, i,
                                                               (VisuDataLoader**)0);
        g_return_val_if_fail(file, res);

        if (!stat(file, &statBuf))
          self->priv->lastReadTime[i] = statBuf.st_ctime;
      }

  return res;
}

/**
 * visu_data_loadable_reload:
 * @self: a #VisuDataLoadable object.
 * @cancel: (allow-none): a cancellation object.
 * @error: an error location.
 *
 * A wrapper to load again the same sub set of self.
 *
 * Since: 3.8
 *
 * Returns: TRUE if reload of @self is successful.
 **/
gboolean visu_data_loadable_reload(VisuDataLoadable *self,
                                   GCancellable *cancel, GError **error)
{
  int iset;

  iset = visu_data_getISubset(VISU_DATA(self));
  visu_data_freePopulation(VISU_DATA(self));
  return visu_data_loadable_load(self, iset, cancel, error);
}

/**
 * visu_data_loadable_new_fromCLI:
 * 
 * Read the command line option and set the filenames for a new
 * #VisuData. The object is not loaded (files are not parsed), just prepared.
 *
 * Returns: (transfer full): a newly allocated #VisuData if required.
 */
VisuDataLoadable* visu_data_loadable_new_fromCLI(void)
{
  char* filename, *spin_filename;
  VisuDataLoadable *newData;

  newData = (VisuDataLoadable*)0;

  DBG_fprintf(stderr, "Visu DataLoadable: create data from command line arguments.\n");
  filename = commandLineGet_ArgFilename();
  spin_filename = commandLineGet_ArgSpinFileName();
  if (filename && !spin_filename)
    newData = VISU_DATA_LOADABLE(visu_data_atomic_new_withFile(filename));
  else if(filename && spin_filename)
    newData = VISU_DATA_LOADABLE(visu_data_spin_new_withFiles(filename, spin_filename));
  return newData;
}
