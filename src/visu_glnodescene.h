/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_GL_NODE_SCENE_H
#define VISU_GL_NODE_SCENE_H

#include <glib.h>
#include <glib-object.h>

#include "visu_dump.h"
#include "visu_extset.h"
#include "visu_dataloadable.h"
#include "iface_nodemasker.h"
#include "openGLFunctions/view.h"
#include "extensions/marks.h"
#include "extensions/pairs.h"
#include "extensions/planes.h"
#include "extensions/surfs.h"
#include "extensions/mapset.h"
#include "extensions/axes.h"
#include "extensions/box.h"
#include "extensions/box_legend.h"
#include "extensions/legend.h"
#include "extensions/fogAndBGColor.h"
#include "extensions/scale.h"
#include "extensions/forces.h"
#include "extensions/vibrations.h"
#include "extensions/geodiff.h"

#include "extraFunctions/dataFile.h"
#include "extraFunctions/fragColorizer.h"
#include "extraFunctions/mover.h"

G_BEGIN_DECLS

/***************/
/* Public part */
/***************/

/**
 * VisuGlNodeSceneColorizationPolicies:
 * @COLORIZATION_POLICY_NONE: no policy.
 * @COLORIZATION_POLICY_FROM_FILE: try to load colourisation data from
 * file matching file name.
 * @COLORIZATION_POLICY_FROM_PREVIOUS: copy previous colourisation
 * data to new #VisuData.
 *
 * The different policies to apply when a new #VisuData is associated to
 * a #VisuGlNodeScene object.
 *
 * Since: 3.8
 */
typedef enum
{
  COLORIZATION_POLICY_NONE,
  COLORIZATION_POLICY_FROM_FILE,
  COLORIZATION_POLICY_FROM_PREVIOUS,
  /*< private >*/
  COLORIZATION_N_POLICIES
} VisuGlNodeSceneColorizationPolicies;

/**
 * VISU_TYPE_GL_NODE_SCENE:
 *
 * return the type of #VisuGlNodeScene.
 */
#define VISU_TYPE_GL_NODE_SCENE	     (visu_gl_node_scene_get_type ())
/**
 * VISU_GL_NODE_SCENE:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuGlNodeScene type.
 */
#define VISU_GL_NODE_SCENE(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL_NODE_SCENE, VisuGlNodeScene))
/**
 * VISU_GL_NODE_SCENE_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuGlNodeSceneClass.
 */
#define VISU_GL_NODE_SCENE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL_NODE_SCENE, VisuGlNodeSceneClass))
/**
 * VISU_IS_GL_NODE_SCENE_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuGlNodeScene object.
 */
#define VISU_IS_GL_NODE_SCENE_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL_NODE_SCENE))
/**
 * VISU_IS_GL_NODE_SCENE_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuGlNodeSceneClass class.
 */
#define VISU_IS_GL_NODE_SCENE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL_NODE_SCENE))
/**
 * VISU_GL_NODE_SCENE_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_GL_NODE_SCENE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL_NODE_SCENE, VisuGlNodeSceneClass))

typedef struct _VisuGlNodeScenePrivate VisuGlNodeScenePrivate;

/**
 * VisuGlNodeScene:
 * 
 * Common name to refer to a #_VisuGlNodeScene.
 */
typedef struct _VisuGlNodeScene VisuGlNodeScene;
struct _VisuGlNodeScene
{
  VisuGlExtSet parent;

  VisuGlNodeScenePrivate *priv;
};

/**
 * VisuGlNodeSceneClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuGlNodeSceneClass.
 */
typedef struct _VisuGlNodeSceneClass VisuGlNodeSceneClass;
struct _VisuGlNodeSceneClass
{
  VisuGlExtSetClass parent;
};

GType visu_gl_node_scene_get_type(void);

VisuGlNodeScene* visu_gl_node_scene_new();

gboolean visu_gl_node_scene_applyCLI(VisuGlNodeScene *scene, GError **error);

gboolean visu_gl_node_scene_dump(VisuGlNodeScene *scene, VisuDump *format,
                                 const char* fileName, guint width, guint height,
                                 ToolVoidDataFunc functionWait, gpointer data,
                                 GError **error);

gboolean visu_gl_node_scene_setData(VisuGlNodeScene *scene, VisuData *data);
gboolean visu_gl_node_scene_loadData(VisuGlNodeScene *scene, VisuDataLoadable *loadable,
                                     guint iSet, GCancellable *cancel, GError **error);
VisuData* visu_gl_node_scene_getData(VisuGlNodeScene *scene);

gboolean visu_gl_node_scene_setColorization(VisuGlNodeScene *scene, VisuColorization *dt);
VisuColorization* visu_gl_node_scene_getColorization(VisuGlNodeScene *scene);
gboolean visu_gl_node_scene_setColorizationPolicy(VisuGlNodeScene *scene,
                                                  VisuGlNodeSceneColorizationPolicies policy);

VisuDataColorizerFragment* visu_gl_node_scene_getFragmentColorizer(VisuGlNodeScene *scene);

VisuGlView* visu_gl_node_scene_getGlView(VisuGlNodeScene *scene);
void visu_gl_node_scene_setGlCamera(VisuGlNodeScene *scene, VisuGlCamera *camera);

VisuGlExtNodes* visu_gl_node_scene_getNodes(VisuGlNodeScene *scene);
VisuGlExtPairs* visu_gl_node_scene_getPairs(VisuGlNodeScene *scene);
VisuGlExtAxes* visu_gl_node_scene_getAxes(VisuGlNodeScene *scene);
VisuGlExtBox* visu_gl_node_scene_getBox(VisuGlNodeScene *scene);
VisuGlExtBoxLegend* visu_gl_node_scene_getBoxLegend(VisuGlNodeScene *scene);
VisuGlExtLegend* visu_gl_node_scene_getLegend(VisuGlNodeScene *scene);
VisuGlExtBg* visu_gl_node_scene_getBgImage(VisuGlNodeScene *scene);
VisuGlExtScale* visu_gl_node_scene_getScales(VisuGlNodeScene *scene);
VisuGlExtForces* visu_gl_node_scene_getForces(VisuGlNodeScene *scene);
VisuGlExtVibrations* visu_gl_node_scene_getVibrations(VisuGlNodeScene *scene);
VisuGlExtShade* visu_gl_node_scene_getColorizationLegend(VisuGlNodeScene *scene);
VisuGlExtGeodiff* visu_gl_node_scene_getGeometryDifferences(VisuGlNodeScene *scene);

VisuGlExtPlanes* visu_gl_node_scene_addPlanes(VisuGlNodeScene *scene);
VisuGlExtSurfaces* visu_gl_node_scene_addSurfaces(VisuGlNodeScene *scene);
VisuGlExtMapSet* visu_gl_node_scene_addMaps(VisuGlNodeScene *scene, VisuGlExtShade **mapLegend);

VisuGlExtMarks* visu_gl_node_scene_getMarks(VisuGlNodeScene *scene);
gboolean visu_gl_node_scene_setMarksFromFile(VisuGlNodeScene *scene,
                                             const gchar *filename, GError **error);
gboolean visu_gl_node_scene_marksToFile(VisuGlNodeScene *scene,
                                        const gchar *filename, GError **error);
gboolean visu_gl_node_scene_getMarkActive(const VisuGlNodeScene *scene, guint nodeId);
gboolean visu_gl_node_scene_setMark(VisuGlNodeScene *scene, guint nodeId, gboolean status);
VisuDataColorizerFragment* visu_gl_node_scene_colorizeShell(VisuGlNodeScene *scene, gint nodeId);
VisuDataColorizerFragment* visu_gl_node_scene_colorizeFragments(VisuGlNodeScene *scene);
void visu_gl_node_scene_removeMarks(VisuGlNodeScene *scene);

VisuGlExtInfos* visu_gl_node_scene_getInfos(VisuGlNodeScene *scene);

gboolean visu_gl_node_scene_setDiffFromLoadable(VisuGlNodeScene *scene,
                                                VisuDataLoadable *ref, GError **error);
void visu_gl_node_scene_setDiffFromData(VisuGlNodeScene *scene, const VisuData *dataRef);
void visu_gl_node_scene_clearPaths(VisuGlNodeScene *scene);
gboolean visu_gl_node_scene_parsePathsFromXML(VisuGlNodeScene *scene,
                                              const gchar *filename, GError **error);
gboolean visu_gl_node_scene_exportPathsToXML(VisuGlNodeScene *scene,
                                             const gchar *filename, GError **error);

gboolean visu_gl_node_scene_addMasker(VisuGlNodeScene *scene, VisuNodeMasker *masker);
gboolean visu_gl_node_scene_removeMasker(VisuGlNodeScene *scene, VisuNodeMasker *masker);

gboolean visu_gl_node_scene_addMover(VisuGlNodeScene *scene, VisuNodeMover *mover);
gboolean visu_gl_node_scene_removeMover(VisuGlNodeScene *scene, VisuNodeMover *mover);

G_END_DECLS

#endif
