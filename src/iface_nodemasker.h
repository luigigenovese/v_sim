/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef IFACE_NODE_MASKER_H
#define IFACE_NODE_MASKER_H

#include <glib.h>
#include <glib-object.h>

#include "visu_nodes.h"
#include "extraFunctions/nodeProp.h"

G_BEGIN_DECLS

/* NodeMasker interface. */
#define VISU_TYPE_NODE_MASKER                (visu_node_masker_get_type ())
#define VISU_NODE_MASKER(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_TYPE_NODE_MASKER, VisuNodeMasker))
#define VISU_IS_NODE_MASKER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_TYPE_NODE_MASKER))
#define VISU_NODE_MASKER_GET_INTERFACE(inst) (G_TYPE_INSTANCE_GET_INTERFACE ((inst), VISU_TYPE_NODE_MASKER, VisuNodeMaskerInterface))

typedef struct _VisuNodeMaskerInterface VisuNodeMaskerInterface;
typedef struct _VisuNodeMasker VisuNodeMasker; /* dummy object */

/**
 * VisuNodeMasker:
 *
 * Interface object.
 *
 * Since: 3.8
 */

/**
 * VisuNodeMaskerFunc:
 * @masker: a #VisuNodeMasker object ;
 * @at: a #VisuNodeValuesIter pointer
 * @data: (closure): some data.
 *
 * Function to decide to hide a node or not.
 * 
 * Returns: TRUE to hide a node depending on values in @at.
 *
 * Since: 3.8
 */
typedef gboolean (*VisuNodeMaskerFunc)(const VisuNodeMasker *masker,
                                       const VisuNodeValuesIter *at,
                                       gpointer data);

/**
 * VisuNodeMaskerInterface:
 * @parent: its parent.
 * @set_mask_func: a method to specify a custom masking routine.
 * @apply: a method used to apply a visibility mask on a #VisuNodeArray.
 *
 * The different routines common to objects implementing a #VisuNodeMasker interface.
 * 
 * Since: 3.8
 */
struct _VisuNodeMaskerInterface
{
  GTypeInterface parent;

  gboolean (*set_mask_func)(VisuNodeMasker *masker, VisuNodeMaskerFunc func,
                            gpointer data, GDestroyNotify destroy);
  gboolean (*apply)        (const VisuNodeMasker *masker, VisuNodeArray *array);
};

GType visu_node_masker_get_type(void);

gboolean visu_node_masker_setMaskFunc(VisuNodeMasker *masker, VisuNodeMaskerFunc func,
                                      gpointer data, GDestroyNotify destroy);
void visu_node_masker_apply(const VisuNodeMasker *masker,
                            gboolean *redraw, VisuNodeArray *array);
void visu_node_masker_emitDirty(VisuNodeMasker *masker);

G_END_DECLS

#endif
