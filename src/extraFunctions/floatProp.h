/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef FARRAYPROP_H
#define FARRAYPROP_H

#include <glib.h>
#include <glib-object.h>

#include "nodeProp.h"
#include <coreTools/toolArray.h>

G_BEGIN_DECLS

/**
 * VISU_TYPE_NODE_VALUES_FARRAY:
 *
 * return the type of #VisuNodeValuesFarray.
 */
#define VISU_TYPE_NODE_VALUES_FARRAY	     (visu_node_values_farray_get_type ())
/**
 * VISU_NODE_VALUES_FARRAY:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuNodeValuesFarray type.
 */
#define VISU_NODE_VALUES_FARRAY(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_NODE_VALUES_FARRAY, VisuNodeValuesFarray))
/**
 * VISU_NODE_VALUES_FARRAY_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuNodeValuesFarrayClass.
 */
#define VISU_NODE_VALUES_FARRAY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_NODE_VALUES_FARRAY, VisuNodeValuesFarrayClass))
/**
 * VISU_IS_NODE_VALUES_FARRAY_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuNodeValuesFarray object.
 */
#define VISU_IS_NODE_VALUES_FARRAY_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_NODE_VALUES_FARRAY))
/**
 * VISU_IS_NODE_VALUES_FARRAY_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuNodeValuesFarrayClass class.
 */
#define VISU_IS_NODE_VALUES_FARRAY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_NODE_VALUES_FARRAY))
/**
 * VISU_NODE_VALUES_FARRAY_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_NODE_VALUES_FARRAY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_NODE_VALUES_FARRAY, VisuNodeValuesFarrayClass))

/**
 * VisuNodeValuesFarrayPrivate:
 * 
 * Private data for #VisuNodeValuesFarray objects.
 */
typedef struct _VisuNodeValuesFarrayPrivate VisuNodeValuesFarrayPrivate;

/**
 * VisuNodeValuesFarray:
 * 
 * Common name to refer to a #_VisuNodeValuesFarray.
 */
typedef struct _VisuNodeValuesFarray VisuNodeValuesFarray;
struct _VisuNodeValuesFarray
{
  VisuNodeValues parent;

  VisuNodeValuesFarrayPrivate *priv;
};

/**
 * VisuNodeValuesFarrayNrm2:
 * @vect: a #VisuNodeValuesFarray object.
 * @value: a value.
 *
 * Prototype of method used to compute the square norm of @value.
 *
 * Since: 3.8
 */
typedef gfloat (*VisuNodeValuesFarrayNrm2)(const VisuNodeValuesFarray *vect,
                                           const GValue *value);

/**
 * VisuNodeValuesFarrayClass:
 * @parent: private.
 * @nrm2: the function used to compute the squared norm of the float array.
 * 
 * Common name to refer to a #_VisuNodeValuesFarrayClass.
 */
typedef struct _VisuNodeValuesFarrayClass VisuNodeValuesFarrayClass;
struct _VisuNodeValuesFarrayClass
{
  VisuNodeValuesClass parent;

  VisuNodeValuesFarrayNrm2 nrm2;
};

/**
 * visu_node_values_farray_get_type:
 *
 * This method returns the type of #VisuNodeValuesFarray, use
 * VISU_TYPE_NODE_VALUES_FARRAY instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuNodeValuesFarray.
 */
GType visu_node_values_farray_get_type(void);

VisuNodeValuesFarray* visu_node_values_farray_new(VisuNodeArray *arr,
                                                  const gchar *label,
                                                  guint dimension);
VisuNodeValuesFarray* visu_node_values_farray_new_fromFile(VisuNodeArray *arr,
                                                           const gchar *label,
                                                           const gchar *filename,
                                                           GError **error);

gfloat visu_node_values_farray_min(VisuNodeValuesFarray *vect);
gfloat visu_node_values_farray_max(VisuNodeValuesFarray *vect);
gfloat visu_node_values_farray_nrm2(VisuNodeValuesFarray *vect);
gboolean visu_node_values_farray_getColumnMinMax(VisuNodeValuesFarray *vect,
                                                 float minMax[2], guint column);

const gfloat* visu_node_values_farray_getAt(VisuNodeValuesFarray *vect,
                                            const VisuNode *node);
const gfloat* visu_node_values_farray_getAtIter(const VisuNodeValuesFarray *vect,
                                                const VisuNodeValuesIter *iter);
gfloat visu_node_values_farray_getFloatAtIter(const VisuNodeValuesFarray *vect,
                                              const VisuNodeValuesIter *iter,
                                              guint column);

gboolean visu_node_values_farray_setAt(VisuNodeValuesFarray *vect,
                                       const VisuNode *node,
                                       const float *vals,
                                       guint ln);
gboolean visu_node_values_farray_setAtDbl(VisuNodeValuesFarray *vect,
                                          const VisuNode *node,
                                          const double *vals,
                                          guint ln);
gboolean visu_node_values_farray_set(VisuNodeValuesFarray *vect,
                                     const GArray *data);

void visu_node_values_farray_scale(VisuNodeValuesFarray *vect, gfloat factor);

const gchar* visu_node_values_farray_getFile(const VisuNodeValuesFarray *vect);

G_END_DECLS

#endif
