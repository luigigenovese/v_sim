/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef SHELLPROP_H
#define SHELLPROP_H

#include <glib.h>
#include <glib-object.h>

#include "fragProp.h"

G_BEGIN_DECLS

/**
 * VISU_TYPE_NODE_VALUES_SHELL:
 *
 * return the type of #VisuNodeValuesShell.
 */
#define VISU_TYPE_NODE_VALUES_SHELL	     (visu_node_values_shell_get_type ())
/**
 * VISU_NODE_VALUES_SHELL:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuNodeValuesShell type.
 */
#define VISU_NODE_VALUES_SHELL(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_NODE_VALUES_SHELL, VisuNodeValuesShell))
/**
 * VISU_NODE_VALUES_SHELL_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuNodeValuesShellClass.
 */
#define VISU_NODE_VALUES_SHELL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_NODE_VALUES_SHELL, VisuNodeValuesShellClass))
/**
 * VISU_IS_NODE_VALUES_SHELL_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuNodeValuesShell object.
 */
#define VISU_IS_NODE_VALUES_SHELL_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_NODE_VALUES_SHELL))
/**
 * VISU_IS_NODE_VALUES_SHELL_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuNodeValuesShellClass class.
 */
#define VISU_IS_NODE_VALUES_SHELL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_NODE_VALUES_SHELL))
/**
 * VISU_NODE_VALUES_SHELL_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_NODE_VALUES_SHELL_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_NODE_VALUES_SHELL, VisuNodeValuesShellClass))

/**
 * VisuNodeValuesShellPrivate:
 * 
 * Private data for #VisuNodeValuesShell objects.
 */
typedef struct _VisuNodeValuesShellPrivate VisuNodeValuesShellPrivate;

/**
 * VisuNodeValuesShell:
 * 
 * Common name to refer to a #_VisuNodeValuesShell.
 */
typedef struct _VisuNodeValuesShell VisuNodeValuesShell;
struct _VisuNodeValuesShell
{
  VisuNodeValuesFrag parent;

  VisuNodeValuesShellPrivate *priv;
};

/**
 * VisuNodeValuesShellClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuNodeValuesShellClass.
 */
typedef struct _VisuNodeValuesShellClass VisuNodeValuesShellClass;
struct _VisuNodeValuesShellClass
{
  VisuNodeValuesFragClass parent;
};

/**
 * visu_node_values_shell_get_type:
 *
 * This method returns the type of #VisuNodeValuesShell, use
 * VISU_TYPE_NODE_VALUES_SHELL instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuNodeValuesShell.
 */
GType visu_node_values_shell_get_type(void);

VisuNodeValuesShell* visu_node_values_shell_new(VisuNodeArray *arr,
                                                const gchar *label);

gboolean visu_node_values_shell_compute(VisuNodeValuesShell *shell,
                                        guint id, gfloat factor);

guint visu_node_values_shell_getLevel(VisuNodeValuesShell *shell);
gboolean visu_node_values_shell_setLevel(VisuNodeValuesShell *shell, guint level);

G_END_DECLS

#endif
