/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef COORDPROP_H
#define COORDPROP_H

#include <glib.h>
#include <glib-object.h>

#include "floatProp.h"
#include <visu_data.h>

G_BEGIN_DECLS

/**
 * VISU_TYPE_NODE_VALUES_COORD:
 *
 * return the type of #VisuNodeValuesCoord.
 */
#define VISU_TYPE_NODE_VALUES_COORD	     (visu_node_values_coord_get_type ())
/**
 * VISU_NODE_VALUES_COORD:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuNodeValuesCoord type.
 */
#define VISU_NODE_VALUES_COORD(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_NODE_VALUES_COORD, VisuNodeValuesCoord))
/**
 * VISU_NODE_VALUES_COORD_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuNodeValuesCoordClass.
 */
#define VISU_NODE_VALUES_COORD_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_NODE_VALUES_COORD, VisuNodeValuesCoordClass))
/**
 * VISU_IS_NODE_VALUES_COORD_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuNodeValuesCoord object.
 */
#define VISU_IS_NODE_VALUES_COORD_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_NODE_VALUES_COORD))
/**
 * VISU_IS_NODE_VALUES_COORD_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuNodeValuesCoordClass class.
 */
#define VISU_IS_NODE_VALUES_COORD_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_NODE_VALUES_COORD))
/**
 * VISU_NODE_VALUES_COORD_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_NODE_VALUES_COORD_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_NODE_VALUES_COORD, VisuNodeValuesCoordClass))

/**
 * VisuNodeValuesCoord:
 * 
 * Common name to refer to a #_VisuNodeValuesCoord.
 */
typedef struct _VisuNodeValuesCoord VisuNodeValuesCoord;
struct _VisuNodeValuesCoord
{
  VisuNodeValuesFarray parent;
};

/**
 * VisuNodeValuesCoordClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuNodeValuesCoordClass.
 */
typedef struct _VisuNodeValuesCoordClass VisuNodeValuesCoordClass;
struct _VisuNodeValuesCoordClass
{
  VisuNodeValuesFarrayClass parent;
};

/**
 * visu_node_values_coord_get_type:
 *
 * This method returns the type of #VisuNodeValuesCoord, use
 * VISU_TYPE_NODE_VALUES_COORD instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuNodeValuesCoord.
 */
GType visu_node_values_coord_get_type(void);

VisuNodeValuesCoord* visu_node_values_coord_new(VisuData *dataObj);

const gfloat* visu_node_values_coord_getAt(const VisuNodeValuesCoord *vect,
                                           const VisuNode *node);

gboolean visu_node_values_coord_setAt(VisuNodeValuesCoord *vect,
                                      const VisuNode *node,
                                      const gfloat xyz[3]);
gboolean visu_node_values_coord_setAtDbl(VisuNodeValuesCoord *vect,
                                         const VisuNode *node,
                                         const double dxyz[3]);

G_END_DECLS

#endif
