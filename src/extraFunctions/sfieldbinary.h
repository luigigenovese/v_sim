/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresses mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail addresses :
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef SFIELDSUM_H
#define SFIELDSUM_H

#include <glib.h>

#include <extraFunctions/scalarFields.h>

/**
 * VisuScalarFieldBinaryOpTypes:
 * @VISU_OPERATOR_ADDITION: an addition binary operation.
 * @VISU_OPERATOR_DIFFERENCE: a difference binary operation.
 * @VISU_OPERATOR_PRODUCT: a multiplication binary operation.
 * @VISU_OPERATOR_RATIO: a division binary operation.
 *
 * Defines the kind of implemented binary operation.
 *
 * Since: 3.8
 */
typedef enum
  {
    VISU_OPERATOR_ADDITION,
    VISU_OPERATOR_DIFFERENCE,
    VISU_OPERATOR_PRODUCT,
    VISU_OPERATOR_RATIO
  } VisuScalarFieldBinaryOpTypes;

/**
 * VISU_TYPE_SCALAR_FIELD_BINARY_OP:
 *
 * return the type of #VisuScalarFieldBinaryOp.
 */
#define VISU_TYPE_SCALAR_FIELD_BINARY_OP	     (visu_scalar_field_binary_op_get_type ())
/**
 * SCALAR_FIELD_BINARY_OP:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuScalarFieldBinaryOp type.
 */
#define VISU_SCALAR_FIELD_BINARY_OP(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_SCALAR_FIELD_BINARY_OP, VisuScalarFieldBinaryOp))
/**
 * VISU_SCALAR_FIELD_BINARY_OP_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuScalarFieldBinaryOpClass.
 */
#define VISU_SCALAR_FIELD_BINARY_OP_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_SCALAR_FIELD_BINARY_OP, VisuScalarFieldBinaryOpClass))
/**
 * VISU_IS_SCALAR_FIELD_BINARY_OP_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuScalarFieldBinaryOp object.
 */
#define VISU_IS_SCALAR_FIELD_BINARY_OP_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_SCALAR_FIELD_BINARY_OP))
/**
 * VISU_IS_SCALAR_FIELD_BINARY_OP_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuScalarFieldBinaryOpClass class.
 */
#define VISU_IS_SCALAR_FIELD_BINARY_OP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_SCALAR_FIELD_BINARY_OP))
/**
 * VISU_SCALAR_FIELD_BINARY_OP_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_SCALAR_FIELD_BINARY_OP_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_SCALAR_FIELD_BINARY_OP, VisuScalarFieldBinaryOpClass))

typedef struct _VisuScalarFieldBinaryOp        VisuScalarFieldBinaryOp;
typedef struct _VisuScalarFieldBinaryOpPrivate VisuScalarFieldBinaryOpPrivate;
typedef struct _VisuScalarFieldBinaryOpClass   VisuScalarFieldBinaryOpClass;

/**
 * VisuScalarFieldBinaryOp:
 *
 * An opaque structure for the scalar field.
 */
struct _VisuScalarFieldBinaryOp
{
  VisuScalarField parent;

  VisuScalarFieldBinaryOpPrivate *priv;
};

/**
 * VisuScalarFieldBinaryOpClass:
 * @parent: the parent class.
 *
 * An opaque structure for the class.
 */
struct _VisuScalarFieldBinaryOpClass
{
  VisuScalarFieldClass parent;
};

/**
 * visu_scalar_field_operator_get_type:
 *
 * This method returns the type of #VisuScalarFieldBinaryOp, use VISU_TYPE_SCALAR_FIELD_BINARY_OP instead.
 *
 * Returns: the type of #VisuScalarFieldBinaryOp.
 */
GType visu_scalar_field_binary_op_get_type(void);

VisuScalarField* visu_scalar_field_binary_op_new(VisuScalarFieldBinaryOpTypes op,
                                                 VisuScalarField *a,
                                                 VisuScalarField *b);

VisuScalarField* visu_scalar_field_binary_op_new_withLeftConst(VisuScalarFieldBinaryOpTypes op,
                                                               double lValue,
                                                               VisuScalarField *b);

VisuScalarField* visu_scalar_field_binary_op_new_withRightConst(VisuScalarFieldBinaryOpTypes op,
                                                                VisuScalarField *a,
                                                                double rValue);

gboolean visu_scalar_field_binary_op_setLeftField(VisuScalarFieldBinaryOp *op,
                                                  VisuScalarField *field);
gboolean visu_scalar_field_binary_op_setRightField(VisuScalarFieldBinaryOp *op,
                                                   VisuScalarField *field);

#endif
