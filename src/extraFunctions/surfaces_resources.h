/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef SURFACES_RESOURCES_H
#define SURFACES_RESOURCES_H

#include <glib.h>
#include <glib-object.h>

#include <visu_tools.h>
#include <coreTools/toolColor.h>

G_BEGIN_DECLS

/**
 * VISU_TYPE_SURFACE_RESOURCE:
 *
 * return the type of #VisuSurfaceResource.
 */
#define VISU_TYPE_SURFACE_RESOURCE	     (visu_surface_resource_get_type ())
/**
 * VISU_SURFACE_RESOURCE:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuSurfaceResource type.
 */
#define VISU_SURFACE_RESOURCE(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_SURFACE_RESOURCE, VisuSurfaceResource))
/**
 * VISU_SURFACE_RESOURCE_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuSurfaceResourceClass.
 */
#define VISU_SURFACE_RESOURCE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_SURFACE_RESOURCE, VisuSurfaceResourceClass))
/**
 * VISU_IS_SURFACE_RESOURCE_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuSurfaceResource object.
 */
#define VISU_IS_SURFACE_RESOURCE_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_SURFACE_RESOURCE))
/**
 * VISU_IS_SURFACE_RESOURCE_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuSurfaceResourceClass class.
 */
#define VISU_IS_SURFACE_RESOURCE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_SURFACE_RESOURCE))
/**
 * VISU_SURFACE_RESOURCE_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_SURFACE_RESOURCE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_SURFACE_RESOURCE, VisuSurfaceResourceClass))


/**
 * VisuSurfaceResourcePrivate:
 * 
 * Private data for #VisuSurfaceResource objects.
 */
typedef struct _VisuSurfaceResourcePrivate VisuSurfaceResourcePrivate;

typedef struct _VisuSurfaceResource VisuSurfaceResource;
struct _VisuSurfaceResource
{
  VisuObject parent;

  VisuSurfaceResourcePrivate *priv;
};

/**
 * VisuSurfaceResourceClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuSurfaceResourceClass.
 */
typedef struct _VisuSurfaceResourceClass VisuSurfaceResourceClass;
struct _VisuSurfaceResourceClass
{
  VisuObjectClass parent;
};

/**
 * visu_surface_resource_get_type:
 *
 * This method returns the type of #VisuSurfaceResource, use
 * VISU_TYPE_SURFACE_RESOURCE instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuSurfaceResource.
 */
GType visu_surface_resource_get_type(void);

VisuSurfaceResource* visu_surface_resource_new_fromName(const gchar *surf_name,
                                                        gboolean *new_surf);
VisuSurfaceResource* visu_surface_resource_new_fromCopy(const gchar *surf_name,
                                                        const VisuSurfaceResource *orig);
const gchar* visu_surface_resource_getLabel(const VisuSurfaceResource *res);
const ToolColor* visu_surface_resource_getColor(const VisuSurfaceResource *res);
const float* visu_surface_resource_getMaterial(const VisuSurfaceResource *res);
gboolean visu_surface_resource_getRendered(const VisuSurfaceResource *res);
gboolean visu_surface_resource_getMaskable(const VisuSurfaceResource *res);

void visu_surface_resource_pool_finalize(void);

G_END_DECLS

#endif
