/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresses mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef MAP_H
#define MAP_H

#include <glib-object.h>

#include "scalarFields.h"
#include "plane.h"
#include "surfaces.h"
#include <coreTools/toolShade.h>
#include <coreTools/toolMatrix.h>

/**
 * VisuMapExportFormat:
 * @VISU_MAP_EXPORT_SVG: SVG export ;
 * @VISU_MAP_EXPORT_PDF: PDF export.
 *
 * Possible export for the map, see visu_map_export().
 *
 * Since: 3.6
 */
typedef enum
  {
    VISU_MAP_EXPORT_SVG,
    VISU_MAP_EXPORT_PDF
  } VisuMapExportFormat;

/**
 * VISU_TYPE_MAP:
 *
 * return the type of #VisuMap.
 */
#define VISU_TYPE_MAP	     (visu_map_get_type ())
/**
 * VISU_MAP:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuMap type.
 */
#define VISU_MAP(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_MAP, VisuMap))
/**
 * VISU_MAP_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuMapClass.
 */
#define VISU_MAP_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_MAP, VisuMapClass))
/**
 * VISU_IS_MAP_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuMap object.
 */
#define VISU_IS_MAP_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_MAP))
/**
 * VISU_IS_MAP_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuMapClass class.
 */
#define VISU_IS_MAP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_MAP))
/**
 * VISU_MAP_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_MAP_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_MAP, VisuMapClass))


/**
 * VisuMapPrivate:
 * 
 * Private data for #VisuMap objects.
 */
typedef struct _VisuMapPrivate VisuMapPrivate;

/**
 * VisuMap:
 * 
 * Common name to refer to a #_VisuMap.
 */
typedef struct _VisuMap VisuMap;
struct _VisuMap
{
  VisuObject parent;

  VisuMapPrivate *priv;
};

typedef struct _VisuMapClass VisuMapClass;
struct _VisuMapClass
{
  VisuObjectClass parent;

  GThreadPool *computePool;
};

/**
 * visu_map_get_type:
 *
 * This method returns the type of #VisuMap, use
 * VISU_TYPE_MAP instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuMap.
 */
GType visu_map_get_type(void);

VisuMap* visu_map_new();
VisuMap* visu_map_new_fromPlane(VisuPlane *plane);

gboolean visu_map_setPlane(VisuMap *map, VisuPlane *plane);
gboolean visu_map_setSurface(VisuMap *map, VisuSurface *surface);
gboolean visu_map_setField(VisuMap *map, VisuScalarField *field);
gboolean visu_map_setLevel(VisuMap *map, float glPrec, float gross, float refLength);
gboolean visu_map_setLines(VisuMap *map, guint nIsoLines, float minmax[2]);
gboolean visu_map_setScaling(VisuMap *map, ToolMatrixScalingFlag scale);
gboolean visu_map_setScalingRange(VisuMap *map, const float *minMax);

void visu_map_compute_sync(VisuMap *map);

VisuPlane* visu_map_getPlane(VisuMap *map);
VisuScalarField* visu_map_getField(VisuMap *map);
gboolean visu_map_getScaledMinMax(const VisuMap *map, float minMax[2]);
float* visu_map_getFieldMinMax(VisuMap *map);
const float* visu_map_getScalingRange(const VisuMap *map);

float visu_map_getLegendScale();
float visu_map_getLegendPosition(ToolXyzDir dir);

void visu_map_draw(VisuMap *map, float prec, ToolShade *shade, float *rgb,
                   gboolean alpha);
gboolean visu_map_export(VisuMap *map, const ToolShade *shade,
                         const float *rgb, float precision,
                         const gchar *filename, VisuMapExportFormat format,
                         GError **error);

#endif
