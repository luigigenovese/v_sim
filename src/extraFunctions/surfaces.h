/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2016)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2016)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef SURFACES_H
#define SURFACES_H

#include <glib.h>
#include <glib-object.h>

#include "surfaces_points.h"

#include <openGLFunctions/view.h>
#include <openGLFunctions/light.h>
#include <visu_data.h>

G_BEGIN_DECLS

/**
 * VISU_TYPE_SURFACE:
 *
 * return the type of #VisuSurface.
 */
#define VISU_TYPE_SURFACE	     (visu_surface_get_type ())
/**
 * SURFACES:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuSurface type.
 */
#define VISU_SURFACE(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_SURFACE, VisuSurface))
/**
 * VISU_SURFACE_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuSurfaceClass.
 */
#define VISU_SURFACE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_SURFACE, VisuSurfaceClass))
/**
 * VISU_IS_SURFACE_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuSurface object.
 */
#define VISU_IS_SURFACE_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_SURFACE))
/**
 * VISU_IS_SURFACE_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuSurfaceClass class.
 */
#define VISU_IS_SURFACE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_SURFACE))
/**
 * VISU_SURFACE_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_SURFACE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_SURFACE, VisuSurfaceClass))

typedef struct _VisuSurfaceClass VisuSurfaceClass;
typedef struct _VisuSurface VisuSurface;
typedef struct _VisuSurfacePrivate VisuSurfacePrivate;

/**
 * visu_surface_get_type:
 *
 * This method returns the type of #VisuSurface, use VISU_TYPE_SURFACE instead.
 *
 * Returns: the type of #VisuSurface.
 */
GType visu_surface_get_type(void);

/**
 * VisuSurface:
 *
 * All fields are private, use the access routines.
 */
struct _VisuSurface
{
  VisuObject parent;

  VisuSurfacePrivate *priv;
};
/**
 * VisuSurfaceClass:
 * @parent: the parent.
 *
 * An opaque structure.
 */
struct _VisuSurfaceClass
{
  VisuObjectClass parent;
};

/**
 * VISU_ERROR_ISOSURFACES: (skip)
 *
 * Internal function for error handling.
 */
#define VISU_ERROR_ISOSURFACES visu_surface_getErrorQuark()
GQuark visu_surface_getErrorQuark(void);
enum
  {
    VISU_ERROR_FORMAT,
    VISU_ERROR_CHECKSUM
  };

/**
 * VISU_SURFACE_PROPERTY_POTENTIAL
 *
 * Flag used in an ASCII surf file to give informations on the value
 * the surface is built from.
 */
#define VISU_SURFACE_PROPERTY_POTENTIAL "potential_values"

#define    VISU_TYPE_SURFACE_DEFINITION (visu_surface_definition_get_type())
GType      visu_surface_definition_get_type(void);
typedef struct _VisuSurfaceDefinition VisuSurfaceDefinition;
struct _VisuSurfaceDefinition
{
  GArray *points;
  GArray *polys;
};

/* Some constructors, destructors. */
VisuSurface* visu_surface_new(const gchar *label, const GArray *points, const GArray *polys);
VisuSurface* visu_surface_new_fromDefinition(const gchar *label,
                                             const VisuSurfaceDefinition *definition);

gboolean visu_surface_loadFile(const char *file, GList **surf, GError **error);

VisuSurfaceResource* visu_surface_getResource(VisuSurface *surf);
gboolean visu_surface_setResource(VisuSurface *surf, VisuSurfaceResource *res);

gboolean visu_surface_setMask(VisuSurface *surface, VisuPlaneSet *mask);

/**
 * VisuSurfaceIterPoly:
 * @surf: the parent #VisuSurface.
 * @valid: a flag TRUE, as long as the iterator is valid.
 * @i: the index of the current polygon.
 * @points: the storage for polygons.
 *
 * Structures used to iterate on polygons of @surf.
 *
 * Since: 3.8
 */
typedef struct _VisuSurfaceIterPoly VisuSurfaceIterPoly;
struct _VisuSurfaceIterPoly
{
  VisuSurface *surf;
  gboolean valid;
  guint i;

  /* Private */
  VisuSurfacePoints *points;
};

void visu_surface_iter_poly_new(VisuSurface *surf,
                               VisuSurfaceIterPoly *iter);
void visu_surface_iter_poly_next(VisuSurfaceIterPoly *iter);

gboolean visu_surface_iter_poly_getZ(const VisuSurfaceIterPoly *iter,
                                      double *z, const float mat[16]);
void visu_surface_iter_poly_getVertices(const VisuSurfaceIterPoly *iter,
                                         GArray *vertices);


float* visu_surface_getPropertyFloat(VisuSurface *surf, const gchar *name);
float* visu_surface_addPropertyFloat(VisuSurface *surf, const gchar* name);
gboolean visu_surface_getPropertyValueFloat(VisuSurface *surf,
					   const gchar *name, float *value);

void visu_surface_checkConsistency(VisuSurface* surf);

G_END_DECLS

#endif
