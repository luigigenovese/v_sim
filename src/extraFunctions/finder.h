/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2014)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2014)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#ifndef FINDER_H
#define FINDER_H

#include "visu_data.h"

/**
 * VISU_TYPE_NODE_FINDER:
 *
 * return the type of #VisuNodeFinder.
 *
 * Since: 3.8
 */
#define VISU_TYPE_NODE_FINDER	     (visu_node_finder_get_type ())
/**
 * VISU_NODE_FINDER:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuNodeFinder type.
 *
 * Since: 3.8
 */
#define VISU_NODE_FINDER(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_NODE_FINDER, VisuNodeFinder))
/**
 * VISU_NODE_FINDER_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuNodeFinderClass.
 *
 * Since: 3.8
 */
#define VISU_NODE_FINDER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_NODE_FINDER, VisuNodeFinderClass))
/**
 * VISU_IS_NODE_FINDER:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuNodeFinder object.
 *
 * Since: 3.8
 */
#define VISU_IS_NODE_FINDER(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_NODE_FINDER))
/**
 * VISU_IS_NODE_FINDER_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuNodeFinderClass class.
 *
 * Since: 3.8
 */
#define VISU_IS_NODE_FINDER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_NODE_FINDER))
/**
 * VISU_NODE_FINDER_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.8
 */
#define VISU_NODE_FINDER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_NODE_FINDER, VisuNodeFinderClass))

typedef struct _VisuNodeFinder        VisuNodeFinder;
typedef struct _VisuNodeFinderPrivate VisuNodeFinderPrivate;
typedef struct _VisuNodeFinderClass   VisuNodeFinderClass;

struct _VisuNodeFinder
{
  VisuObject parent;

  VisuNodeFinderPrivate *priv;
};

struct _VisuNodeFinderClass
{
  VisuObjectClass parent;
};

/**
 * visu_node_finder_get_type:
 *
 * This method returns the type of #VisuNodeFinder, use
 * VISU_TYPE_NODE_FINDER instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuNodeFinder.
 */
GType visu_node_finder_get_type(void);

VisuNodeFinder* visu_node_finder_new(VisuData *data);
VisuData* visu_node_finder_getData(VisuNodeFinder *finder);

gint visu_node_finder_lookup(VisuNodeFinder *finder, const gfloat at[3], gfloat tol);
void visu_node_finder_lookupArray(VisuNodeFinder *finder, gint *ids, const gfloat *at,
                                  guint np, gfloat tol);

#endif
