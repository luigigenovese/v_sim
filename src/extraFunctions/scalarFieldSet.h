/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef SCALARFIELDSET_H
#define SCALARFIELDSET_H

#include <glib.h>
#include <glib-object.h>

#include "scalarFields.h"
#include "sfielddata.h"

G_BEGIN_DECLS

/**
 * VISU_TYPE_SCALARFIELD_SET:
 *
 * return the type of #VisuScalarfieldSet.
 */
#define VISU_TYPE_SCALARFIELD_SET	     (visu_scalarfield_set_get_type ())
/**
 * VISU_SCALARFIELD_SET:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuScalarfieldSet type.
 */
#define VISU_SCALARFIELD_SET(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_SCALARFIELD_SET, VisuScalarfieldSet))
/**
 * VISU_SCALARFIELD_SET_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuScalarfieldSetClass.
 */
#define VISU_SCALARFIELD_SET_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_SCALARFIELD_SET, VisuScalarfieldSetClass))
/**
 * VISU_IS_SCALARFIELD_SET_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuScalarfieldSet object.
 */
#define VISU_IS_SCALARFIELD_SET_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_SCALARFIELD_SET))
/**
 * VISU_IS_SCALARFIELD_SET_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuScalarfieldSetClass class.
 */
#define VISU_IS_SCALARFIELD_SET_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_SCALARFIELD_SET))
/**
 * VISU_SCALARFIELD_SET_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_SCALARFIELD_SET_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_SCALARFIELD_SET, VisuScalarfieldSetClass))

/**
 * VisuScalarfieldSetPrivate:
 * 
 * Private data for #VisuScalarfieldSet objects.
 */
typedef struct _VisuScalarfieldSetPrivate VisuScalarfieldSetPrivate;

/**
 * VisuScalarfieldSet:
 * 
 * Common name to refer to a #_VisuScalarfieldSet.
 */
typedef struct _VisuScalarfieldSet VisuScalarfieldSet;
struct _VisuScalarfieldSet
{
  VisuObject parent;

  VisuScalarfieldSetPrivate *priv;
};

/**
 * VisuScalarfieldSetClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuScalarfieldSetClass.
 */
typedef struct _VisuScalarfieldSetClass VisuScalarfieldSetClass;
struct _VisuScalarfieldSetClass
{
  VisuObjectClass parent;
};

/**
 * visu_scalarfield_set_get_type:
 *
 * This method returns the type of #VisuScalarfieldSet, use
 * VISU_TYPE_SCALARFIELD_SET instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuScalarfieldSet.
 */
GType visu_scalarfield_set_get_type(void);

VisuScalarfieldSet* visu_scalarfield_set_new();

gboolean visu_scalarfield_set_add(VisuScalarfieldSet *set, const gchar *label,
                                  VisuScalarField *field);
gboolean visu_scalarfield_set_addFromFile(VisuScalarfieldSet *set,
                                          VisuScalarFieldMethod *meth,
                                          const gchar *filename,
                                          GHashTable *table,
                                          GCancellable *cancel,
                                          GAsyncReadyCallback callback,
                                          gpointer user_data);
gboolean visu_scalarfield_set_addFromFileSync(VisuScalarfieldSet *set,
                                              VisuScalarFieldMethod *meth,
                                              const gchar *filename,
                                              GHashTable *table,
                                              GCancellable *cancel,
                                              GError **error);
gboolean visu_scalarfield_set_remove(VisuScalarfieldSet *set, VisuScalarField *field);

VisuScalarField* visu_scalarfield_set_getAt(const VisuScalarfieldSet *set, guint i);
guint visu_scalarfield_set_getLength(const VisuScalarfieldSet *set);
const gchar* visu_scalarfield_set_getLabel(const VisuScalarfieldSet *set,
                                           const VisuScalarField *field);

VisuScalarfieldSet* visu_scalarfield_set_getDefault(void);
void visu_scalarfield_set_class_finalize(void);

typedef struct _VisuScalarfieldSetIter VisuScalarfieldSetIter;
struct _VisuScalarfieldSetIter
{
  const VisuScalarfieldSet *set;
  VisuScalarField *field;

  GList *next;
};
gboolean visu_scalarfield_set_iter_new(const VisuScalarfieldSet *set,
                                       VisuScalarfieldSetIter *iter);
gboolean visu_scalarfield_set_iter_next(VisuScalarfieldSetIter *iter);

G_END_DECLS

#endif
