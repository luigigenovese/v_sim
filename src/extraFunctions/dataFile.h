/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_DATAFILE_H
#define VISU_DATAFILE_H

#include <glib.h>
#include <visu_data.h>
#include <coreTools/toolShade.h>
#include <iface_nodemasker.h>
#include "floatProp.h"
#include "colorizer.h"

/**
 * VisuColorizationInputScaleId:
 * @VISU_COLORIZATION_NORMALIZE: input data are converted into [0;1] using input min/max values.
 * @VISU_COLORIZATION_MINMAX: input data are converted into [0;1] using user defined min/max values.
 *
 * Control how input data are converted into [0;1], after conversion,
 * values are clamped if needed.
 */
typedef enum
  {
    VISU_COLORIZATION_NORMALIZE,
    VISU_COLORIZATION_MINMAX
  } VisuColorizationInputScaleId;
/**
 * VISU_COLORIZATION_UNSET:
 *
 * To be used when a column id is awaited. A constant value will then
 * be used.
 */
#define VISU_COLORIZATION_UNSET  -4
/**
 * VISU_COLORIZATION_FROM_X:
 *
 * To be used when a column id is awaited. The reduced coordinate in x
 * direction will then be used.
 */
#define VISU_COLORIZATION_FROM_X  -3
/**
 * VISU_COLORIZATION_FROM_Y:
 *
 * To be used when a column id is awaited. The reduced coordinate in y
 * direction will then be used.
 */
#define VISU_COLORIZATION_FROM_Y  -2
/**
 * VISU_COLORIZATION_FROM_Z:
 *
 * To be used when a column id is awaited. The reduced coordinate in z
 * direction will then be used.
 */
#define VISU_COLORIZATION_FROM_Z  -1

/**
 * VISU_COLORIZATION_LABEL:
 *
 * Default label used to name colourisation data.
 */
#define VISU_COLORIZATION_LABEL _("Colorization data")

/**
 * VISU_TYPE_COLORIZATION:
 *
 * return the type of #VisuColorization.
 */
#define VISU_TYPE_COLORIZATION	     (visu_colorization_get_type ())
/**
 * VISU_COLORIZATION:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuColorization type.
 */
#define VISU_COLORIZATION(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_COLORIZATION, VisuColorization))
/**
 * VISU_COLORIZATION_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuColorizationClass.
 */
#define VISU_COLORIZATION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_COLORIZATION, VisuColorizationClass))
/**
 * VISU_IS_COLORIZATION_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuColorization object.
 */
#define VISU_IS_COLORIZATION_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_COLORIZATION))
/**
 * VISU_IS_COLORIZATION_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuColorizationClass class.
 */
#define VISU_IS_COLORIZATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_COLORIZATION))
/**
 * VISU_COLORIZATION_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_COLORIZATION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_COLORIZATION, VisuColorizationClass))


/**
 * VisuColorizationPrivate:
 * 
 * Private data for #VisuColorization objects.
 */
typedef struct _VisuColorizationPrivate VisuColorizationPrivate;

/**
 * VisuColorization:
 *
 * An opaque structure to store colorisation settings.
 *
 * Since: 3.7
 */
typedef struct _VisuColorization VisuColorization;
struct _VisuColorization
{
  VisuDataColorizer parent;

  VisuColorizationPrivate *priv;
};

/**
 * VisuColorizationClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuColorizationClass.
 */
typedef struct _VisuColorizationClass VisuColorizationClass;
struct _VisuColorizationClass
{
  VisuDataColorizerClass parent;
};

/**
 * visu_colorization_get_type:
 *
 * This method returns the type of #VisuColorization, use
 * VISU_TYPE_COLORIZATION instead.
 *
 * Returns: the type of #VisuColorization.
 */
GType visu_colorization_get_type(void);

VisuColorization* visu_colorization_new();
VisuColorization* visu_colorization_new_fromCLI(VisuData *dataObj, GError **error);
gboolean visu_colorization_setBox(VisuColorization *dt, VisuBox *box);
void visu_colorization_setNodeModel(VisuColorization *dt, VisuNodeValuesFarray *values);

gboolean visu_colorization_setScaleType(VisuColorization *dt, VisuColorizationInputScaleId scale);
VisuColorizationInputScaleId visu_colorization_getScaleType(const VisuColorization *dt);

gboolean visu_colorization_setMin(VisuColorization *dt, float min, int column);
gboolean visu_colorization_setMax(VisuColorization *dt, float max, int column);
float visu_colorization_getMin(const VisuColorization *dt, int column);
float visu_colorization_getMax(const VisuColorization *dt, int column);

gboolean visu_colorization_setColUsed(VisuColorization *dt, int val, int pos);
gboolean visu_colorization_setColUsedArr(VisuColorization *dt, const int vals[3]);
const int* visu_colorization_getColUsed(const VisuColorization *dt);

int visu_colorization_getNColumns(const VisuColorization *dt);
gboolean visu_colorization_getSingleColumnId(const VisuColorization *dt, gint *id);

gboolean visu_colorization_setShade(VisuColorization *dt, const ToolShade *shade);
const ToolShade* visu_colorization_getShade(const VisuColorization *dt);

gboolean visu_colorization_setScalingUsed(VisuColorization *dt, int val);
int visu_colorization_getScalingUsed(const VisuColorization *dt);

gboolean visu_colorization_setRestrictInRange(VisuColorization *dt, gboolean status);
gboolean visu_colorization_getRestrictInRange(const VisuColorization *dt);

const gchar* visu_colorization_getFile(const VisuColorization *dt);

#endif
