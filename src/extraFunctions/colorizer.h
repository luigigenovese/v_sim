/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef DATA_COLORIZER_H
#define DATA_COLORIZER_H

#include <glib.h>
#include <glib-object.h>

#include <visu_data.h>
#include "nodeProp.h"

G_BEGIN_DECLS

/* DataColorizer interface. */
#define VISU_TYPE_DATA_COLORIZER                (visu_data_colorizer_get_type())
#define VISU_DATA_COLORIZER(obj)                (G_TYPE_CHECK_INSTANCE_CAST((obj), VISU_TYPE_DATA_COLORIZER, VisuDataColorizer))
#define VISU_DATA_COLORIZER_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_DATA_COLORIZER, VisuDataColorizerClass))
#define VISU_IS_DATA_COLORIZER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE((obj), VISU_TYPE_DATA_COLORIZER))
#define VISU_IS_DATA_COLORIZER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_DATA_COLORIZER))
#define VISU_DATA_COLORIZER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_DATA_COLORIZER, VisuDataColorizerClass))

typedef struct _VisuDataColorizerClass VisuDataColorizerClass;
typedef struct _VisuDataColorizer VisuDataColorizer;
typedef struct _VisuDataColorizerPrivate VisuDataColorizerPrivate;

/**
 * visu_data_colorizer_get_type:
 *
 * This method returns the type of #VisuDataColorizer, use VISU_TYPE_DATA_COLORIZER instead.
 *
 * Returns: the type of #VisuDataColorizer.
 */
GType visu_data_colorizer_get_type(void);


/**
 * VisuDataColorizer:
 *
 * Structure used to define #VisuDataColorizer objects.
 *
 * Since: 3.8
 */
struct _VisuDataColorizer
{
  VisuObject parent;

  VisuDataColorizerPrivate *priv;
};

/**
 * VisuDataColorizerClass:
 * @parent: its parent.
 * @setNodeModel: a method to change the #VisuNodeValues used to store
 * the model.
 * @colorize: a method to colorize a given node according to a model.
 * @scale: a method to scale a given node according to a model.
 *
 * Interface for class that can represent #VisuDataColorizer.
 *
 * Since: 3.8
 */
struct _VisuDataColorizerClass
{
  VisuObjectClass parent;

  gboolean (*setNodeModel)(VisuDataColorizer *colorizer,
                           VisuNodeValues *model);
  gboolean (*colorize)(const VisuDataColorizer *colorizer, float rgba[4],
                       const VisuData *visuData, const VisuNode* node);
  gfloat (*scale)(const VisuDataColorizer *colorizer,
                  const VisuData *visuData, const VisuNode* node);
};

gboolean visu_data_colorizer_setActive(VisuDataColorizer *colorizer, gboolean status);
gboolean visu_data_colorizer_getActive(const VisuDataColorizer *colorizer);

gboolean visu_data_colorizer_setDirty(VisuDataColorizer *colorizer);

VisuNodeValues* visu_data_colorizer_getNodeModel(VisuDataColorizer *colorizer);
const VisuNodeValues* visu_data_colorizer_getConstNodeModel(const VisuDataColorizer *colorizer);
gboolean visu_data_colorizer_setNodeModel(VisuDataColorizer *colorizer,
                                          VisuNodeValues *model);

gboolean visu_data_colorizer_setSource(VisuDataColorizer *colorizer,
                                       const gchar *source);
const gchar* visu_data_colorizer_getSource(const VisuDataColorizer *colorizer);

gboolean visu_data_colorizer_getColor(const VisuDataColorizer *colorizer, float rgba[4],
                                      const VisuData *visuData, const VisuNode* node);
gfloat visu_data_colorizer_getScalingFactor(const VisuDataColorizer *colorizer,
                                            const VisuData *visuData,
                                            const VisuNode* node);

G_END_DECLS

#endif
