/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef STRINGPROP_H
#define STRINGPROP_H

#include <glib.h>
#include <glib-object.h>

#include "nodeProp.h"

G_BEGIN_DECLS

/**
 * VISU_TYPE_NODE_VALUES_STRING:
 *
 * return the type of #VisuNodeValuesString.
 */
#define VISU_TYPE_NODE_VALUES_STRING	     (visu_node_values_string_get_type ())
/**
 * VISU_NODE_VALUES_STRING:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuNodeValuesString type.
 */
#define VISU_NODE_VALUES_STRING(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_NODE_VALUES_STRING, VisuNodeValuesString))
/**
 * VISU_NODE_VALUES_STRING_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuNodeValuesStringClass.
 */
#define VISU_NODE_VALUES_STRING_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_NODE_VALUES_STRING, VisuNodeValuesStringClass))
/**
 * VISU_IS_NODE_VALUES_STRING_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuNodeValuesString object.
 */
#define VISU_IS_NODE_VALUES_STRING_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_NODE_VALUES_STRING))
/**
 * VISU_IS_NODE_VALUES_STRING_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuNodeValuesStringClass class.
 */
#define VISU_IS_NODE_VALUES_STRING_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_NODE_VALUES_STRING))
/**
 * VISU_NODE_VALUES_STRING_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_NODE_VALUES_STRING_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_NODE_VALUES_STRING, VisuNodeValuesStringClass))

/**
 * VisuNodeValuesString:
 * 
 * Common name to refer to a #_VisuNodeValuesString.
 */
typedef struct _VisuNodeValuesString VisuNodeValuesString;
struct _VisuNodeValuesString
{
  VisuNodeValues parent;
};

/**
 * VisuNodeValuesStringClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuNodeValuesStringClass.
 */
typedef struct _VisuNodeValuesStringClass VisuNodeValuesStringClass;
struct _VisuNodeValuesStringClass
{
  VisuNodeValuesClass parent;
};

/**
 * visu_node_values_string_get_type:
 *
 * This method returns the type of #VisuNodeValuesString, use
 * VISU_TYPE_NODE_VALUES_STRING instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuNodeValuesString.
 */
GType visu_node_values_string_get_type(void);

VisuNodeValuesString* visu_node_values_string_new(VisuNodeArray *arr,
                                                  const gchar *label);

const gchar* visu_node_values_string_getAt(VisuNodeValuesString *vect,
                                           const VisuNode *node);

gboolean visu_node_values_string_setAt(VisuNodeValuesString *vect,
                                       const VisuNode *node,
                                       const gchar *str);

G_END_DECLS

#endif
