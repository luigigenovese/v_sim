/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "translate.h"
#include <coreTools/toolMatrix.h>

/**
 * SECTION:translate
 * @short_description: A class defining translations for a set of nodes.
 *
 * <para>Define and apply a translation for a set of nodes. The
 * translation can be changed and the set of nodes also. Each time the
 * translation is applied, it is added to an undo stack.</para>
 */

/**
 * VisuNodeMoverTranslationClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuNodeMoverTranslationClass structure.
 *
 * Since: 3.8
 */
/**
 * VisuNodeMoverTranslation:
 *
 * An opaque structure.
 *
 * Since: 3.8
 */
struct _Params
{
  gfloat delta[3];
};
/**
 * VisuNodeMoverTranslationPrivate:
 *
 * Private fields for #VisuNodeMoverTranslation objects.
 *
 * Since: 3.8
 */
struct _VisuNodeMoverTranslationPrivate
{
  struct _Params params;
  struct _Params target;
  gfloat lastDelta[3];

  GSList *stack;
};


enum
  {
    PROP_0,
    TRANS_PROP,
    N_PROP
  };
static GParamSpec *_properties[N_PROP];

static void visu_node_mover_translation_finalize(GObject *obj);
static void visu_node_mover_translation_get_property(GObject* obj, guint property_id,
                                                  GValue *value, GParamSpec *pspec);
static void visu_node_mover_translation_set_property(GObject* obj, guint property_id,
                                                  const GValue *value, GParamSpec *pspec);
static gboolean _validate(const VisuNodeMover *mover);
static void _setup(VisuNodeMover *mover);
static gboolean _apply(VisuNodeMover *mover, VisuNodeArray *arr,
                       const GArray *ids, gfloat completion);
static gboolean _push(VisuNodeMover *mover);
static void _undo(VisuNodeMover *mover, VisuNodeArray *arr, const GArray *ids);

G_DEFINE_TYPE_WITH_CODE(VisuNodeMoverTranslation, visu_node_mover_translation,
                        VISU_TYPE_NODE_MOVER,
                        G_ADD_PRIVATE(VisuNodeMoverTranslation))

static void visu_node_mover_translation_class_init(VisuNodeMoverTranslationClass *klass)
{
  DBG_fprintf(stderr, "Visu MoverTranslation: creating the class of the object.\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->finalize = visu_node_mover_translation_finalize;
  G_OBJECT_CLASS(klass)->set_property = visu_node_mover_translation_set_property;
  G_OBJECT_CLASS(klass)->get_property = visu_node_mover_translation_get_property;
  VISU_NODE_MOVER_CLASS(klass)->validate = _validate;
  VISU_NODE_MOVER_CLASS(klass)->setup = _setup;
  VISU_NODE_MOVER_CLASS(klass)->apply = _apply;
  VISU_NODE_MOVER_CLASS(klass)->push = _push;
  VISU_NODE_MOVER_CLASS(klass)->undo = _undo;

  /**
   * VisuNodeMoverTranslation::translation:
   *
   * The translation.
   *
   * Since: 3.8
   */
  _properties[TRANS_PROP] = g_param_spec_boxed("translation", "Translation",
                                               "value of translation.",
                                               TOOL_TYPE_VECTOR,
                                               G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties(G_OBJECT_CLASS(klass), N_PROP, _properties);
}

static void visu_node_mover_translation_init(VisuNodeMoverTranslation *obj)
{
  DBG_fprintf(stderr, "Visu MoverTranslation: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = visu_node_mover_translation_get_instance_private(obj);

  obj->priv->stack = (GSList*)0;
  obj->priv->params.delta[0] = 0.f;
  obj->priv->params.delta[1] = 0.f;
  obj->priv->params.delta[2] = 0.f;
}
static void visu_node_mover_translation_get_property(GObject* obj, guint property_id,
                                                  GValue *value, GParamSpec *pspec)
{
  VisuNodeMoverTranslation *self = VISU_NODE_MOVER_TRANSLATION(obj);

  DBG_fprintf(stderr, "Visu MoverTranslation: get property %s.\n",
              g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case TRANS_PROP:
      g_value_set_boxed(value, self->priv->params.delta);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_node_mover_translation_set_property(GObject* obj, guint property_id,
                                                  const GValue *value, GParamSpec *pspec)
{
  VisuNodeMoverTranslation *self = VISU_NODE_MOVER_TRANSLATION(obj);

  DBG_fprintf(stderr, "Visu MoverTranslation: set property %s.\n",
              g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case TRANS_PROP:
      visu_node_mover_translation_set(self, (float*)g_value_get_boxed(value));
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_node_mover_translation_finalize(GObject *obj)
{
  VisuNodeMoverTranslation *trans;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Visu MoverTranslation: finalize object %p.\n", (gpointer)obj);

  trans = VISU_NODE_MOVER_TRANSLATION(obj);

  /* Free privs elements. */
  if (trans->priv->stack)
    g_slist_free_full(trans->priv->stack, g_free);

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Visu MoverTranslation: chain to parent.\n");
  G_OBJECT_CLASS(visu_node_mover_translation_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Visu MoverTranslation: freeing ... OK.\n");
}

static gboolean _validate(const VisuNodeMover *mover)
{
  VisuNodeMoverTranslation *trans;
  g_return_val_if_fail(VISU_IS_NODE_MOVER_TRANSLATION(mover), FALSE);

  trans = VISU_NODE_MOVER_TRANSLATION(mover);
  if (trans->priv->params.delta[0] == 0.f &&
      trans->priv->params.delta[1] == 0.f &&
      trans->priv->params.delta[2] == 0.f)
    return FALSE;
  return TRUE;
}
static void _setup(VisuNodeMover *mover)
{
  VisuNodeMoverTranslation *trans;
  g_return_if_fail(VISU_IS_NODE_MOVER_TRANSLATION(mover));

  trans = VISU_NODE_MOVER_TRANSLATION(mover);
  trans->priv->target = trans->priv->params;
  trans->priv->lastDelta[0] = 0.f;
  trans->priv->lastDelta[1] = 0.f;
  trans->priv->lastDelta[2] = 0.f;
}
static gboolean _apply(VisuNodeMover *mover, VisuNodeArray *arr,
                       const GArray *ids, gfloat completion)
{
  VisuNodeMoverTranslation *trans;
  struct _Params *params;
  gfloat delta[3];
  
  g_return_val_if_fail(VISU_IS_NODE_MOVER_TRANSLATION(mover), FALSE);

  trans = VISU_NODE_MOVER_TRANSLATION(mover);
  DBG_fprintf(stderr, "Visu MoverTranslation: apply translation %gx%gx%g.\n",
              trans->priv->params.delta[0],
              trans->priv->params.delta[1], trans->priv->params.delta[2]);
  if (!_validate(mover))
    return FALSE;

  delta[0] = trans->priv->target.delta[0] * completion - trans->priv->lastDelta[0];
  delta[1] = trans->priv->target.delta[1] * completion - trans->priv->lastDelta[1];
  delta[2] = trans->priv->target.delta[2] * completion - trans->priv->lastDelta[2];
  visu_node_array_shiftNodes(arr, ids, delta);
  trans->priv->lastDelta[0] = trans->priv->target.delta[0] * completion;
  trans->priv->lastDelta[1] = trans->priv->target.delta[1] * completion;
  trans->priv->lastDelta[2] = trans->priv->target.delta[2] * completion;
  if (completion == 1.f)
    {
      params = g_malloc(sizeof(struct _Params));
      *params = trans->priv->params;
      trans->priv->stack = g_slist_prepend(trans->priv->stack, params);
    }
  return TRUE;
}
static gboolean _push(VisuNodeMover *mover)
{
  VisuNodeMoverTranslation *trans;
  struct _Params *params;
  
  g_return_val_if_fail(VISU_IS_NODE_MOVER_TRANSLATION(mover), FALSE);

  trans = VISU_NODE_MOVER_TRANSLATION(mover);
  if (!_validate(mover))
    return FALSE;

  DBG_fprintf(stderr, "Visu MoverTranslation: push the current translation.\n");
  params = g_malloc(sizeof(struct _Params));
  *params = trans->priv->params;
  trans->priv->stack = g_slist_prepend(trans->priv->stack, params);
  return TRUE;
}
static void _undo(VisuNodeMover *mover, VisuNodeArray *arr, const GArray *ids)
{
  VisuNodeMoverTranslation *trans;
  struct _Params *params;
  gfloat delta[3];
  GSList *p;
  
  g_return_if_fail(VISU_IS_NODE_MOVER_TRANSLATION(mover));

  trans = VISU_NODE_MOVER_TRANSLATION(mover);
  if (!trans->priv->stack)
    return;
  
  p = trans->priv->stack;
  params = (struct _Params*)p->data;
  delta[0] = -params->delta[0];
  delta[1] = -params->delta[1];
  delta[2] = -params->delta[2];
  visu_node_array_shiftNodes(arr, ids, delta);

  trans->priv->stack = g_slist_next(trans->priv->stack);
  g_free(params);
  g_slist_free_1(p);
}

/**
 * visu_node_mover_translation_new:
 *
 * Creates a new #VisuNodeMoverTranslation object.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a new #VisuNodeMoverTranslation object.
 **/
VisuNodeMoverTranslation* visu_node_mover_translation_new()
{
  return g_object_new(VISU_TYPE_NODE_MOVER_TRANSLATION, NULL);
}
/**
 * visu_node_mover_translation_new_full:
 * @ids: (element-type guint): a list of node ids.
 * @trans: (array fixed-size=3): a translation.
 *
 * Creates a new @trans for nodes defined by @ids.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a new #VisuNodeMoverTranslation object.
 **/
VisuNodeMoverTranslation* visu_node_mover_translation_new_full(const GArray *ids,
                                                               const gfloat trans[3])
{
  return g_object_new(VISU_TYPE_NODE_MOVER_TRANSLATION, "ids", ids,
                      "translation", trans, NULL);
}

/**
 * visu_node_mover_translation_set:
 * @trans: a #VisuNodeMoverTranslation object.
 * @delta: (array fixed-size=3): a translation.
 *
 * Defines the translation to be applied by @trans.
 *
 * Since: 3.8
 *
 * Returns: TRUE if value is actually changed.
 **/
gboolean visu_node_mover_translation_set(VisuNodeMoverTranslation *trans,
                                          const gfloat delta[3])
{
  g_return_val_if_fail(VISU_IS_NODE_MOVER_TRANSLATION(trans), FALSE);

  DBG_fprintf(stderr, "Visu MoverTranslation: set %gx%gx%g.\n",
              delta[0], delta[1], delta[2]);
  if (!tool_vector_set(trans->priv->params.delta, delta))
    return FALSE;

  DBG_fprintf(stderr, "Visu MoverTranslation: translation changed.\n");
  g_object_notify_by_pspec(G_OBJECT(trans), _properties[TRANS_PROP]);
  g_object_notify(G_OBJECT(trans), "valid");
  return TRUE;
}
/**
 * visu_node_mover_translation_reset:
 * @trans: a #VisuNodeMoverTranslation object.
 *
 * Helper function to sets the translation to [0;0;0].
 *
 * Since: 3.8
 *
 * Returns: TRUE if translation is actually changed.
 **/
gboolean visu_node_mover_translation_reset(VisuNodeMoverTranslation *trans)
{
  gfloat zeros[3] = {0.f, 0.f, 0.f};
  return visu_node_mover_translation_set(trans, zeros);
}
/**
 * visu_node_mover_translation_add:
 * @trans: a #VisuNodeMoverTranslation object.
 * @delta: (array fixed-size=3): a translation.
 *
 * Add @delta to the current translation.
 *
 * Since: 3.8
 *
 * Returns: TRUE if translation is actually changed.
 **/
gboolean visu_node_mover_translation_add(VisuNodeMoverTranslation *trans,
                                         const gfloat delta[3])
{
  gfloat value[3];

  g_return_val_if_fail(VISU_IS_NODE_MOVER_TRANSLATION(trans), FALSE);

  DBG_fprintf(stderr, "Visu MoverTranslation: add %gx%gx%g.\n",
              delta[0], delta[1], delta[2]);
  value[0] = trans->priv->params.delta[0] + delta[0];
  value[1] = trans->priv->params.delta[1] + delta[1];
  value[2] = trans->priv->params.delta[2] + delta[2];
  return visu_node_mover_translation_set(trans, value);
}

/**
 * visu_node_mover_translation_get:
 * @trans: a #VisuNodeMoverTranslation object.
 * @delta: (out) (array fixed-size=3): a location for the translation.
 *
 * Retrieves the current translation.
 *
 * Since: 3.8
 **/
void visu_node_mover_translation_get(const VisuNodeMoverTranslation *trans,
                                     gfloat delta[3])
{
  g_return_if_fail(VISU_IS_NODE_MOVER_TRANSLATION(trans));

  tool_vector_set(delta, trans->priv->params.delta);
}

