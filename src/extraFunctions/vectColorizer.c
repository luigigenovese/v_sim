/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2018)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2018)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "vectColorizer.h"

/**
 * SECTION:vectColorizer
 * @short_description: A class defining node colorisation according to
 * #VisuNodeValuesVect.
 *
 * <para>This class implements #VisuDataColorizer for data coming from
 * #VisuNodeVector. One can specify colorization according to
 * vector label or vector id, see
 * visu_data_colorizer_vector_setType(). This class also implements
 * #VisuNodeMaskerInterface and can be used to toggle the visibility
 * of #VisuNode depending on their vector label, see
 * visu_data_colorizer_vector_setVisibility() and
 * visu_data_colorizer_vector_setDefaultVisibility().</para>
 */

struct _VisuDataColorizerVectorPrivate
{
  ToolShade *shade;
};

static void visu_data_colorizer_vector_finalize(GObject* obj);
static gboolean _colorize(const VisuDataColorizer *colorizer,
                          float rgba[4], const VisuData *visuData,
                          const VisuNode* node);
static gboolean _setNodeModel(VisuDataColorizer *colorizer, VisuNodeValues *model);

G_DEFINE_TYPE_WITH_CODE(VisuDataColorizerVector, visu_data_colorizer_vector,
                        VISU_TYPE_DATA_COLORIZER,
                        G_ADD_PRIVATE(VisuDataColorizerVector))

static void visu_data_colorizer_vector_class_init(VisuDataColorizerVectorClass *klass)
{
  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->finalize = visu_data_colorizer_vector_finalize;
  VISU_DATA_COLORIZER_CLASS(klass)->setNodeModel = _setNodeModel;
  VISU_DATA_COLORIZER_CLASS(klass)->colorize = _colorize;
}

static void visu_data_colorizer_vector_init(VisuDataColorizerVector *obj)
{
  DBG_fprintf(stderr, "Visu Data Colorizer Vector: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = visu_data_colorizer_vector_get_instance_private(obj);
  obj->priv->shade = tool_shade_copy(tool_shade_getById(0));
}

static void visu_data_colorizer_vector_finalize(GObject *obj)
{
  VisuDataColorizerVector *vect;

  g_return_if_fail(obj);

  vect = VISU_DATA_COLORIZER_VECTOR(obj);

  tool_shade_free(vect->priv->shade);

  G_OBJECT_CLASS(visu_data_colorizer_vector_parent_class)->finalize(obj);
}

/**
 * visu_data_colorizer_vector_new:
 *
 * Creates a #VisuDataColorizer object to colorize and hide #VisuNode
 * based on a #VisuNodeValuesVect model.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly created
 * #VisuDataColorizerVector object.
 **/
VisuDataColorizerVector* visu_data_colorizer_vector_new()
{
  return VISU_DATA_COLORIZER_VECTOR(g_object_new(VISU_TYPE_DATA_COLORIZER_VECTOR, NULL));
}

/**
 * visu_data_colorizer_vector_setNodeModel:
 * @colorizer: a #VisuDataColorizerVector object.
 * @model: (transfer none): a #VisuNodeValuesVect object.
 *
 * Associate @model to @colorizer.
 *
 * Since: 3.8
 *
 * Returns: TRUE if the model is indeed changed.
 **/
gboolean visu_data_colorizer_vector_setNodeModel(VisuDataColorizerVector *colorizer,
                                                 VisuNodeValuesVector *model)
{
  return _setNodeModel(VISU_DATA_COLORIZER(colorizer), VISU_NODE_VALUES(model));
}

/**
 * visu_data_colorizer_vector_setShade:
 * @colorizer: a #VisuDataColorizerVector object.
 * @shade: a #ToolShade object.
 *
 * Change the @shade used by the @colorizer.
 *
 * Since: 3.8
 *
 * Returns: TRUE if the shade is actually changed.
 **/
gboolean visu_data_colorizer_vector_setShade(VisuDataColorizerVector *colorizer,
                                             const ToolShade *shade)
{
  g_return_val_if_fail(VISU_IS_DATA_COLORIZER_VECTOR_TYPE(colorizer), FALSE);

  if (tool_shade_compare(colorizer->priv->shade, shade))
    return FALSE;

  tool_shade_free(colorizer->priv->shade);
  colorizer->priv->shade = tool_shade_copy(shade);
  visu_data_colorizer_setDirty(VISU_DATA_COLORIZER(colorizer));

  return TRUE;
}

static gboolean _setNodeModel(VisuDataColorizer *colorizer,
                              VisuNodeValues *model)
{
  g_return_val_if_fail(VISU_IS_NODE_VALUES_VECTOR_TYPE(model) || !model, FALSE);

  return VISU_DATA_COLORIZER_CLASS(visu_data_colorizer_vector_parent_class)->setNodeModel(colorizer, model);
}

static gboolean _colorize(const VisuDataColorizer *colorizer,
                          float rgba[4], const VisuData *visuData _U_,
                          const VisuNode* node)
{
  const gfloat *f;
  const VisuNodeValues *model;
  const VisuDataColorizerVector *self;
  gfloat m, M;

  DBG_fprintf(stderr, "Hello %d!\n", node->number);
  model = visu_data_colorizer_getConstNodeModel(colorizer);
  if (!visu_data_colorizer_getActive(colorizer) || !model)
    return FALSE;

  f = visu_node_values_vector_getAtSpherical(VISU_NODE_VALUES_VECTOR(model), node);
  if (!f)
    return FALSE;
  m = visu_node_values_farray_min(VISU_NODE_VALUES_FARRAY(model));
  M = visu_node_values_farray_max(VISU_NODE_VALUES_FARRAY(model));

  self = VISU_DATA_COLORIZER_VECTOR(colorizer);
  tool_shade_valueToRGB(self->priv->shade, rgba,
                        (f[TOOL_MATRIX_SPHERICAL_MODULUS] - m) / (M - m));
  DBG_fprintf(stderr, "done %g!\n", (f[TOOL_MATRIX_SPHERICAL_MODULUS] - m) / (M - m));
  return TRUE;
}
