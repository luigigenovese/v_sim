/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2018)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2018)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef VECTCOLORIZER_H
#define VECTCOLORIZER_H

#include <glib.h>
#include <glib-object.h>

#include "colorizer.h"
#include "vectorProp.h"
#include <coreTools/toolShade.h>

G_BEGIN_DECLS

/**
 * VISU_TYPE_DATA_COLORIZER_VECTOR:
 *
 * return the type of #VisuDataColorizerVector.
 */
#define VISU_TYPE_DATA_COLORIZER_VECTOR	     (visu_data_colorizer_vector_get_type ())
/**
 * VISU_DATA_COLORIZER_VECTOR:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuDataColorizerVector type.
 */
#define VISU_DATA_COLORIZER_VECTOR(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_DATA_COLORIZER_VECTOR, VisuDataColorizerVector))
/**
 * VISU_DATA_COLORIZER_VECTOR_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuDataColorizerVectorClass.
 */
#define VISU_DATA_COLORIZER_VECTOR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_DATA_COLORIZER_VECTOR, VisuDataColorizerVectorClass))
/**
 * VISU_IS_DATA_COLORIZER_VECTOR_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuDataColorizerVector object.
 */
#define VISU_IS_DATA_COLORIZER_VECTOR_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_DATA_COLORIZER_VECTOR))
/**
 * VISU_IS_DATA_COLORIZER_VECTOR_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuDataColorizerVectorClass class.
 */
#define VISU_IS_DATA_COLORIZER_VECTOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_DATA_COLORIZER_VECTOR))
/**
 * VISU_DATA_COLORIZER_VECTOR_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_DATA_COLORIZER_VECTOR_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_DATA_COLORIZER_VECTOR, VisuDataColorizerVectorClass))

/**
 * VisuDataColorizerVectorPrivate:
 * 
 * Private data for #VisuDataColorizerVector objects.
 */
typedef struct _VisuDataColorizerVectorPrivate VisuDataColorizerVectorPrivate;

/**
 * VisuDataColorizerVector:
 * 
 * Common name to refer to a #_VisuDataColorizerVector.
 */
typedef struct _VisuDataColorizerVector VisuDataColorizerVector;
struct _VisuDataColorizerVector
{
  VisuDataColorizer parent;

  VisuDataColorizerVectorPrivate *priv;
};

/**
 * VisuDataColorizerVectorClass:
 * @parent: its parent.
 *
 * Interface for class that can represent #VisuDataColorizerVector.
 *
 * Since: 3.8
 */
typedef struct _VisuDataColorizerVectorClass VisuDataColorizerVectorClass;
struct _VisuDataColorizerVectorClass
{
  VisuDataColorizerClass parent;
};

/**
 * visu_data_colorizer_vector_get_type:
 *
 * This method returns the type of #VisuDataColorizerVector, use
 * VISU_TYPE_DATA_COLORIZER_VECTOR instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuDataColorizerVector.
 */
GType visu_data_colorizer_vector_get_type(void);

VisuDataColorizerVector* visu_data_colorizer_vector_new();

gboolean visu_data_colorizer_vector_setNodeModel(VisuDataColorizerVector *colorizer,
                                                 VisuNodeValuesVector *model);
gboolean visu_data_colorizer_vector_setShade(VisuDataColorizerVector *colorizer,
                                             const ToolShade *shade);

G_END_DECLS

#endif
