/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "visu_basic.h"
#include "visu_commandLine.h"
#include "visu_tools.h"
#include "visu_gtk.h"
#include "gtk_main.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <glib.h>

#ifdef HAVE_GTKGLEXT
#include <gdk/gdkgl.h>
#endif

int main (int argc, char *argv[])
{
  int res;

  DBG_fprintf(stderr, "--- Get the default path ---\n");
  visuBasicSet_paths(argv[0]);

#ifdef G_THREADS_ENABLED
  DBG_fprintf(stderr, "--- Initialise threads ---\n");
  g_thread_init(NULL);
#endif

#ifdef ENABLE_NLS
  setlocale (LC_ALL, "");
  bindtextdomain (GETTEXT_PACKAGE, v_sim_locale_dir);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);
#endif

  DBG_fprintf(stderr, "--- Parse the command line ---\n");
  res = commandLineParse(argc, argv);
  if (res)
    exit(1);

  DBG_fprintf(stderr, "Visu Main: initialise Gtk.\n");
  gtk_init(&argc, &argv);

#ifdef HAVE_GTKGLEXT
  DBG_fprintf(stderr, "Visu Main: initialise GtkGlExt.\n");
  gdk_gl_init(&argc, &argv);
#endif

  /* the default is V_Sim with gtk interface. */
  visu_ui_mainCreate(visu_ui_createInterface);

  DBG_fprintf(stderr, "Visu Main: starting main GTK loop.\n");
  gtk_main();

  return res;
}
