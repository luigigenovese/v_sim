/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_COMMANDLINE_H
#define VISU_COMMANDLINE_H

/* This .h and .c are here to read parameters from the
   command line. */
#include <glib.h>

#include <coreTools/toolOptions.h>
#include <coreTools/toolMatrix.h>

/**
 * VISU_COMMAND_LINE_ERROR: (skip)
 *
 * Internal function for error handling.
 */
#define VISU_COMMAND_LINE_ERROR visu_command_line_getErrorQuark()
GQuark visu_command_line_getErrorQuark();
/**
 * VisuCommandLineErrorFlag:
 * @ERROR_ARGUMENT: invalid argument.
 *
 * Possible errors when reading a file with column data.
 */
typedef enum
  {
    ERROR_ARGUMENT
  } VisuCommandLineErrorFlag;

/**
 * commandLineParse:
 * @argc: the number of arguments.
 * @argv: the values of all arguments.
 *
 * This method is called at startup to parse the command line
 * and store all important information. If --help is given, or an unknown
 * option, a little help is printed on the standard output.
 *
 * Returns: 0 if everything goes well.
 */
int commandLineParse(int argc, char **argv);

/**
 * commandLineExport:
 * @filename: a path to a filename to create ;
 * @error: a location to store a possible error.
 *
 * Export the known command line options to an XML file.
 *
 * Returns: TRUE on success.
 *
 * Since: 3.5
 */
gboolean commandLineExport(const gchar *filename, GError **error);

/**
 * commandLineGet_ArgFilename:
 *
 * This method retrieves the first argument. All other arguments are ignored.
 *
 * Returns: the value of the first argument.
 */
char* commandLineGet_ArgFilename(void);
/**
 * commandLineGet_ArgSpinFileName:
 *
 * This method retrieves the second argument. All other arguments are ignored.
 *
 * Returns: the value of the second argument.
 */
char* commandLineGet_ArgSpinFileName(void);
/**
 * commandLineGet_ExportFileName:
 *
 * This method retrieves the value of the option --export or -e. This value must
 * be a valid filename, with an extension known by V_Sim to do the export.
 *
 * Returns: the value of the option --export.
 */
char* commandLineGet_ExportFileName(void);
/**
 * commandLineGet_WithGtk:
 *
 * This method tells V_Sim is the GTK interface is needed or not.
 *
 * Returns: 1 if the interface is needed.
 */
int   commandLineGet_WithGtk(void);
/**
 * commandLineGet_XWindowGeometry:
 * @width: an integer to stores the desired width.
 * @height: an integer to stores the desired height.
 *
 * This method retrieves the values of the option --geometry or -g.
 * These values must be formatted with the following format : &amp;dx&amp;d and they give
 * the size of the rendering window.
 *
 */
void  commandLineGet_XWindowGeometry(int *width, int *height);
/**
 * commandLineGet_colorizeFileName:
 *
 * This method retrieves the value of the option --colorize or -c. This value must
 * be a valid filename. If this option is called, V_Sim actually enable
 * the colorization, even if parameter file doesn't.
 *
 * Returns: the value of the option --colorize.
 */
const gchar* commandLineGet_colorizeSource(gboolean *isFile);
/**
 * commandLineGet_colorizeColUsed:
 *
 * This method retrieves the value of the option --use-column or -u. This value consists
 * of three integer values.
 *
 * Returns: the three values of the option --use-column, or NULL if this option
 *          is not present.
 */
int* commandLineGet_colorizeColUsed(void);
/**
 * commandLineGet_colorizePresetColor:
 *
 * DEPRECATED, use commandLineGet_presetColor() instead.
 *
 * Returns: the value of option --color-preset if set, -1 if not.
 */
#define commandLineGet_colorizePresetColor() commandLineGet_presetColor();
/**
 * commandLineGet_presetColor:
 *
 * This method returns the value of option --color-preset.
 *
 * Returns: the value of option --color-preset if set, -1 if not.
 */
int commandLineGet_presetColor(void);
float* commandLineGet_translation(gboolean *boxTranslation);
/**
 * commandLineGet_extension:
 *
 * This method retrieves the value of the option --expand or -x. This value consists
 * of three floating values.
 *
 * Returns: the three values of the option --expand.
 */
float* commandLineGet_extension(void);
/**
 * commandLineGet_planesFileName:
 *
 * This method retrieves the value of the option --planes or -p. This value must
 * be a valid filename.
 *
 * Returns: the value of the option --planes.
 */
gchar* commandLineGet_planesFileName(void);
/**
 * commandLineGet_spinHidingMode:
 *
 * This method retrieves if the option --hiding-mode or -m has been set.
 *
 * Returns: the value of the option.
 */
int commandLineGet_spinHidingMode(void);
/**
 * commandLineGet_spinAndAtomic:
 *
 * This method retrieves if the option --spin-and-atomic or -a has been set.
 *
 * Returns: the TRUE if the option exists.
 */
gboolean commandLineGet_spinAndAtomic(void);
/**
 * commandLineGet_isoValues:
 * @nb: a location to store an integer.
 *
 * This method retrieves the values of the option --ios-values or -v.
 *
 * Returns: an array with the values of a size stored in @nb.
 */
float* commandLineGet_isoValues(int *nb);
/**
 * commandLineGet_isoNames:
 * @nb: a location to store an integer.
 *
 * This method retrieves the names associated to the values of the option --ios-values or -v.
 * It returns an array of size @nb, but not all element are set since names are not
 * mandatory. The @nb value is guarantied to by equal to the one returned by
 * commandLineGet_isoValues(void);
 *
 * Returns: an array with the values of a size stored in @nb.
 */
const gchar** commandLineGet_isoNames(int *nb);
const GList* commandLineGet_scalarFieldFileNames(void);
/**
 * commandLineGet_isoVisuSurfaceFileName:
 *
 * This method retrieves the filename given by the option --iso-surfaces or -i.
 *
 * Returns: a filename, the string is owned by V_Sim.
 */
gchar* commandLineGet_isoVisuSurfaceFileName(void);
/**
 * commandLineGet_fitToBox:
 *
 * This method gets if the surface should be adapted to the bounding box of the structure.
 *
 * Returns: TRUE if the surface should be fitted.
 */
gboolean commandLineGet_fitToBox(void);
/**
 * commandLineGet_resourcesFile:
 *
 * This method gets if a resources file has been given.
 *
 * Returns: the name (owned by V_Sim) of the given resources file or NULL if none
 *          was present.
 */
gchar* commandLineGet_resourcesFile(void);
/**
 * commandLineGet_options:
 *
 * This method gets the contents of all -o options. The value is first parsed as
 * letters to check for a boolean value (F/T), then, a float is used and finally
 * an integer. If nothing parsed, the option is dismissed.
 *
 * Returns: a #GHashTable pointer owned by V_Sim.
 */
GHashTable* commandLineGet_options(void);
int* commandLineGet_coloredMap(void);
/**
 * commandLineGet_logScale:
 *
 * Retrieve if a log scale is required for various plots.
 *
 * Returns: the logscale method.
 */
ToolMatrixScalingFlag commandLineGet_logScale(void);
/**
 * commandLineGet_nIsoLines:
 *
 * Retrieve if the user asked for isolines on the coloured map (see
 * commandLineGet_coloredMap()).
 *
 * Returns: a positive number if some isolines are required, 0 if not.
 */
guint commandLineGet_nIsoLines(void);
/**
 * commandLineGet_bgImage:
 *
 * Retrieve if the filename to be loaded as a background image.
 *
 * Returns: a string or NULL if option is not used.
 */
gchar* commandLineGet_bgImage(void);
/**
 * commandLineGet_isoLinesColor:
 *
 * Retrieve the chosen colour for the iso-lines.
 *
 * Returns: an array of three floats or NULL if the colour is auto.
 *
 * Since: 3.5
 */
float* commandLineGet_isoLinesColor(void);
/**
 * commandLineGet_windowMode:
 *
 * Retrieve the windowing mode for V_Sim, 'classic' with the two
 * rendering and command panel windows ; 'oneWindow' with a joined
 * version of the two windows or 'renderOnly' with only the rendering window.
 *
 * Returns: one of the three strings.
 *
 * Since: 3.5
 */
gchar* commandLineGet_windowMode(void);
/**
 * commandLineGet_iSet:
 *
 * Retrieve the desired id for multi dataset file to render.
 *
 * Returns: 0 as default value.
 *
 * Since: 3.5
 */
guint commandLineGet_iSet(void);
/**
 * commandLineGet_valueFile:
 *
 * Retrieve the name of a possible value file, if any has been given.
 *
 * Returns: a path (absolute) or NULL if the option has not been
 * passed. Data are static.
 *
 * Since: 3.5
 */
gchar* commandLineGet_valueFile(void);
void commandLineFree_all(void);
guint commandLineGet_mapPrecision(void);
float* commandLineGet_mapMinMax(void);
typedef struct _VisuColorRange VisuColorRange;
/**
 * VisuColorRange:
 * @column: a column identifgier.
 * @min: the minimum value for clamping.
 * @max: the maximum value for clamping.
 *
 * A structure to pass information from the command line to the code
 * on clamping values for column.
 *
 * Since: 3.8
 */
struct _VisuColorRange
{
  int column;
  float min, max;
};
GArray* commandLineGet_colorMinMax(void);
const gchar* commandLineGet_programName(void);
int commandLineGet_scalingColumn(void);
const gchar* commandLineGet_geodiff(void);
gint commandLineGet_phononMode(void);
gfloat commandLineGet_phononTime(void);
gfloat commandLineGet_phononAmpl(void);

#endif
