/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2014)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2014)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "ui_box.h"

#include <support.h>
#include <visu_tools.h>

/**
 * SECTION:ui_box
 * @short_description: Defines a widget to setup box.
 *
 * <para>A set of widgets to setup the rendring of a box.</para>
 */

/**
 * VisuUiBoxClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuUiBoxClass structure.
 *
 * Since: 3.8
 */
/**
 * VisuUiBox:
 *
 * An opaque structure.
 *
 * Since: 3.8
 */
/**
 * VisuUiBoxPrivate:
 *
 * Private fields for #VisuUiBox objects.
 *
 * Since: 3.8
 */
struct _VisuUiBoxPrivate
{
  gboolean dispose_has_run;

  GtkWidget *checkBox;
  GtkWidget *spinXPos, *spinYPos;

  VisuGlExtBox *model;

  VisuGlExtBoxLegend *legend;
  GBinding *bind_active, *bind_xpos, *bind_ypos;

  VisuBox *box;
};

enum {
  SHOW_SIZE_SIGNAL,
  X_POS_SIGNAL,
  Y_POS_SIGNAL,
  LAST_SIGNAL
};
static guint signals[LAST_SIGNAL] = { 0 };

static void visu_ui_box_finalize(GObject* obj);
static void visu_ui_box_dispose(GObject* obj);

/* Local callbacks. */
static void onShowSizeChanged(GtkToggleButton *toggle, VisuUiBox *box);
static void onXPosChanged(GtkSpinButton *spin, VisuUiBox *box);
static void onYPosChanged(GtkSpinButton *spin, VisuUiBox *box);

G_DEFINE_TYPE_WITH_CODE(VisuUiBox, visu_ui_box, VISU_UI_TYPE_LINE,
                        G_ADD_PRIVATE(VisuUiBox))

static void visu_ui_box_class_init(VisuUiBoxClass *klass)
{
  DBG_fprintf(stderr, "Ui Box: creating the class of the widget.\n");
  DBG_fprintf(stderr, "                     - adding new signals ;\n");
  /**
   * VisuUiBox::show-size-changed:
   * @box: the #VisuUiBox that emits the signal ;
   * @used: TRUE if the box follow the box basis-set.
   *
   * This signal is emitted when the box legend check box is changed.
   *
   * Since: 3.8
   */
  signals[SHOW_SIZE_SIGNAL] =
    g_signal_new ("show-size-changed", G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		  0, NULL, NULL, g_cclosure_marshal_VOID__BOOLEAN,
		  G_TYPE_NONE, 1, G_TYPE_BOOLEAN);
  /**
   * VisuUiBox::x-pos-changed:
   * @box: the #VisuUiBox that emits the signal ;
   * @pos: the new position.
   *
   * This signal is emitted when the box legend rendering position is changed.
   *
   * Since: 3.8
   */
  signals[X_POS_SIGNAL] =
    g_signal_new ("x-pos-changed", G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		  0, NULL, NULL, g_cclosure_marshal_VOID__DOUBLE,
		  G_TYPE_NONE, 1, G_TYPE_DOUBLE);
  /**
   * VisuUiBox::y-pos-changed:
   * @box: the #VisuUiBox that emits the signal ;
   * @pos: the new position.
   *
   * This signal is emitted when the box legend rendering position is changed.
   *
   * Since: 3.8
   */
  signals[Y_POS_SIGNAL] =
    g_signal_new ("y-pos-changed", G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		  0, NULL, NULL, g_cclosure_marshal_VOID__DOUBLE,
		  G_TYPE_NONE, 1, G_TYPE_DOUBLE);

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose = visu_ui_box_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_ui_box_finalize;
}
static void visu_ui_box_dispose(GObject *obj)
{
  DBG_fprintf(stderr, "Ui Box: dispose object %p.\n", (gpointer)obj);

  if (VISU_UI_BOX(obj)->priv->dispose_has_run)
    return;

  visu_ui_box_bind(VISU_UI_BOX(obj), (VisuGlExtBox*)0);
  visu_ui_box_bindLegend(VISU_UI_BOX(obj), (VisuGlExtBoxLegend*)0);

  VISU_UI_BOX(obj)->priv->dispose_has_run = TRUE;
  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_box_parent_class)->dispose(obj);
}
static void visu_ui_box_finalize(GObject *obj)
{
  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Ui Box: finalize object %p.\n", (gpointer)obj);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_box_parent_class)->finalize(obj);

  DBG_fprintf(stderr, " | freeing ... OK.\n");
}

static void visu_ui_box_init(VisuUiBox *obj)
{
  GtkWidget *vbox, *hbox;

  DBG_fprintf(stderr, "Extension Box: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = visu_ui_box_get_instance_private(obj);
  obj->priv->dispose_has_run = FALSE;

  obj->priv->model = (VisuGlExtBox*)0;
  obj->priv->legend = (VisuGlExtBoxLegend*)0;

  vbox = visu_ui_line_getOptionBox(VISU_UI_LINE(obj));
  obj->priv->checkBox = gtk_check_button_new_with_mnemonic(_("Show box _lengths"));
  gtk_box_pack_start(GTK_BOX(vbox), obj->priv->checkBox, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(obj->priv->checkBox), "toggled",
		   G_CALLBACK(onShowSizeChanged), (gpointer)obj);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(_("x pos.")), TRUE, TRUE, 0);
  obj->priv->spinXPos = gtk_spin_button_new_with_range(0., 1., 0.1);
  gtk_box_pack_start(GTK_BOX(hbox), obj->priv->spinXPos, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(obj->priv->spinXPos), "value-changed",
		   G_CALLBACK(onXPosChanged), (gpointer)obj);
  gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(_("y pos.")), TRUE, TRUE, 0);
  obj->priv->spinYPos = gtk_spin_button_new_with_range(0., 1., 0.1);
  gtk_box_pack_start(GTK_BOX(hbox), obj->priv->spinYPos, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(obj->priv->spinYPos), "value-changed",
		   G_CALLBACK(onYPosChanged), (gpointer)obj);
/*   label = gtk_label_new(_("Draw basis set:")); */
/*   gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5); */
/*   gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0); */
  /* checkBasis = gtk_check_button_new(); */
/*   gtk_box_pack_start(GTK_BOX(hbox), checkBasis, FALSE, FALSE, 0); */

  gtk_widget_show_all(GTK_WIDGET(vbox));
}


/**
 * visu_ui_box_new:
 *
 * Creates a new #VisuUiBox to allow to setup box rendering characteristics.
 *
 * Since: 3.8
 *
 * Returns: a pointer to the newly created widget.
 */
GtkWidget* visu_ui_box_new()
{
  DBG_fprintf(stderr,"Ui Box: new object.\n");
  
  return GTK_WIDGET(g_object_new(VISU_TYPE_UI_BOX, "label", _("Bounding box"), NULL));
}

/**
 * visu_ui_box_bind:
 * @box: a #VisuUiBox object.
 * @model: (transfer full): a #VisuGlExtBox object.
 *
 * Bind the properties of @model to be displayed by @box.
 *
 * Since: 3.8
 **/
void visu_ui_box_bind(VisuUiBox *box, VisuGlExtBox *model)
{
  g_return_if_fail(VISU_IS_UI_BOX(box));

  if (box->priv->model == model)
    return;

  visu_ui_line_bind(VISU_UI_LINE(box), VISU_GL_EXT_LINED(model));
  if (box->priv->model)
    g_object_unref(G_OBJECT(box->priv->model));
  box->priv->model = model;
  if (model)
    g_object_ref(G_OBJECT(model));
}
/**
 * visu_ui_box_bindLegend:
 * @box: a #VisuUiBox object.
 * @legend: (transfer full): a #VisuGlExtBoxLegend object.
 *
 * Bind the properties of @model to be displayed by @legend.
 *
 * Since: 3.8
 **/
void visu_ui_box_bindLegend(VisuUiBox *box, VisuGlExtBoxLegend *legend)
{
  g_return_if_fail(VISU_IS_UI_BOX(box));

  if (box->priv->legend == legend)
    return;

  if (box->priv->legend)
    {
      g_object_unref(G_OBJECT(box->priv->bind_active));
      g_object_unref(G_OBJECT(box->priv->bind_xpos));
      g_object_unref(G_OBJECT(box->priv->bind_ypos));
      g_object_unref(G_OBJECT(box->priv->legend));
    }
  box->priv->legend = legend;
  if (legend)
    {
      g_object_ref(G_OBJECT(legend));
      box->priv->bind_active = g_object_bind_property(legend, "active",
                                                      box->priv->checkBox, "active",
                                                      G_BINDING_BIDIRECTIONAL |
                                                      G_BINDING_SYNC_CREATE);
      box->priv->bind_xpos = g_object_bind_property(legend, "x-pos",
                                                     box->priv->spinXPos, "value",
                                                     G_BINDING_BIDIRECTIONAL |
                                                     G_BINDING_SYNC_CREATE);
      box->priv->bind_ypos = g_object_bind_property(legend, "y-pos",
                                                     box->priv->spinYPos, "value",
                                                     G_BINDING_BIDIRECTIONAL |
                                                     G_BINDING_SYNC_CREATE);
    }
}

static void onShowSizeChanged(GtkToggleButton *toggle, VisuUiBox *box)
{
  g_signal_emit(G_OBJECT(box), signals[SHOW_SIZE_SIGNAL],
		0, gtk_toggle_button_get_active(toggle), NULL);
}
static void onXPosChanged(GtkSpinButton *spin, VisuUiBox *box)
{
  g_signal_emit(G_OBJECT(box), signals[X_POS_SIGNAL],
		0, gtk_spin_button_get_value(spin), NULL);
}
static void onYPosChanged(GtkSpinButton *spin, VisuUiBox *box)
{
  g_signal_emit(G_OBJECT(box), signals[Y_POS_SIGNAL],
		0, gtk_spin_button_get_value(spin), NULL);
}
