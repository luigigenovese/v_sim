/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef UI_ATOMIC_H
#define UI_ATOMIC_H

#include <gtk/gtk.h>

#include <renderingMethods/iface_nodeArrayRenderer.h>

/**
 * VISU_TYPE_UI_ATOMIC:
 *
 * return the type of #VisuUiAtomic.
 *
 * Since: 3.8
 */
#define VISU_TYPE_UI_ATOMIC	     (visu_ui_atomic_get_type ())
/**
 * VISU_UI_ATOMIC:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuUiAtomic type.
 *
 * Since: 3.8
 */
#define VISU_UI_ATOMIC(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_UI_ATOMIC, VisuUiAtomic))
/**
 * VISU_UI_ATOMIC_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuUiAtomicClass.
 *
 * Since: 3.8
 */
#define VISU_UI_ATOMIC_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_UI_ATOMIC, VisuUiAtomicClass))
/**
 * VISU_IS_UI_ATOMIC:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuUiAtomic object.
 *
 * Since: 3.8
 */
#define VISU_IS_UI_ATOMIC(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_UI_ATOMIC))
/**
 * VISU_IS_UI_ATOMIC_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuUiAtomicClass class.
 *
 * Since: 3.8
 */
#define VISU_IS_UI_ATOMIC_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_UI_ATOMIC))
/**
 * VISU_UI_ATOMIC_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.8
 */
#define VISU_UI_ATOMIC_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_UI_ATOMIC, VisuUiAtomicClass))

typedef struct _VisuUiAtomic        VisuUiAtomic;
typedef struct _VisuUiAtomicPrivate VisuUiAtomicPrivate;
typedef struct _VisuUiAtomicClass   VisuUiAtomicClass;

struct _VisuUiAtomic
{
  GtkBox parent;

  VisuUiAtomicPrivate *priv;
};

struct _VisuUiAtomicClass
{
  GtkBoxClass parent;
};

/**
 * visu_ui_atomic_get_type:
 *
 * This method returns the type of #VisuUiAtomic, use
 * VISU_TYPE_UI_ATOMIC instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuUiAtomic.
 */
GType visu_ui_atomic_get_type(void);

GtkWidget* visu_ui_atomic_new(VisuNodeArrayRenderer *renderer);
void visu_ui_atomic_bind(VisuUiAtomic *atomic, GList *eleList);

#endif
