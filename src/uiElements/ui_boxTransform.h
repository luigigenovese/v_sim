/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2014)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2014)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef UI_BOX_TRANSFORM_H
#define UI_BOX_TRANSFORM_H

#include <iface_pointset.h>
#include <gtk/gtk.h>
#include <extensions/box.h>

/**
 * VISU_TYPE_UI_BOX_TRANSFORM:
 *
 * return the type of #VisuUiBoxTransform.
 *
 * Since: 3.8
 */
#define VISU_TYPE_UI_BOX_TRANSFORM	     (visu_ui_box_transform_get_type ())
/**
 * VISU_UI_BOX_TRANSFORM:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuUiBoxTransform type.
 *
 * Since: 3.8
 */
#define VISU_UI_BOX_TRANSFORM(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_UI_BOX_TRANSFORM, VisuUiBoxTransform))
/**
 * VISU_UI_BOX_TRANSFORM_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuUiBoxTransformClass.
 *
 * Since: 3.8
 */
#define VISU_UI_BOX_TRANSFORM_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_UI_BOX_TRANSFORM, VisuUiBoxTransformClass))
/**
 * VISU_IS_UI_BOX_TRANSFORM:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuUiBoxTransform object.
 *
 * Since: 3.8
 */
#define VISU_IS_UI_BOX_TRANSFORM(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_UI_BOX_TRANSFORM))
/**
 * VISU_IS_UI_BOX_TRANSFORM_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuUiBoxTransformClass class.
 *
 * Since: 3.8
 */
#define VISU_IS_UI_BOX_TRANSFORM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_UI_BOX_TRANSFORM))
/**
 * VISU_UI_BOX_TRANSFORM_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.8
 */
#define VISU_UI_BOX_TRANSFORM_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_UI_BOX_TRANSFORM, VisuUiBoxTransformClass))

typedef struct _VisuUiBoxTransform        VisuUiBoxTransform;
typedef struct _VisuUiBoxTransformPrivate VisuUiBoxTransformPrivate;
typedef struct _VisuUiBoxTransformClass   VisuUiBoxTransformClass;

struct _VisuUiBoxTransform
{
  GtkVBox parent;

  VisuUiBoxTransformPrivate *priv;
};

struct _VisuUiBoxTransformClass
{
  GtkVBoxClass parent;
};

/**
 * visu_ui_box_transform_get_type:
 *
 * This method returns the type of #VisuUiBoxTransform, use
 * VISU_TYPE_UI_BOX_TRANSFORM instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuUiBoxTransform.
 */
GType visu_ui_box_transform_get_type(void);

GtkWidget* visu_ui_box_transform_new();
void visu_ui_box_transform_bind(VisuUiBoxTransform *box_transform, VisuPointset *model);
void visu_ui_box_transform_bindGlExtBox(VisuUiBoxTransform *box_transform,
                                        VisuGlExtBox *model);

#endif
