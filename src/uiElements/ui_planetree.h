/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2015)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2015)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_UI_PLANE_LIST_H
#define VISU_UI_PLANE_LIST_H

#include <glib.h>
#include <glib-object.h>

#include <gtk/gtk.h>

#include <extraFunctions/planeset.h>

/***************/
/* Public part */
/***************/

/**
 * VISU_TYPE_UI_PLANE_LIST:
 *
 * return the type of #VisuUiPlaneList.
 */
#define VISU_TYPE_UI_PLANE_LIST	     (visu_ui_plane_list_get_type ())
/**
 * VISU_UI_PLANE_LIST:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuUiPlaneList type.
 */
#define VISU_UI_PLANE_LIST(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_UI_PLANE_LIST, VisuUiPlaneList))
/**
 * VISU_UI_PLANE_LIST_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuUiPlaneListClass.
 */
#define VISU_UI_PLANE_LIST_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_UI_PLANE_LIST, VisuUiPlaneListClass))
/**
 * VISU_IS_UI_PLANE_LIST_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuUiPlaneList object.
 */
#define VISU_IS_UI_PLANE_LIST_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_UI_PLANE_LIST))
/**
 * VISU_IS_UI_PLANE_LIST_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuUiPlaneListClass class.
 */
#define VISU_IS_UI_PLANE_LIST_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_UI_PLANE_LIST))
/**
 * VISU_UI_PLANE_LIST_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_UI_PLANE_LIST_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_UI_PLANE_LIST, VisuUiPlaneListClass))


/**
 * VisuUiPlaneListPrivate:
 * 
 * Private data for #VisuUiPlaneList objects.
 */
typedef struct _VisuUiPlaneListPrivate VisuUiPlaneListPrivate;

/**
 * VisuUiPlaneList:
 * 
 * Common name to refer to a #_VisuUiPlaneList.
 */
typedef struct _VisuUiPlaneList VisuUiPlaneList;
struct _VisuUiPlaneList
{
  GtkListStore parent;

  VisuUiPlaneListPrivate *priv;
};

/**
 * VisuUiPlaneListClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuUiPlaneListClass.
 */
typedef struct _VisuUiPlaneListClass VisuUiPlaneListClass;
struct _VisuUiPlaneListClass
{
  GtkListStoreClass parent;
};

/**
 * visu_ui_plane_list_get_type:
 *
 * This method returns the type of #VisuUiPlaneList, use
 * VISU_TYPE_UI_PLANE_LIST instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuUiPlaneList.
 */
GType visu_ui_plane_list_get_type(void);

/**
 * VisuUiPlaneListColumnId:
 * @VISU_UI_PLANE_LIST_POINTER: the pointer to the #VisuPlane object.
 * @VISU_UI_PLANE_LIST_NOTIFY: a handler of the VisuPlane::notify signal.
 * @VISU_UI_PLANE_LIST_MODE: cache the hidding side of the plane when
 * plane is not hiding.
 * @VISU_UI_PLANE_LIST_N_COLUMNS: the number of columns.
 *
 * Thesse are the description of the columns stored in the object.
 */
typedef enum
  {
    VISU_UI_PLANE_LIST_POINTER,
    VISU_UI_PLANE_LIST_NOTIFY,
    VISU_UI_PLANE_LIST_MODE,
    VISU_UI_PLANE_LIST_N_COLUMNS
  } VisuUiPlaneListColumnId;

VisuUiPlaneList* visu_ui_plane_list_new();

VisuPlaneSet* visu_ui_plane_list_getModel(const VisuUiPlaneList *list);
gboolean visu_ui_plane_list_setModel(VisuUiPlaneList *list, VisuPlaneSet *set);

GtkWidget* visu_ui_plane_list_getView(VisuUiPlaneList *list);
GtkBox* visu_ui_plane_list_getControls(VisuUiPlaneList *list);

VisuPlane* visu_ui_plane_list_getSelection(const VisuUiPlaneList *list);
VisuPlane* visu_ui_plane_list_getAt(VisuUiPlaneList *list, guint i);

#endif
