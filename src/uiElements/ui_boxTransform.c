/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2014)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2014)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "ui_boxTransform.h"

#include <support.h>
#include <visu_tools.h>

#include <coreTools/toolMatrix.h>
#include <extraGtkFunctions/gtk_stippleComboBoxWidget.h>
#include <extraGtkFunctions/gtk_colorComboBoxWidget.h>

/**
 * SECTION:ui_boxTransform
 * @short_description: Defines a widget to setup box transformations.
 *
 * <para>A set of widgets to setup the transformation applied to a box
 * (translations, expansions...).</para>
 */

/**
 * VisuUiBoxTransformClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuUiBoxTransformClass structure.
 *
 * Since: 3.8
 */
/**
 * VisuUiBoxTransform:
 *
 * An opaque structure.
 *
 * Since: 3.8
 */
/**
 * VisuUiBoxTransformPrivate:
 *
 * Private fields for #VisuUiBoxTransform objects.
 *
 * Since: 3.8
 */
struct _VisuUiBoxTransformPrivate
{
  gboolean dispose_has_run;

  GtkWidget *checkAllowTranslations, *checkInBox, *checkAllowExpand;
  GtkWidget *spinTransXYZ[3], *spinExpandXYZ[3];
  GtkWidget *stippleExpandBox, *colorExpandBox;
  GtkWidget *labelBc, *warnBc;
  GtkWidget *comboUnit, *comboHide;

  VisuPointset *pointset;
  gulong sig_box;
  VisuBox *box;
  GBinding *bind_bc, *bind_bcWarn;
  GBinding *bind_trans[3], *bind_allowTrans, *bind_boxTrans;
  GBinding *bind_expand[3], *bind_allowExpand;
  GBinding *bind_units, *bind_hide;
  gulong sig_boundary;

  VisuGlExtBox *glBox;
  GBinding *bind_stipple, *bind_color;
};

enum
  {
    PROP_0,
    POINTSET_PROP,
    GL_BOX_PROP,
    N_PROP
  };
static GParamSpec *properties[N_PROP];

static void visu_ui_box_transform_finalize(GObject* obj);
static void visu_ui_box_transform_dispose(GObject* obj);
static void visu_ui_box_transform_get_property(GObject* obj, guint property_id,
                                               GValue *value, GParamSpec *pspec);
static void visu_ui_box_transform_set_property(GObject* obj, guint property_id,
                                               const GValue *value, GParamSpec *pspec);

G_DEFINE_TYPE_WITH_CODE(VisuUiBoxTransform, visu_ui_box_transform, GTK_TYPE_BOX,
                        G_ADD_PRIVATE(VisuUiBoxTransform))

static void visu_ui_box_transform_class_init(VisuUiBoxTransformClass *klass)
{
  DBG_fprintf(stderr, "Ui BoxTransform: creating the class of the widget.\n");
  DBG_fprintf(stderr, "                     - adding new signals ;\n");

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose = visu_ui_box_transform_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_ui_box_transform_finalize;
  G_OBJECT_CLASS(klass)->set_property = visu_ui_box_transform_set_property;
  G_OBJECT_CLASS(klass)->get_property = visu_ui_box_transform_get_property;

  /**
   * VisuUiBoxTransform::pointset:
   *
   * Store the #VisuPointset model.
   *
   * Since: 3.8
   */
  properties[POINTSET_PROP] = g_param_spec_object("pointset", "Pointset",
                                                  "Pointset to transform",
                                                  VISU_TYPE_POINTSET, G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), POINTSET_PROP,
				  properties[POINTSET_PROP]);
  /**
   * VisuUiBoxTransform::gl-box:
   *
   * Store the rendering box object of box.
   *
   * Since: 3.8
   */
  properties[GL_BOX_PROP] = g_param_spec_object("gl-box", "OpenGL box object",
                                                "rendering object used for box",
                                                VISU_TYPE_GL_EXT_BOX, G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), GL_BOX_PROP,
				  properties[GL_BOX_PROP]);
}
static void visu_ui_box_transform_dispose(GObject *obj)
{
  DBG_fprintf(stderr, "Ui BoxTransform: dispose object %p.\n", (gpointer)obj);

  if (VISU_UI_BOX_TRANSFORM(obj)->priv->dispose_has_run)
    return;

  visu_ui_box_transform_bind(VISU_UI_BOX_TRANSFORM(obj), (VisuPointset*)0);
  visu_ui_box_transform_bindGlExtBox(VISU_UI_BOX_TRANSFORM(obj), (VisuGlExtBox*)0);

  VISU_UI_BOX_TRANSFORM(obj)->priv->dispose_has_run = TRUE;
  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_box_transform_parent_class)->dispose(obj);
}
static void visu_ui_box_transform_finalize(GObject *obj)
{
  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Ui BoxTransform: finalize object %p.\n", (gpointer)obj);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_box_transform_parent_class)->finalize(obj);

  DBG_fprintf(stderr, " | freeing ... OK.\n");
}
static void visu_ui_box_transform_get_property(GObject* obj, guint property_id,
                                               GValue *value, GParamSpec *pspec)
{
  VisuUiBoxTransform *self = VISU_UI_BOX_TRANSFORM(obj);

  DBG_fprintf(stderr, "Ui BoxTransform: get property '%s'.\n",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case POINTSET_PROP:
      g_value_set_object(value, self->priv->pointset);
      break;
    case GL_BOX_PROP:
      g_value_set_object(value, self->priv->glBox);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_ui_box_transform_set_property(GObject* obj, guint property_id,
                                               const GValue *value, GParamSpec *pspec)
{
  VisuUiBoxTransform *self = VISU_UI_BOX_TRANSFORM(obj);

  DBG_fprintf(stderr, "Ui BoxTransform: set property '%s'.\n",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case POINTSET_PROP:
      visu_ui_box_transform_bind(self, VISU_POINTSET(g_value_get_object(value)));
      break;
    case GL_BOX_PROP:
      visu_ui_box_transform_bindGlExtBox(self, VISU_GL_EXT_BOX(g_value_get_object(value)));
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}

static gboolean setPeriodicityWarning(GBinding *bind _U_, const GValue *source,
                                      GValue *target, gpointer user_data _U_)
{
  g_value_set_boolean(target, g_value_get_uint(source) == VISU_BOX_FREE);
  return TRUE;
}
static gboolean setPeriodicity(GBinding *bind _U_, const GValue *source,
                               GValue *target, gpointer user_data _U_)
{
  gchar *lbl;

  switch (g_value_get_uint(source))
    {
    case VISU_BOX_FREE:
      lbl = g_markup_printf_escaped("<i>%s</i>", _("non periodic data"));
      g_value_take_string(target, lbl);
      return TRUE;
    case VISU_BOX_WIRE_X:
      g_value_set_static_string(target, _("(wire X)"));
      return TRUE;
    case VISU_BOX_WIRE_Y:
      g_value_set_static_string(target, _("(wire Y)"));
      return TRUE;
    case VISU_BOX_WIRE_Z:
      g_value_set_static_string(target, _("(wire Z)"));
      return TRUE;
    case VISU_BOX_SURFACE_XY:
      g_value_set_static_string(target, _("(surface XY)"));
      return TRUE;
    case VISU_BOX_SURFACE_YZ:
      g_value_set_static_string(target, _("(surface YZ)"));
      return TRUE;
    case VISU_BOX_SURFACE_ZX:
      g_value_set_static_string(target, _("(surface ZX)"));
      return TRUE;
    case VISU_BOX_PERIODIC:
      g_value_set_static_string(target, _("(periodic)"));
      return TRUE;
    default:
      g_value_set_static_string(target, _("unknown periodicity"));
      return TRUE;
    }
  return TRUE;
}
static void setSensitive(VisuUiBoxTransform *obj)
{
  gboolean expanded;
  VisuBoxBoundaries bc;
  
  expanded = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(obj->priv->checkAllowExpand));
  bc = (obj->priv->box) ? visu_box_getBoundary(obj->priv->box) : VISU_BOX_PERIODIC;
  /* For the translations. */
  gtk_widget_set_sensitive(obj->priv->checkInBox,
                           !expanded && obj->priv->box && (bc != VISU_BOX_FREE));
  gtk_widget_set_sensitive(obj->priv->checkAllowTranslations,
                           !expanded && obj->priv->box && (bc != VISU_BOX_FREE));
  gtk_widget_set_sensitive(obj->priv->spinTransXYZ[0],
                           !expanded && obj->priv->box && (bc & TOOL_XYZ_MASK_X));
  gtk_widget_set_sensitive(obj->priv->spinTransXYZ[1],
                           !expanded && obj->priv->box && (bc & TOOL_XYZ_MASK_Y));
  gtk_widget_set_sensitive(obj->priv->spinTransXYZ[2],
                           !expanded && obj->priv->box && (bc & TOOL_XYZ_MASK_Z));
  /* For the extensions. */
  gtk_widget_set_sensitive(obj->priv->checkAllowExpand,
                           obj->priv->box && (bc != VISU_BOX_FREE));
  gtk_widget_set_sensitive(obj->priv->spinExpandXYZ[0],
                           obj->priv->box && (bc & TOOL_XYZ_MASK_X));
  gtk_widget_set_sensitive(obj->priv->spinExpandXYZ[1],
                           obj->priv->box && (bc & TOOL_XYZ_MASK_Y));
  gtk_widget_set_sensitive(obj->priv->spinExpandXYZ[2],
                           obj->priv->box && (bc & TOOL_XYZ_MASK_Z));
}
static void visu_ui_box_transform_init(VisuUiBoxTransform *obj)
{
  GtkWidget *hbox, *label, *vbox;
  guint i;
#define X_LABEL _("dx:")
#define Y_LABEL _("dy:")
#define Z_LABEL _("dz:")
  char *xyz[3];
  const gchar **units;
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips;

  tooltips = gtk_tooltips_new ();
#endif

  DBG_fprintf(stderr, "Extension BoxTransform: initializing a new object (%p).\n",
	      (gpointer)obj);

  gtk_orientable_set_orientation(GTK_ORIENTABLE(obj), GTK_ORIENTATION_VERTICAL);
  
  obj->priv = visu_ui_box_transform_get_instance_private(obj);
  obj->priv->dispose_has_run = FALSE;

  obj->priv->box = (VisuBox*)0;
  obj->priv->glBox = (VisuGlExtBox*)0;

  /**************************/
  /* The periodicity stuff. */
  /**************************/
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(obj), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("<b>Periodic operations</b>"));
  gtk_widget_set_name(label, "label_head");
  gtk_label_set_xalign(GTK_LABEL(label), 0.);
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);

  obj->priv->labelBc = gtk_label_new("");
  gtk_box_pack_end(GTK_BOX(hbox), obj->priv->labelBc, FALSE, FALSE, 0);
  gtk_label_set_use_markup(GTK_LABEL(obj->priv->labelBc), TRUE);
  obj->priv->warnBc = gtk_image_new_from_icon_name("dialog-warning",
                                                   GTK_ICON_SIZE_MENU);
  gtk_widget_set_no_show_all(obj->priv->warnBc, TRUE);
  gtk_box_pack_end(GTK_BOX(hbox), obj->priv->warnBc, FALSE, FALSE, 0);

  vbox = gtk_vbox_new(FALSE, 2);
  gtk_widget_set_margin_start(vbox, 15);
  gtk_box_pack_start(GTK_BOX(obj), vbox, FALSE, FALSE, 0);
                         
  /* The translations. */
  hbox = gtk_hbox_new(FALSE, 0);
  g_object_bind_property(obj, "pointset", hbox, "sensitive",
                         G_BINDING_SYNC_CREATE);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  obj->priv->checkAllowTranslations =
    gtk_check_button_new_with_mnemonic(_("_Translations"));
  gtk_widget_set_tooltip_text(obj->priv->checkAllowTranslations,
			      _("Translations are given in box coordinates."));
  gtk_box_pack_start(GTK_BOX(hbox),
                     obj->priv->checkAllowTranslations, TRUE, TRUE, 0);
  obj->priv->checkInBox =
    gtk_check_button_new_with_mnemonic(_("_Put in the box"));
  gtk_widget_set_tooltip_text(obj->priv->checkInBox,
			      _("Nodes are automatically translated back into the bounding box."));
  gtk_box_pack_start(GTK_BOX(hbox),
                     obj->priv->checkInBox, TRUE, TRUE, 0);

  hbox = gtk_hbox_new(FALSE, 2);
  g_object_bind_property(obj, "pointset", hbox, "sensitive",
                         G_BINDING_SYNC_CREATE);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

  xyz[0] = X_LABEL;
  xyz[1] = Y_LABEL;
  xyz[2] = Z_LABEL;
  for (i = 0; i < 3; i++)
    {
      label = gtk_label_new(xyz[i]);
      gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
      gtk_label_set_xalign(GTK_LABEL(label), 1.);

      obj->priv->spinTransXYZ[i] = gtk_spin_button_new_with_range(-1, 1, 0.05);
      gtk_spin_button_set_value(GTK_SPIN_BUTTON(obj->priv->spinTransXYZ[i]), 0.);
      gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(obj->priv->spinTransXYZ[i]), TRUE);
      gtk_box_pack_start(GTK_BOX(hbox), obj->priv->spinTransXYZ[i], FALSE, FALSE, 0);
    }

  /* The replication. */
  hbox = gtk_hbox_new(FALSE, 2);
  g_object_bind_property(obj, "pointset", hbox, "sensitive",
                         G_BINDING_SYNC_CREATE);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  obj->priv->checkAllowExpand =
    gtk_check_button_new_with_mnemonic(_("_Expand nodes"));
  gtk_widget_set_tooltip_text(obj->priv->checkAllowExpand,
			      _("The size of the expansion is given in box coordinates."
				" Nodes are automatically translated back into the new"
				" defined area. The drawn bounding box is kept to the"
				" original size."));
  gtk_box_pack_start(GTK_BOX(hbox), obj->priv->checkAllowExpand, TRUE, TRUE, 0);
  g_signal_connect_object(G_OBJECT(obj->priv->checkAllowExpand), "toggled",
                          G_CALLBACK(setSensitive), (gpointer)obj, G_CONNECT_SWAPPED);
  /* The rendering parameters. */
  label = gtk_label_new(_("param.:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  /* The stipple pattern. */
  obj->priv->stippleExpandBox = visu_ui_stipple_combobox_new();
  gtk_box_pack_start(GTK_BOX(hbox), obj->priv->stippleExpandBox, FALSE, FALSE, 0);
  /* The color widget. */
  obj->priv->colorExpandBox = visu_ui_color_combobox_new(TRUE);
  visu_ui_color_combobox_setPrintValues(VISU_UI_COLOR_COMBOBOX(obj->priv->colorExpandBox), FALSE);
  gtk_box_pack_start(GTK_BOX(hbox), obj->priv->colorExpandBox, FALSE, FALSE, 0);

  hbox = gtk_hbox_new(FALSE, 2);
  g_object_bind_property(obj, "pointset", hbox, "sensitive",
                         G_BINDING_SYNC_CREATE);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

  xyz[0] = X_LABEL;
  xyz[1] = Y_LABEL;
  xyz[2] = Z_LABEL;
  for (i = 0; i < 3; i++)
    {
      label = gtk_label_new(xyz[i]);
      gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
      gtk_label_set_xalign(GTK_LABEL(label), 1.);

      obj->priv->spinExpandXYZ[i] = gtk_spin_button_new_with_range(0, 5, 0.05);
      gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(obj->priv->spinExpandXYZ[i]), TRUE);
      gtk_box_pack_start(GTK_BOX(hbox), obj->priv->spinExpandXYZ[i], FALSE, FALSE, 0);
    }

  /********************/
  /* The units stuff. */
  /********************/
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(obj), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("<b>Box settings</b>"));
  gtk_widget_set_name(label, "label_head");
  gtk_label_set_xalign(GTK_LABEL(label), 0.);
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);

  hbox = gtk_hbox_new(FALSE, 2);
  gtk_widget_set_margin_start(hbox, 15);
  g_object_bind_property(obj, "pointset", hbox, "sensitive",
                         G_BINDING_SYNC_CREATE);
  gtk_box_pack_start(GTK_BOX(obj), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("Set the unit of the file:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

  obj->priv->comboUnit = gtk_combo_box_text_new();
  units = tool_physic_getUnitNames();
  for (i = 0; units[i]; i++)
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(obj->priv->comboUnit),
                              (const gchar*)0, units[i]);
  gtk_box_pack_start(GTK_BOX(hbox), obj->priv->comboUnit, FALSE, FALSE, 0);

  hbox = gtk_hbox_new(FALSE, 2);
  gtk_widget_set_margin_start(hbox, 15);
  g_object_bind_property(obj, "pointset", hbox, "sensitive",
                         G_BINDING_SYNC_CREATE);
  gtk_box_pack_start(GTK_BOX(obj), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("Hide nodes with respect to box:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

  obj->priv->comboHide = gtk_combo_box_text_new();
  gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(obj->priv->comboHide),
                            (const gchar*)0, _("never"));
  gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(obj->priv->comboHide),
                            (const gchar*)0, _("outside"));
  gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(obj->priv->comboHide),
                            (const gchar*)0, _("inside"));
  gtk_box_pack_start(GTK_BOX(hbox), obj->priv->comboHide, FALSE, FALSE, 0);

  gtk_widget_show_all(GTK_WIDGET(obj));
}


/**
 * visu_ui_box_transform_new:
 *
 * Creates a new #VisuUiBoxTransform to allow to setup box_transform rendering characteristics.
 *
 * Since: 3.8
 *
 * Returns: a pointer to the newly created widget.
 */
GtkWidget* visu_ui_box_transform_new()
{
  DBG_fprintf(stderr,"Ui BoxTransform: new object.\n");
  
  return GTK_WIDGET(g_object_new(VISU_TYPE_UI_BOX_TRANSFORM,
                                 "orientation", GTK_ORIENTATION_VERTICAL, NULL));
}

static gboolean fromExpandToSpin(GBinding *bind, const GValue *source,
                                 GValue *target, gpointer data)
{
  float *expand;
  VisuUiBoxTransform *boxT = VISU_UI_BOX_TRANSFORM(data);

  expand = (float*)g_value_get_boxed(source);
  if (g_binding_get_target(bind) == (gpointer)boxT->priv->spinExpandXYZ[0])
    g_value_set_double(target, (double)expand[0]);
  else if (g_binding_get_target(bind) == (gpointer)boxT->priv->spinExpandXYZ[1])
    g_value_set_double(target, (double)expand[1]);
  else if (g_binding_get_target(bind) == (gpointer)boxT->priv->spinExpandXYZ[2])
    g_value_set_double(target, (double)expand[2]);

  return TRUE;
}
static gboolean fromSpinToExpand(GBinding *bind, const GValue *source,
                                 GValue *target, gpointer data)
{
  float expand[3];
  VisuUiBoxTransform *boxT = VISU_UI_BOX_TRANSFORM(data);

  visu_box_getExtension(VISU_BOX(g_binding_get_source(bind)), expand);
  if (g_binding_get_target(bind) == (gpointer)boxT->priv->spinExpandXYZ[0])
    expand[0] = (float)g_value_get_double(source);
  else if (g_binding_get_target(bind) == (gpointer)boxT->priv->spinExpandXYZ[1])
    expand[1] = (float)g_value_get_double(source);
  else if (g_binding_get_target(bind) == (gpointer)boxT->priv->spinExpandXYZ[2])
    expand[2] = (float)g_value_get_double(source);
  g_value_set_boxed(target, expand);

  return TRUE;
}
static void _bindBox(VisuUiBoxTransform *box_transform, VisuBox *box, VisuPointset *model _U_)
{
  int i;

  if (box_transform->priv->box == box)
    return;

  if (box_transform->priv->box)
    {
      g_object_unref(G_OBJECT(box_transform->priv->bind_allowExpand));
      g_object_unref(G_OBJECT(box_transform->priv->bind_expand[0]));
      g_object_unref(G_OBJECT(box_transform->priv->bind_expand[1]));
      g_object_unref(G_OBJECT(box_transform->priv->bind_expand[2]));
      g_object_unref(G_OBJECT(box_transform->priv->bind_bc));
      g_object_unref(G_OBJECT(box_transform->priv->bind_bcWarn));
      g_signal_handler_disconnect(G_OBJECT(box_transform->priv->box),
                                  box_transform->priv->sig_boundary);
      g_object_unref(G_OBJECT(box_transform->priv->bind_units));
      g_object_unref(G_OBJECT(box_transform->priv->bind_hide));
      g_object_unref(G_OBJECT(box_transform->priv->box));
    }
  box_transform->priv->box = box;
  if (box)
    {
      g_object_ref(G_OBJECT(box));
      box_transform->priv->bind_allowExpand =
        g_object_bind_property(box, "use-expansion",
                               box_transform->priv->checkAllowExpand, "active",
                               G_BINDING_BIDIRECTIONAL | G_BINDING_SYNC_CREATE);
      for (i = 0; i < 3; i++)
        {
          box_transform->priv->bind_expand[i] =
            g_object_bind_property_full(box, "expansion",
                                        box_transform->priv->spinExpandXYZ[i], "value",
                                        G_BINDING_BIDIRECTIONAL | G_BINDING_SYNC_CREATE,
                                        fromExpandToSpin, fromSpinToExpand,
                                        box_transform, (GDestroyNotify)0);
        }
      box_transform->priv->sig_boundary =
        g_signal_connect_object(G_OBJECT(box), "notify::boundary",
                                G_CALLBACK(setSensitive),
                                box_transform, G_CONNECT_SWAPPED);
      setSensitive(box_transform);
      box_transform->priv->bind_bc =
        g_object_bind_property_full(box, "boundary", box_transform->priv->labelBc, "label",
                                    G_BINDING_SYNC_CREATE, setPeriodicity, NULL,
                                    (gpointer)0, (GDestroyNotify)0);
      box_transform->priv->bind_bcWarn =
      g_object_bind_property_full(box, "boundary", box_transform->priv->warnBc, "visible",
                                  G_BINDING_SYNC_CREATE, setPeriodicityWarning, NULL,
                                  (gpointer)0, (GDestroyNotify)0);
      box_transform->priv->bind_units =
      g_object_bind_property(box, "units", box_transform->priv->comboUnit, "active",
                             G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
      box_transform->priv->bind_hide =
      g_object_bind_property(box, "hidding-scheme", box_transform->priv->comboHide, "active",
                             G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
    }
}
static gboolean fromTransToSpin(GBinding *bind, const GValue *source,
                                 GValue *target, gpointer data)
{
  gfloat *box;
  VisuUiBoxTransform *boxT = VISU_UI_BOX_TRANSFORM(data);

  box = (gfloat*)g_value_get_boxed(source);
  if (g_binding_get_target(bind) == (gpointer)boxT->priv->spinTransXYZ[0])
    g_value_set_double(target, (double)box[0]);
  else if (g_binding_get_target(bind) == (gpointer)boxT->priv->spinTransXYZ[1])
    g_value_set_double(target, (double)box[1]);
  else if (g_binding_get_target(bind) == (gpointer)boxT->priv->spinTransXYZ[2])
    g_value_set_double(target, (double)box[2]);

  return TRUE;
}
static gboolean fromSpinToTrans(GBinding *bind _U_, const GValue *source _U_,
                                 GValue *target, gpointer data)
{
  float trans[3];
  VisuUiBoxTransformPrivate *priv = VISU_UI_BOX_TRANSFORM(data)->priv;

  trans[0] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(priv->spinTransXYZ[0]));
  trans[1] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(priv->spinTransXYZ[1]));
  trans[2] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(priv->spinTransXYZ[2]));
  g_value_set_boxed(target, trans);

  return TRUE;
}
/**
 * visu_ui_box_transform_bind:
 * @box_transform: a #VisuUiBoxTransform object.
 * @model: (transfer none): a #VisuPointset object.
 *
 * Bind the properties of @model to be displayed by @box_transform.
 *
 * Since: 3.8
 **/
void visu_ui_box_transform_bind(VisuUiBoxTransform *box_transform, VisuPointset *model)
{
  guint i;

  g_return_if_fail(VISU_IS_UI_BOX_TRANSFORM(box_transform));

  if (box_transform->priv->pointset == model)
    return;

  _bindBox(box_transform, (model) ? visu_boxed_getBox(VISU_BOXED(model)) : (VisuBox*)0, model);
  if (box_transform->priv->pointset)
    {
      g_signal_handler_disconnect(G_OBJECT(box_transform->priv->pointset),
                                  box_transform->priv->sig_box);
      g_object_unref(G_OBJECT(box_transform->priv->bind_trans[0]));
      g_object_unref(G_OBJECT(box_transform->priv->bind_trans[1]));
      g_object_unref(G_OBJECT(box_transform->priv->bind_trans[2]));
      g_object_unref(G_OBJECT(box_transform->priv->bind_allowTrans));
      g_object_unref(G_OBJECT(box_transform->priv->bind_boxTrans));
      g_object_unref(G_OBJECT(box_transform->priv->pointset));
    }
  box_transform->priv->pointset = model;
  g_object_notify_by_pspec(G_OBJECT(box_transform), properties[POINTSET_PROP]);
  if (model)
    {
      g_object_ref(G_OBJECT(model));
      box_transform->priv->sig_box = g_signal_connect_object
        (G_OBJECT(model), "setBox",
         G_CALLBACK(_bindBox), box_transform, G_CONNECT_SWAPPED);
      for (i = 0; i < 3; i++)
        box_transform->priv->bind_trans[i] =
          g_object_bind_property_full(model, "reduced-translation",
                                      box_transform->priv->spinTransXYZ[i], "value",
                                      G_BINDING_BIDIRECTIONAL | G_BINDING_SYNC_CREATE,
                                      fromTransToSpin, fromSpinToTrans,
                                      box_transform, (GDestroyNotify)0);
      box_transform->priv->bind_allowTrans =
          g_object_bind_property(model, "use-translation",
                                 box_transform->priv->checkAllowTranslations, "active",
                                 G_BINDING_BIDIRECTIONAL | G_BINDING_SYNC_CREATE);
      box_transform->priv->bind_boxTrans =
          g_object_bind_property(model, "in-the-box",
                                 box_transform->priv->checkInBox, "active",
                                 G_BINDING_BIDIRECTIONAL | G_BINDING_SYNC_CREATE);
    }
}
/**
 * visu_ui_box_transform_bindGlExtBox:
 * @box_transform: a #VisuUiBoxTransform object.
 * @model: (transfer none): a #VisuGlExtBox object.
 *
 * Bind the properties of @model to be displayed by @box_transform.
 *
 * Since: 3.8
 **/
void visu_ui_box_transform_bindGlExtBox(VisuUiBoxTransform *box_transform,
                                        VisuGlExtBox *model)
{
  g_return_if_fail(VISU_IS_UI_BOX_TRANSFORM(box_transform));

  if (box_transform->priv->glBox == model)
    return;

  if (box_transform->priv->glBox)
    {
      g_object_unref(G_OBJECT(box_transform->priv->bind_stipple));
      g_object_unref(G_OBJECT(box_transform->priv->bind_color));
      g_object_unref(G_OBJECT(box_transform->priv->glBox));
    }
  box_transform->priv->glBox = model;
  g_object_notify_by_pspec(G_OBJECT(box_transform), properties[GL_BOX_PROP]);
  if (model)
    {
      g_object_ref(G_OBJECT(model));
      box_transform->priv->bind_stipple =
        g_object_bind_property(model, "expand-stipple",
                               box_transform->priv->stippleExpandBox, "value",
                               G_BINDING_BIDIRECTIONAL | G_BINDING_SYNC_CREATE);
      box_transform->priv->bind_color =
        g_object_bind_property(model, "side-color",
                               box_transform->priv->colorExpandBox, "color",
                               G_BINDING_BIDIRECTIONAL | G_BINDING_SYNC_CREATE);
    }
}
