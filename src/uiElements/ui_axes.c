/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2014)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2014)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "ui_axes.h"

#include <support.h>
#include <visu_tools.h>

/**
 * SECTION:ui_axes
 * @short_description: Defines a widget to setup axes.
 *
 * <para>A set of widgets to setup the rendring of axes.</para>
 */

/**
 * VisuUiAxesClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuUiAxesClass structure.
 *
 * Since: 3.8
 */
/**
 * VisuUiAxes:
 *
 * An opaque structure.
 *
 * Since: 3.8
 */
/**
 * VisuUiAxesPrivate:
 *
 * Private fields for #VisuUiAxes objects.
 *
 * Since: 3.8
 */
struct _VisuUiAxesPrivate
{
  gboolean dispose_has_run;

  GtkWidget *checkAxes;
  GtkWidget *spinFactor;
  GtkWidget *spinXPos, *spinYPos;
  GtkWidget *entryLbl[3];

  VisuGlExtAxes *model;
  GBinding *bind_factor;
  GBinding *bind_xpos, *bind_ypos;
  GBinding *bindLbl[3];
};

static void visu_ui_axes_dispose(GObject* obj);

/* Local callbacks. */

G_DEFINE_TYPE_WITH_CODE(VisuUiAxes, visu_ui_axes, VISU_UI_TYPE_LINE,
                        G_ADD_PRIVATE(VisuUiAxes))

static void visu_ui_axes_class_init(VisuUiAxesClass *klass)
{
  DBG_fprintf(stderr, "Ui Axes: creating the class of the widget.\n");
  DBG_fprintf(stderr, "                     - adding new signals ;\n");

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose = visu_ui_axes_dispose;
}
static void visu_ui_axes_dispose(GObject *obj)
{
  DBG_fprintf(stderr, "Ui Axes: dispose object %p.\n", (gpointer)obj);

  if (VISU_UI_AXES(obj)->priv->dispose_has_run)
    return;

  visu_ui_axes_bind(VISU_UI_AXES(obj), (VisuGlExtAxes*)0);

  VISU_UI_AXES(obj)->priv->dispose_has_run = TRUE;
  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_axes_parent_class)->dispose(obj);
}

static void visu_ui_axes_init(VisuUiAxes *obj)
{
  GtkWidget *vbox, *hbox;
#define SET_LABEL(I) {obj->priv->entryLbl[I] = gtk_entry_new(); \
    gtk_entry_set_width_chars(GTK_ENTRY(obj->priv->entryLbl[I]), 6);   \
    gtk_box_pack_start(GTK_BOX(hbox), obj->priv->entryLbl[I], TRUE, TRUE, 0);}

  DBG_fprintf(stderr, "Extension Axes: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = visu_ui_axes_get_instance_private(obj);
  obj->priv->dispose_has_run = FALSE;

  obj->priv->model = (VisuGlExtAxes*)0;

  vbox = visu_ui_line_getOptionBox(VISU_UI_LINE(obj));

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  obj->priv->checkAxes = gtk_check_button_new_with_mnemonic(_("Use _box basis-set"));
  gtk_box_pack_start(GTK_BOX(hbox), obj->priv->checkAxes, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(_("size: ")), TRUE, TRUE, 0);
  obj->priv->spinFactor = gtk_spin_button_new_with_range(0., 1., 0.01);
  gtk_box_pack_start(GTK_BOX(hbox), obj->priv->spinFactor, FALSE, FALSE, 0);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(_("x pos.: ")), TRUE, TRUE, 0);
  obj->priv->spinXPos = gtk_spin_button_new_with_range(0., 1., 0.1);
  gtk_box_pack_start(GTK_BOX(hbox), obj->priv->spinXPos, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(_("y pos.: ")), TRUE, TRUE, 0);
  obj->priv->spinYPos = gtk_spin_button_new_with_range(0., 1., 0.1);
  gtk_box_pack_start(GTK_BOX(hbox), obj->priv->spinYPos, FALSE, FALSE, 0);
  hbox = gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(_("Axis labels: ")), TRUE, TRUE, 0);
  SET_LABEL(0);
  SET_LABEL(1);
  SET_LABEL(2);

  gtk_widget_show_all(GTK_WIDGET(vbox));
}


/**
 * visu_ui_axes_new:
 *
 * Creates a new #VisuUiAxes to allow to setup axes rendering characteristics.
 *
 * Since: 3.8
 *
 * Returns: a pointer to the newly created widget.
 */
GtkWidget* visu_ui_axes_new()
{
  DBG_fprintf(stderr,"Ui Axes: new object.\n");
  
  return GTK_WIDGET(g_object_new(VISU_TYPE_UI_AXES, "label", _("Basis set"), NULL));
}

/**
 * visu_ui_axes_bind:
 * @axes: a #VisuUiAxes object.
 * @model: (transfer full): a #VisuGlExtAxes object.
 *
 * Bind the properties of @model to be displayed by @axes.
 *
 * Since: 3.8
 **/
void visu_ui_axes_bind(VisuUiAxes *axes, VisuGlExtAxes *model)
{
  g_return_if_fail(VISU_IS_UI_AXES(axes));
#define BIND_LBL(I, L) {axes->priv->bindLbl[I] = \
      g_object_bind_property(model, L, axes->priv->entryLbl[I], "text", \
                             G_BINDING_BIDIRECTIONAL | G_BINDING_SYNC_CREATE);}

  if (axes->priv->model == model)
    return;

  visu_ui_line_bind(VISU_UI_LINE(axes), VISU_GL_EXT_LINED(model));
  if (axes->priv->model)
    {
      g_object_unref(axes->priv->bind_factor);
      g_object_unref(axes->priv->bind_xpos);
      g_object_unref(axes->priv->bind_ypos);
      g_object_unref(axes->priv->bindLbl[0]);
      g_object_unref(axes->priv->bindLbl[1]);
      g_object_unref(axes->priv->bindLbl[2]);
      g_object_unref(axes->priv->model);
    }
  axes->priv->model = model;
  if (model)
    {
      g_object_ref(model);
      axes->priv->bind_factor = g_object_bind_property(model, "size",
                                                       axes->priv->spinFactor, "value",
                                                       G_BINDING_BIDIRECTIONAL |
                                                       G_BINDING_SYNC_CREATE);
      axes->priv->bind_xpos = g_object_bind_property(model, "x-pos",
                                                     axes->priv->spinXPos, "value",
                                                     G_BINDING_BIDIRECTIONAL |
                                                     G_BINDING_SYNC_CREATE);
      axes->priv->bind_ypos = g_object_bind_property(model, "y-pos",
                                                     axes->priv->spinYPos, "value",
                                                     G_BINDING_BIDIRECTIONAL |
                                                     G_BINDING_SYNC_CREATE);
      BIND_LBL(0, "x-label");
      BIND_LBL(1, "y-label");
      BIND_LBL(2, "z-label");
    }
}
/**
 * visu_ui_axes_getBasisCheckButton:
 * @axes: a #VisuUiAxes object.
 *
 * Retrieve the check button used to indicate if axes are orthogonal
 * or follow the box basis set.
 *
 * Since: 3.8
 *
 * Returns: (transfer none): a #GtkCheckButton widget.
 **/
GtkWidget* visu_ui_axes_getBasisCheckButton(VisuUiAxes *axes)
{
  g_return_val_if_fail(VISU_IS_UI_AXES(axes), (GtkWidget*)0);

  return axes->priv->checkAxes;
}
