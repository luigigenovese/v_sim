/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "ui_atomic.h"

#include <support.h>
#include <interface.h>

#include <renderingMethods/elementAtomic.h>

/**
 * SECTION:ui_atomic
 * @short_description: Defines a widget to setup a atomic.
 *
 * <para>A set of widgets to setup the rendring of a atomic.</para>
 */

/**
 * VisuUiAtomicClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuUiAtomicClass structure.
 *
 * Since: 3.8
 */
/**
 * VisuUiAtomic:
 *
 * An opaque structure.
 *
 * Since: 3.8
 */
/**
 * VisuUiAtomicPrivate:
 *
 * Private fields for #VisuUiAtomic objects.
 *
 * Since: 3.8
 */
struct _VisuUiAtomicPrivate
{
  gboolean dispose_has_run;

  GtkWidget *spinRadius;
  GtkWidget *entryShape;
  GtkWidget *spinRatio;
  GtkWidget *spinPhi;
  GtkWidget *spinTheta;

  VisuNodeArrayRenderer *renderer;

  VisuElementAtomic *model;
  gulong units_sig;
  GBinding *radius_bind, *shape_bind, *ratio_bind, *phi_bind, *theta_bind;

  GList *targets;
};

static void visu_ui_atomic_finalize(GObject* obj);
static void visu_ui_atomic_dispose(GObject* obj);

/* Local callbacks. */
static gboolean formatRadius(GtkSpinButton *button, const VisuUiAtomic *atomic);

G_DEFINE_TYPE_WITH_CODE(VisuUiAtomic, visu_ui_atomic, GTK_TYPE_BOX,
                        G_ADD_PRIVATE(VisuUiAtomic))

static void visu_ui_atomic_class_init(VisuUiAtomicClass *klass)
{
  DBG_fprintf(stderr, "Ui Atomic: creating the class of the widget.\n");
  DBG_fprintf(stderr, "                     - adding new signals ;\n");

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose = visu_ui_atomic_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_ui_atomic_finalize;
}
static void visu_ui_atomic_dispose(GObject *obj)
{
  VisuUiAtomic *self = VISU_UI_ATOMIC(obj);
  DBG_fprintf(stderr, "Ui Atomic: dispose object %p.\n", (gpointer)obj);

  if (self->priv->dispose_has_run)
    return;
  self->priv->dispose_has_run = TRUE;

  visu_ui_atomic_bind(self, (GList*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_atomic_parent_class)->dispose(obj);
}
static void visu_ui_atomic_finalize(GObject *obj)
{
  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Ui Atomic: finalize object %p.\n", (gpointer)obj);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_atomic_parent_class)->finalize(obj);
  DBG_fprintf(stderr, " | freeing ... OK.\n");
}

static void visu_ui_atomic_init(VisuUiAtomic *obj)
{
  GtkWidget *label, *hbox;
  const char **names, **ids;
  int i;
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips;
  tooltips = gtk_tooltips_new ();
#endif

  DBG_fprintf(stderr, "Extension Atomic: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  gtk_orientable_set_orientation(GTK_ORIENTABLE(obj), GTK_ORIENTATION_VERTICAL);

  obj->priv = visu_ui_atomic_get_instance_private(obj);
  obj->priv->dispose_has_run = FALSE;

  obj->priv->renderer = (VisuNodeArrayRenderer*)0;
  obj->priv->model = (VisuElementAtomic*)0;
  obj->priv->targets = (GList*)0;

  gtk_widget_set_sensitive(GTK_WIDGET(obj), FALSE);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(obj), hbox, FALSE, FALSE, 5);

  label = gtk_label_new("");
  gtk_label_set_text(GTK_LABEL(label), _("Radius:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

  obj->priv->spinRadius = gtk_spin_button_new_with_range(0.001, 999., 0.05);
  g_signal_connect(obj->priv->spinRadius, "output", G_CALLBACK(formatRadius), obj);
  gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(obj->priv->spinRadius), FALSE);
  gtk_entry_set_width_chars(GTK_ENTRY(obj->priv->spinRadius), 10);
  gtk_box_pack_start(GTK_BOX(hbox), obj->priv->spinRadius, FALSE,FALSE, 3);

  label = gtk_label_new(_("Shape: "));
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 1);
  gtk_label_set_xalign(GTK_LABEL(label), 1.);

  obj->priv->entryShape = gtk_combo_box_text_new();
  names = visu_element_atomic_getShapeNames(TRUE);
  ids   = visu_element_atomic_getShapeNames(FALSE);
  for (i = 0; names[i] && ids[i]; i++)
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(obj->priv->entryShape), ids[i], names[i]);
  gtk_combo_box_set_active(GTK_COMBO_BOX(obj->priv->entryShape), 0);
  gtk_box_pack_start(GTK_BOX(hbox), obj->priv->entryShape, FALSE, FALSE, 0);

  /* Set widgets for the elipsoid parameters. */
  label = gtk_label_new("");
  gtk_label_set_markup(GTK_LABEL(label), _("Parameters for elipsoid shape"));
  gtk_box_pack_start(GTK_BOX(obj), label, FALSE, FALSE, 5);
  gtk_label_set_xalign(GTK_LABEL(label), 0.);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(obj), hbox, FALSE, FALSE, 2);
  label = gtk_label_new(_("Ratio: "));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_label_set_xalign(GTK_LABEL(label), 0.);
  gtk_widget_set_margin_start(label, 10);
  obj->priv->spinRatio = gtk_spin_button_new_with_range(1., 10., 0.1);
  gtk_box_pack_end(GTK_BOX(hbox), obj->priv->spinRatio, FALSE, FALSE, 0);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(obj), hbox, FALSE, FALSE, 2);
  label = gtk_label_new(_("Phi: "));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_label_set_xalign(GTK_LABEL(label), 0.);
  gtk_widget_set_margin_start(label, 10);
  obj->priv->spinPhi = gtk_spin_button_new_with_range(-180., 180., 1.);
  gtk_box_pack_end(GTK_BOX(hbox), obj->priv->spinPhi, FALSE, FALSE, 0);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(obj), hbox, FALSE, FALSE, 2);
  label = gtk_label_new(_("Theta: "));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_label_set_xalign(GTK_LABEL(label), 0.);
  gtk_widget_set_margin_start(label, 10);
  obj->priv->spinTheta = gtk_spin_button_new_with_range(-180., 180., 1.);
  gtk_box_pack_end(GTK_BOX(hbox), obj->priv->spinTheta, FALSE, FALSE, 0);
}

/**
 * visu_ui_atomic_new:
 * @renderer: a #VisuNodeArrayRenderer object.
 *
 * Creates a new #VisuUiAtomic to allow to setup atomic rendering characteristics.
 *
 * Since: 3.8
 *
 * Returns: a pointer to the newly created widget.
 */
GtkWidget* visu_ui_atomic_new(VisuNodeArrayRenderer *renderer)
{
  VisuUiAtomic *atomic;

  DBG_fprintf(stderr,"Ui Atomic: new object.\n");

  atomic = VISU_UI_ATOMIC(g_object_new(VISU_TYPE_UI_ATOMIC, NULL));
  atomic->priv->renderer = renderer;
  return GTK_WIDGET(atomic);
}

static gboolean formatRadius(GtkSpinButton *button, const VisuUiAtomic *atomic)
{
  gchar *str;
  ToolUnits units;

  if (!atomic->priv->model)
    return FALSE;

  units = visu_element_atomic_getUnits(atomic->priv->model);
  if (units == TOOL_UNITS_UNDEFINED)
    str = g_strdup_printf("%0.3f", gtk_spin_button_get_value(button));
  else
    str = g_strdup_printf("%0.3f %s", gtk_spin_button_get_value(button),
                          tool_physic_getUnitLabel(units));
  gtk_entry_set_text(GTK_ENTRY(button), str);
  g_free(str);
  return TRUE;
}

static gboolean setForAll(GBinding *bind, const GValue *source_value,
                          GValue *target_value, gpointer data)
{
  VisuUiAtomic *atomic = VISU_UI_ATOMIC(data);
  GList *lst;
  
  for (lst = atomic->priv->targets; lst; lst = g_list_next(lst))
    if (lst->data != atomic->priv->model)
      g_object_set_property(lst->data, g_binding_get_source_property(bind), source_value);
  
  return g_value_transform(source_value, target_value);
}

static void _bind(VisuUiAtomic *atomic, VisuElementAtomic *element)
{
  if (atomic->priv->model == element)
    return;

  if (atomic->priv->model)
    {
      g_object_unref(atomic->priv->radius_bind);
      g_object_unref(atomic->priv->shape_bind);
      g_object_unref(atomic->priv->ratio_bind);
      g_object_unref(atomic->priv->phi_bind);
      g_object_unref(atomic->priv->theta_bind);
      g_signal_handler_disconnect(atomic->priv->model, atomic->priv->units_sig);
      g_object_unref(atomic->priv->model);
    }
  atomic->priv->model = element;
  if (element)
    {
      g_object_ref(element);
      atomic->priv->radius_bind =
        g_object_bind_property_full(element, "radius", atomic->priv->spinRadius, "value",
                                    G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                                    NULL, setForAll, atomic, (GDestroyNotify)0);
      atomic->priv->shape_bind =
        g_object_bind_property_full(element, "shape", atomic->priv->entryShape, "active",
                                    G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                                    NULL, setForAll, atomic, (GDestroyNotify)0);
      atomic->priv->ratio_bind =
        g_object_bind_property_full(element, "elipsoid-ratio",
                                    atomic->priv->spinRatio, "value",
                                    G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                                    NULL, setForAll, atomic, (GDestroyNotify)0);
      atomic->priv->phi_bind =
        g_object_bind_property_full(element, "elipsoid-angle-phi",
                                    atomic->priv->spinPhi, "value",
                                    G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                                    NULL, setForAll, atomic, (GDestroyNotify)0);
      atomic->priv->theta_bind =
        g_object_bind_property_full(element, "elipsoid-angle-theta",
                                    atomic->priv->spinTheta, "value",
                                    G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                                    NULL, setForAll, atomic, (GDestroyNotify)0);
      atomic->priv->units_sig =
        g_signal_connect_swapped(element, "notify::units",
                                 G_CALLBACK(gtk_spin_button_update), atomic->priv->spinRadius);
    }
}
/**
 * visu_ui_atomic_bind:
 * @atomic: a #VisuUiAtomic object.
 * @eleList: (element-type VisuElement): a list of #VisuElement.
 *
 * Use the list @eleList to be handled by @elements. Any change in
 * @elements will be applied to the #VisuElementRenderer corresponding
 * to each #VisuElement of @eleList.
 *
 * Since: 3.8
 **/
void visu_ui_atomic_bind(VisuUiAtomic *atomic, GList *eleList)
{
  GList *lst;

  g_return_if_fail(VISU_IS_UI_ATOMIC(atomic));
  g_return_if_fail(atomic->priv->renderer);

  if (!eleList)
    _bind(atomic, (VisuElementAtomic*)0);
  else
    {
      if (!atomic->priv->model || !g_list_find(eleList, visu_element_renderer_getElement(VISU_ELEMENT_RENDERER(atomic->priv->model))))
        _bind(atomic, VISU_ELEMENT_ATOMIC(visu_node_array_renderer_get(atomic->priv->renderer, VISU_ELEMENT(eleList->data))));
    }
  
  if (atomic->priv->targets)
    g_list_free(atomic->priv->targets);

  atomic->priv->targets = (GList*)0;
  for (lst = eleList; lst; lst = g_list_next(lst))
    atomic->priv->targets = g_list_prepend(atomic->priv->targets, visu_node_array_renderer_get(atomic->priv->renderer, VISU_ELEMENT(lst->data)));

  gtk_widget_set_sensitive(GTK_WIDGET(atomic), (atomic->priv->model != (VisuElementAtomic*)0));
}
