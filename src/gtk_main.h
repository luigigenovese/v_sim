/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_PUBLIC_H
#define VISU_PUBLIC_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

#include "visu_tools.h"
#include "visu_dump.h"

#include "extraGtkFunctions/gtk_toolPanelWidget.h"
#include "gtk_renderingWindowWidget.h"

/**
 * VISU_UI_TYPE_MAIN:
 *
 * return the type of #VisuUiMain.
 */
#define VISU_UI_TYPE_MAIN	     (visu_ui_main_get_type ())
/**
 * VISU_UI_MAIN:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuUiMain type.
 */
#define VISU_UI_MAIN(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_UI_TYPE_MAIN, VisuUiMain))
/**
 * VISU_UI_MAIN_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuUiMainClass.
 */
#define VISU_UI_MAIN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_UI_TYPE_MAIN, VisuUiMainClass))
/**
 * VISU_UI_IS_MAIN_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuUiMain object.
 */
#define VISU_UI_IS_MAIN_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_UI_TYPE_MAIN))
/**
 * VISU_UI_IS_MAIN_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuUiMainClass class.
 */
#define VISU_UI_IS_MAIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_UI_TYPE_MAIN))
/**
 * VISU_UI_MAIN_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_UI_MAIN_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_UI_TYPE_MAIN, VisuUiMainClass))

/**
 * VisuUiMain_private:
 *
 * Private fields for #VisuUiMain objects.
 */
typedef struct _VisuUiMainPrivate VisuUiMainPrivate;
/**
 * _VisuUiMain:
 * @parent: the parent object, a #GtkWindow here ;
 * @renderingWindow: a pointer on the associated rendering window ;
 * @pairsDialog: a pointer to the corresponding pair dialog, or NULL
 * if not yet built ;
 * @interactiveDialog: idem for the interactive dialog ;
 * @aboutDialog: idem for the about dialog ;
 * @private: a pointer to the private data.
 *
 * This structure describes a #VisuUiMain object.
 */
typedef struct _VisuUiMain VisuUiMain;
struct _VisuUiMain
{
  GtkWindow parent;

  /* Pointers on permanent windows. */
  GtkWidget *renderingWindow;

  GtkWidget *pairsDialog;

  GtkWidget *interactiveDialog;

  GtkWidget *aboutDialog;

  /* Private data. */
  VisuUiMainPrivate *priv;
};
/**
 * VisuUiMainClass:
 *
 * A short way to identify #_VisuUiMainClass structure.
 */
typedef struct _VisuUiMainClass VisuUiMainClass;

/**
 * visu_ui_main_get_type:
 *
 * This method returns the type of #VisuUiMain, use VISU_UI_TYPE_MAIN instead.
 *
 * Returns: the type of #VisuUiMain.
 */
GType visu_ui_main_get_type(void);

/**
 * VisuUiMainInitFunc:
 * @ui: the #VisuUiMain object.
 *
 * These methods are used by V_Sim to initialise some part of the
 * interface. They are called once on start-up.
 *
 * Since: 3.8
 */
typedef void (*VisuUiMainInitFunc)(VisuUiMain *ui);

/**
 * visu_ui_main_new:
 * @oneWindow: a boolean.
 *
 * Create the command panel window and is dependencies, such as the
 * associated rendering window... WARNING: some part are still currently
 * static, so only once instance can be created at a time. If
 * @oneWindow argument is TRUE, then the rendering area is creating in
 * the same #GtkWindow on the right of the panel.
 *
 * Returns: a newly create command panel.
 */
GtkWidget* visu_ui_main_new(gboolean oneWindow);

/**
 * visu_ui_main_quit:
 * @main: a pointer to the main interface.
 * @force: if TRUE, override the preference of a quiting dialog and quit.
 *
 * Quit the program. If the preference to have a confirm dialog is set,
 * then it raises the little warning window before quiting (or not).
 */
void visu_ui_main_quit(VisuUiMain *main, gboolean force);

/**
 * visu_ui_main_buildInteractiveDialog:
 * @main: a #VisuUiMain object.
 *
 * Create the mouse action dialog window (if not already done).
 */
void visu_ui_main_buildInteractiveDialog(VisuUiMain *main);
/**
 * visu_ui_main_runCommandLine:
 * @data: a pointer on a #VisuUiMain object.
 *
 * Call the get routines from the command line module and associate
 * the different tasks to the different panels. For a version that do
 * not use the panels, call visu_ui_runCommandLine() instead.
 *
 * Returns: always FALSE.
 */
gboolean visu_ui_main_runCommandLine(gpointer data);
/**
 * visu_ui_main_initPanels:
 * @data: a pointer on a #VisuUiMain object.
 *
 * Call the init routines for the different panels.
 *
 * Returns: always FALSE.
 */
gboolean visu_ui_main_initPanels(gpointer data);
VisuUiRenderingWindow* visu_ui_main_getRendering(VisuUiMain *main);


/**
 * visu_ui_main_class_setRememberPosition:
 * @val: an boolean.
 *
 * V_Sim can try to remember the position of its main windows, then
 * open them again will result in a positioning on screen equivalent
 * to previous position.
 */
void visu_ui_main_class_setRememberPosition(gboolean val);
/**
 * visu_ui_main_class_getRememberPosition:
 *
 * V_Sim can store the position of its main windows. Use this routine
 * to get the status of this capability.
 *
 * Returns: TRUE if set.
 */
gboolean visu_ui_main_class_getRememberPosition();

/**
 * VisuUiDirectoryType:
 * @VISU_UI_DIR_FILE: default kind of directory ;
 * @VISU_UI_DIR_SURFACE: directory for surfaces ;
 * @VISU_UI_DIR_DATAFILE: directory for data files ;
 * @VISU_UI_DIR_CONF: directory for configuration files ;
 * @VISU_UI_DIR_BROWSER: directory used in the browser.
 * @VISU_UI_DIR_N_VALUES: private.
 *
 * V_Sim remembers the last open directory when presenting a new file
 * selector. This enum is used to specify different kind of
 * directories. Like that saving a configuration file in a given
 * directory will not affect the prefered directory for browsing a
 * list of files.
 */
typedef enum
  {
    VISU_UI_DIR_FILE,
    VISU_UI_DIR_SURFACE,
    VISU_UI_DIR_DATAFILE,
    VISU_UI_DIR_CONF,
    VISU_UI_DIR_BROWSER,
    /*< private >*/
    VISU_UI_DIR_N_VALUES
  } VisuUiDirectoryType;
const gchar* visu_ui_main_getLastOpenDirectory(VisuUiMain *main);
void visu_ui_main_setLastOpenDirectory(VisuUiMain *main,
                                       const char* directory, VisuUiDirectoryType type);
gchar** visu_ui_main_getSelectedDirectory(VisuUiMain *main,
                                          GtkWindow *parent, gboolean multiple,
                                          const gchar *dir);

VisuUiMain* visu_ui_main_class_getCurrentPanel();
void visu_ui_main_class_setCurrentPanel(VisuUiMain *main);

void visu_ui_main_class_createMain(GtkWindow **panel,
                                   GtkWindow **renderWindow, GtkWidget **renderArea);

VisuUiRenderingWindow* visu_ui_main_class_getDefaultRendering();

#endif
