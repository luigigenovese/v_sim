/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef IFACE_POINTSET_H
#define IFACE_POINTSET_H

#include <glib.h>
#include <glib-object.h>

#include "iface_boxed.h"

G_BEGIN_DECLS

/* Pointset interface. */
#define VISU_TYPE_POINTSET                (visu_pointset_get_type ())
#define VISU_POINTSET(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_TYPE_POINTSET, VisuPointset))
#define VISU_IS_POINTSET(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_TYPE_POINTSET))
#define VISU_POINTSET_GET_INTERFACE(inst) (G_TYPE_INSTANCE_GET_INTERFACE ((inst), VISU_TYPE_POINTSET, VisuPointsetInterface))

typedef struct _VisuPointsetInterface VisuPointsetInterface;
typedef struct _VisuPointset VisuPointset; /* dummy object */

/**
 * VisuPointset:
 *
 * Interface object.
 *
 * Since: 3.8
 */

/**
 * VisuPointsetInterface:
 * @parent: yet, its parent.
 * @set_inTheBox: a routine to translate every content in the box.
 * @set_translationActive: a routine to activate or not the translation.
 * @get_translation: a routine to get the current translation status.
 * @set_translation: a routine to set the translation status.
 * @apply_translation: a routine to apply the stored translations.
 *
 * The different routines common to objects implementing a #VisuPointset interface.
 * 
 * Since: 3.8
 */
struct _VisuPointsetInterface
{
  VisuBoxedInterface parent;

  gboolean (*set_inTheBox) (VisuPointset *self, gboolean status);

  gboolean (*set_translationActive) (VisuPointset *self, gboolean status);
  void (*get_translation) (VisuPointset *self, float translation[3]);
  gboolean (*set_translation) (VisuPointset *self, float translation[3], gboolean withModulo);

  void (*apply_translation) (VisuPointset *set);
};

GType visu_pointset_get_type (void);

gboolean visu_pointset_setInTheBox(VisuPointset *self, gboolean status);
gboolean visu_pointset_setTranslationActive(VisuPointset *self, gboolean status);
void visu_pointset_applyTranslation(VisuPointset *self);

void visu_pointset_getTranslation(VisuPointset *self, float trans[3]);
gboolean visu_pointset_setTranslation(VisuPointset *self, float trans[3],
                                      gboolean withModulo);
gboolean visu_pointset_setTranslationPeriodic(VisuPointset* self, float trans[3],
                                              gboolean withModulo);
gboolean visu_pointset_setBoxTranslation(VisuPointset* self, float boxTrans[3],
                                         gboolean withModulo);
gboolean visu_pointset_shift(VisuPointset *self, const gfloat delta[3],
                             gboolean withModulo);
gboolean visu_pointset_getTranslationPeriodicStatus(VisuPointset *self);

G_END_DECLS

#endif
