/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_UI_DATA_CHOOSER_H
#define VISU_UI_DATA_CHOOSER_H

#include <gtk/gtk.h>

#include "visu_dataloadable.h"

G_BEGIN_DECLS

#define VISU_UI_TYPE_DATA_CHOOSER         (visu_ui_data_chooser_get_type ())
#define VISU_UI_DATA_CHOOSER(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_UI_TYPE_DATA_CHOOSER, VisuUiDataChooser))
#define VISU_UI_DATA_CHOOSER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), VISU_UI_TYPE_DATA_CHOOSER, VisuUiDataChooserClass))
#define VISU_UI_IS_DATA_CHOOSER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_UI_TYPE_DATA_CHOOSER))
#define VISU_UI_IS_DATA_CHOOSER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), VISU_UI_TYPE_DATA_CHOOSER))
#define VISU_UI_DATA_CHOOSER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_UI_TYPE_DATA_CHOOSER, VisuUiDataChooserClass))

typedef struct _VisuUiDataChooser VisuUiDataChooser;
typedef struct _VisuUiDataChooserPrivate VisuUiDataChooserPrivate;

/**
 * VisuUiDataChooser:
 *
 * Structure used to define #VisuUiDataChooser objects.
 *
 * Since: 3.8
 */
struct _VisuUiDataChooser
{
  GtkFileChooserDialog parent;

  VisuUiDataChooserPrivate *priv;
};

/**
 * VisuUiDataChooserClass:
 * @parent: its parent.
 *
 * The class defining a file chooser widget specialised to open
 * #VisuDataLoadable objects.
 *
 * Since: 3.8
 */
typedef struct _VisuUiDataChooserClass VisuUiDataChooserClass;
struct _VisuUiDataChooserClass
{
  GtkFileChooserDialogClass parent;
};

GType visu_ui_data_chooser_get_type (void);

GtkWidget* visu_ui_data_chooser_new(GtkWindow *parent);

VisuDataLoadable* visu_ui_data_chooser_run(VisuUiDataChooser *this);

G_END_DECLS

#endif
