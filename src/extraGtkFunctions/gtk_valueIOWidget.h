/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef GTK_STIPPLECOMBOBOXWIDGET_H
#define GTK_STIPPLECOMBOBOXWIDGET_H

#include <glib.h>
#include <glib-object.h>

#include <gtk/gtk.h>

G_BEGIN_DECLS
/**
 * VISU_UI_TYPE_VALUE_IO:
 *
 * Get the associated #GType to the VisuUiValueIo objects.
 *
 * Since: 3.5
 */
#define VISU_UI_TYPE_VALUE_IO         (visu_ui_value_io_get_type ())
/**
 * VISU_UI_VALUE_IO:
 * @obj: the widget to cast.
 *
 * Cast the given object to a #VisuUiValueIo object.
 *
 * Since: 3.5
 */
#define VISU_UI_VALUE_IO(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_UI_TYPE_VALUE_IO, VisuUiValueIo))
/**
 * VISU_UI_VALUE_IO_CLASS:
 * @klass: the class to cast.
 *
 * Cast the given class to a #VisuUiValueIoClass object.
 *
 * Since: 3.5
 */
#define VISU_UI_VALUE_IO_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), VISU_UI_TYPE_VALUE_IO, VisuUiValueIoClass))
/**
 * VISU_UI_IS_VALUE_IO:
 * @obj: the object to test.
 *
 * Get if the given object is a valid #VisuUiValueIo object.
 *
 * Since: 3.5
 */
#define VISU_UI_IS_VALUE_IO(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_UI_TYPE_VALUE_IO))
/**
 * VISU_UI_IS_VALUE_IO_CLASS:
 * @klass: the class to test.
 *
 * Get if the given class is a valid #VisuUiValueIoClass class.
 *
 * Since: 3.5
 */
#define VISU_UI_IS_VALUE_IO_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), VISU_UI_TYPE_VALUE_IO))

typedef struct _VisuUiValueIo VisuUiValueIo;
typedef struct _VisuUiValueIoClass VisuUiValueIoClass;

GType visu_ui_value_io_get_type(void);
GtkWidget* visu_ui_value_io_new(GtkWindow *parent, const gchar*tipsOpen,
		      const gchar*tipsSave, const gchar*tipsSaveAs);

/**
 * VisuUiValueIoCallback:
 * @filename: the name of the file to be saved or opened ;
 * @error: a location to store a possible error.
 *
 * Prototypes of functions to be called when an IO file is saved or
 * opened, see visu_ui_value_io_connectOnOpen() and visu_ui_value_io_connectOnSave().
 *
 * Returns: should return TRUE if the action of the callback succeed.
 */
typedef gboolean (*VisuUiValueIoCallback)(const gchar *filename, GError **error);

void visu_ui_value_io_connectOnOpen(VisuUiValueIo *valueio, VisuUiValueIoCallback open);
void visu_ui_value_io_connectOnSave(VisuUiValueIo *valueio, VisuUiValueIoCallback save);
void visu_ui_value_io_setSensitiveOpen(VisuUiValueIo *valueio, gboolean status);
void visu_ui_value_io_setSensitiveSave(VisuUiValueIo *valueio, gboolean status);
gboolean visu_ui_value_io_setFilename(VisuUiValueIo *valueio, const gchar *filename);
gchar* visu_ui_value_io_getFilename(GtkWindow *parent);

G_END_DECLS

#endif
