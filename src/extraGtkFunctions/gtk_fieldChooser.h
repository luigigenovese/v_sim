/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2011-2011)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2011-2011)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef GTK_FIELDCHOOSER_H
#define GTK_FIELDCHOOSER_H

#include <gtk/gtk.h>

#include <coreTools/toolFileFormat.h>
#include <extraFunctions/sfielddata.h>

G_BEGIN_DECLS

/**
 * VISU_UI_TYPE_FIELD_CHOOSER:
 *
 * Return the associated #GType to the VisuUiFieldChooser objects.
 */
#define VISU_UI_TYPE_FIELD_CHOOSER         (visu_ui_field_chooser_get_type ())
/**
 * VISU_UI_FIELD_CHOOSER:
 * @obj: the widget to cast.
 *
 * Cast the given object to a #VisuUiFieldChooser object.
 */
#define VISU_UI_FIELD_CHOOSER(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_UI_TYPE_FIELD_CHOOSER, VisuUiFieldChooser))
/**
 * VISU_UI_FIELD_CHOOSER_CLASS:
 * @klass: the class to cast.
 *
 * Cast the given class to a #VisuUiFieldChooserClass object.
 */
#define VISU_UI_FIELD_CHOOSER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), VISU_UI_TYPE_FIELD_CHOOSER, VisuUiFieldChooserClass))
/**
 * VISU_UI_IS_FIELD_CHOOSER:
 * @obj: the object to test.
 *
 * Return if the given object is a valid #VisuUiFieldChooser object.
 */
#define VISU_UI_IS_FIELD_CHOOSER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_UI_TYPE_FIELD_CHOOSER))
/**
 * VISU_UI_IS_FIELD_CHOOSER_CLASS:
 * @klass: the class to test.
 *
 * Return if the given class is a valid #VisuUiFieldChooserClass class.
 */
#define VISU_UI_IS_FIELD_CHOOSER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), VISU_UI_TYPE_FIELD_CHOOSER))
/**
 * VISU_UI_FIELD_CHOOSER_GET_CLASS:
 * @obj: the widget to get the class of.
 *
 * Get the class of the given object.
 */
#define VISU_UI_FIELD_CHOOSER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_UI_TYPE_FIELD_CHOOSER, VisuUiFieldChooserClass))

/**
 * VisuUiFieldChooser
 *
 * Private structure to store informations of a #VisuUiFieldChooser object.
 */
typedef struct _VisuUiFieldChooser VisuUiFieldChooser;
/**
 * VisuUiFieldChooserClass
 *
 * Private structure to store informations of a #VisuUiFieldChooserClass object.
 */
typedef struct _VisuUiFieldChooserClass VisuUiFieldChooserClass;

/**
 * visu_ui_field_chooser_get_type
 *
 * #GType are unique numbers to identify objects.
 *
 * Returns: the #GType associated with #VisuUiFieldChooser objects.
 */
GType          visu_ui_field_chooser_get_type        (void);

/**
 * VisuUiBoxFit:
 * @VISU_UI_NO_FIT: don't change the box.
 * @VISU_UI_FIT_TO_BOX: adapt the box of the field to the box of the
 * loaded #VisuData.
 * @VISU_UI_FIT_TO_SURFACE: adapt the box of the loaded #VisuData to
 * the box of the field.
 *
 * Possible value to adapt the boxes of structure and field.
 *
 * Since: 3.7
 */
typedef enum
  {
    VISU_UI_NO_FIT,
    VISU_UI_FIT_TO_BOX,
    VISU_UI_FIT_TO_SURFACE
  } VisuUiBoxFit;

GtkWidget* visu_ui_field_chooser_new(GtkWindow *parent);

VisuScalarFieldMethod* visu_ui_field_chooser_getFileFormat(VisuUiFieldChooser *dialog);
VisuUiBoxFit visu_ui_field_chooser_getFit(VisuUiFieldChooser *dialog);
void visu_ui_field_chooser_setOptions(VisuUiFieldChooser *dialog, GtkWidget *wd);

G_END_DECLS


#endif
