/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef GTK_DUMPDIALOG_H
#define GTK_DUMPDIALOG_H

#include <glib.h>
#include <glib-object.h>

#include <gtk/gtk.h>

#include <visu_dump.h>
#include <coreTools/toolFileFormat.h>

G_BEGIN_DECLS
/**
 * VISU_UI_TYPE_DUMP_DIALOG:
 *
 * Return the associated #GType to the VisuUiDumpDialog objects.
 */
#define VISU_UI_TYPE_DUMP_DIALOG         (visu_ui_dump_dialog_get_type ())
/**
 * VISU_UI_DUMP_DIALOG:
 * @obj: the widget to cast.
 *
 * Cast the given object to a #VisuUiDumpDialog object.
 */
#define VISU_UI_DUMP_DIALOG(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_UI_TYPE_DUMP_DIALOG, VisuUiDumpDialog))
/**
 * VISU_UI_DUMP_DIALOG_CLASS:
 * @klass: the class to cast.
 *
 * Cast the given class to a #VisuUiDumpDialogClass object.
 */
#define VISU_UI_DUMP_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), VISU_UI_TYPE_DUMP_DIALOG, VisuUiDumpDialogClass))
/**
 * VISU_UI_IS_DUMP_DIALOG:
 * @obj: the object to test.
 *
 * Return if the given object is a valid #VisuUiDumpDialog object.
 */
#define VISU_UI_IS_DUMP_DIALOG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_UI_TYPE_DUMP_DIALOG))
/**
 * VISU_UI_IS_DUMP_DIALOG_CLASS:
 * @klass: the class to test.
 *
 * Return if the given class is a valid #VisuUiDumpDialogClass class.
 */
#define VISU_UI_IS_DUMP_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), VISU_UI_TYPE_DUMP_DIALOG))
/**
 * VISU_UI_DUMP_DIALOG_GET_CLASS:
 * @obj: the widget to get the class of.
 *
 * Get the class of the given object.
 */
#define VISU_UI_DUMP_DIALOG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_UI_TYPE_DUMP_DIALOG, VisuUiDumpDialogClass))

/**
 * VisuUiDumpDialog
 *
 * Private structure to store informations of a #VisuUiDumpDialog object.
 */
typedef struct _VisuUiDumpDialog VisuUiDumpDialog;
/**
 * VisuUiDumpDialogClass
 *
 * Private structure to store informations of a #VisuUiDumpDialogClass object.
 */
typedef struct _VisuUiDumpDialogClass VisuUiDumpDialogClass;

/**
 * visu_ui_dump_dialog_get_type
 *
 * #GType are unique numbers to identify objects.
 *
 * Returns: the #GType associated with #VisuUiDumpDialog objects.
 */
GType          visu_ui_dump_dialog_get_type        (void);
GtkWidget* visu_ui_dump_dialog_new(VisuData *dataObj, GtkWindow *parent,
                                   const gchar *suggestedFilename,
                                   gint suggestedWidth, gint suggestedHeight);

/**
 * visu_ui_dump_dialog_getFilename:
 * @dialog: a #VisuUiDumpDialog object.
 *
 * Retrieve the chosen filename.
 *
 * Returns: a read-only string.
 */
gchar* visu_ui_dump_dialog_getFilename(VisuUiDumpDialog *dialog);
/**
 * visu_ui_dump_dialog_getType:
 * @dialog: a #VisuUiDumpDialog object.
 *
 * Retrieve the chosen VisuDump.
 *
 * Returns: (transfer none): the selected format (ToolFileFormat and write method).
 */
VisuDump* visu_ui_dump_dialog_getType(VisuUiDumpDialog *dialog);
/**
 * visu_ui_dump_dialog_getProgressBar:
 * @dialog: a #VisuUiDumpDialog object.
 *
 * Retrieve interesting widget.
 *
 * Returns: (transfer none): a pointer to the progress bar.
 */
GtkProgressBar* visu_ui_dump_dialog_getProgressBar(VisuUiDumpDialog *dialog);
/**
 * visu_ui_dump_dialog_getCancelButton:
 * @dialog: a #VisuUiDumpDialog object.
 *
 * Retrieve interesting widget.
 *
 * Returns: (transfer none): a pointer to the cancel button.
 */
GtkButton* visu_ui_dump_dialog_getCancelButton(VisuUiDumpDialog *dialog);
/**
 * visu_ui_dump_dialog_getWidth:
 * @dialog: a #VisuUiDumpDialog object.
 *
 * Retrieve request image size.
 *
 * Returns: the width value.
 */
gint visu_ui_dump_dialog_getWidth(VisuUiDumpDialog *dialog);
/**
 * visu_ui_dump_dialog_getHeight:
 * @dialog: a #VisuUiDumpDialog object.
 *
 * Retrieve request image size.
 *
 * Returns: the height value.
 */
gint visu_ui_dump_dialog_getHeight(VisuUiDumpDialog *dialog);
/**
 * visu_ui_dump_dialog_start:
 * @dialog: a #VisuUiDumpDialog object.
 *
 * Make the file chooser part insensitive during dump, only the
 * progress bar and the abort button are kept sensitive.
 */
void visu_ui_dump_dialog_start(VisuUiDumpDialog *dialog);
/**
 * visu_ui_dump_dialog_stop:
 * @dialog: a #VisuUiDumpDialog object.
 *
 * Return the filechooser to a sensitive state.
 */
void visu_ui_dump_dialog_stop(VisuUiDumpDialog *dialog);


G_END_DECLS

#endif
