/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "iface_maskable.h"

#include "config.h"

/**
 * SECTION:iface_maskable
 * @short_description: an interface for maskable objects.
 *
 * <para>This interface describes objects that can be masked by
 * others. The implementation should provide one method to reset the
 * visibility to fully visible. In addition, it is defining a signal
 * that can be triggered when calling visu_maskable_visibilityChanged().</para> 
 */


/* enum { */
/*   PROP_0, */
/*   N_PROPS */
/* }; */
/* static GParamSpec *properties[N_PROPS]; */

enum
  {
    REQUEST_SIGNAL,
    VISIBILITY_CHANGED_SIGNAL,
    NB_SIGNAL
  };

/* Internal variables. */
static guint _signals[NB_SIGNAL] = { 0 };

/* Maskable interface. */
G_DEFINE_INTERFACE(VisuMaskable, visu_maskable, G_TYPE_OBJECT)

static void visu_maskable_default_init(VisuMaskableInterface *iface)
{
  /**
   * VisuMaskable::visibility-changed:
   * @maskable: the object which received the signal ;
   *
   * Gets emitted when one or more nodes have changed of
   * visibility. Some may have appeared, some may have disappeared.
   *
   * Since: 3.2
   */
  _signals[VISIBILITY_CHANGED_SIGNAL] =
    g_signal_new("visibility-changed", G_TYPE_FROM_INTERFACE(iface),
		  G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		  0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
		  G_TYPE_NONE, 0);
}

/**
 * visu_maskable_resetVisibility:
 * @maskable: a #VisuMaskable object.
 *
 * Reset the visibility of all elements in @maskable to visible.
 *
 * Since: 3.8
 *
 * Returns: TRUE if any element visibility has changed.
 **/
gboolean visu_maskable_resetVisibility(VisuMaskable *maskable)
{
  g_return_val_if_fail(VISU_IS_MASKABLE(maskable), FALSE);

  if (!VISU_MASKABLE_GET_INTERFACE(maskable)->reset_visibility)
    return FALSE;

  return VISU_MASKABLE_GET_INTERFACE(maskable)->reset_visibility(maskable);
}
/**
 * visu_maskable_visibilityChanged:
 * @maskable: a #VisuMaskable object.
 *
 * A convenience routine to emit the "visibility-changed" signal.
 *
 * Since: 3.8
 **/
void visu_maskable_visibilityChanged(VisuMaskable *maskable)
{
  g_return_if_fail(VISU_IS_MASKABLE(maskable));

  g_signal_emit(maskable, _signals[VISIBILITY_CHANGED_SIGNAL], 0);
}
