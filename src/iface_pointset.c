/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "iface_pointset.h"

#include "config.h"

/**
 * SECTION:iface_pointset
 * @short_description: Defines a common interface for objects with
 * points inside a box that can be translated or expanded.
 * @See_also: #VisuData, #VisuSurface, #VisuScalarField
 * @Title: VisuPointset
 *
 * <para></para>
 */

enum {
  PROP_0,
  PROP_USE_TRANS,
  PROP_TRANS,
  PROP_RED_TRANS,
  PROP_MODULO,
  N_PROPS
};
static GParamSpec *properties[N_PROPS];

/* enum */
/*   { */
/*     NB_SIGNAL */
/*   }; */

/* /\* Internal variables. *\/ */
/* static guint _signals[NB_SIGNAL] = { 0 }; */

/* Pointset interface. */
G_DEFINE_INTERFACE(VisuPointset, visu_pointset, VISU_TYPE_BOXED)

static void visu_pointset_default_init(VisuPointsetInterface *iface)
{
  /**
   * VisuPointset::use-translation:
   *
   * Wether translations are applied or not.
   *
   * Since: 3.8
   */
  properties[PROP_USE_TRANS] =
    g_param_spec_boolean("use-translation", "Use translation",
                         "Use translations along all axis", FALSE, G_PARAM_READWRITE);
  g_object_interface_install_property(iface, properties[PROP_USE_TRANS]);

  /**
   * VisuPointset::translation:
   *
   * The translation applied on [x,y,z] axis.
   *
   * Since: 3.8
   */
  properties[PROP_TRANS] =
    g_param_spec_boxed("translation", "Translations along all axis",
                       "Translations along all axis", TOOL_TYPE_VECTOR, G_PARAM_READWRITE);
  g_object_interface_install_property(iface, properties[PROP_TRANS]);

  /**
   * VisuPointset::reduced-translation:
   *
   * The translation applied on the box axis, normalised by box lengths.
   *
   * Since: 3.8
   */
  properties[PROP_RED_TRANS] =
    g_param_spec_boxed("reduced-translation", "Translations along box axis",
                       "Translations along box axis", TOOL_TYPE_VECTOR, G_PARAM_READWRITE);
  g_object_interface_install_property(iface, properties[PROP_RED_TRANS]);

  /**
   * VisuPointset::in-the-box:
   *
   * The status of the position of all nodes.
   *
   * Since: 3.8
   */
  properties[PROP_MODULO] =
    g_param_spec_boolean("in-the-box", "Constrain nodes in the box",
                         "All nodes are constrained in the box", FALSE, G_PARAM_READWRITE);
  g_object_interface_install_property(iface, properties[PROP_MODULO]);
}

/**
 * visu_pointset_getTranslation:
 * @self: a #VisuPointset object.
 * @trans: (array fixed-size=3) (out): a location to store the
 * translation values.
 *
 * Retrieves the translations of @self.
 *
 * Since: 3.8
 **/
void visu_pointset_getTranslation(VisuPointset *self, float trans[3])
{
  g_return_if_fail(VISU_IS_POINTSET(self));

  VISU_POINTSET_GET_INTERFACE(self)->get_translation(self, trans);
}
/**
 * visu_pointset_setTranslation:
 * @self: a #VisuPointset object.
 * @trans: (array fixed-size=3): the translations.
 * @withModulo: a boolean.
 *
 * Apply the given translation values to @self. If @withModulo is
 * given, all points inside the pointset will be shifted inside the
 * periodic cell.
 *
 * Since: 3.8
 *
 * Returns: FALSE @pointset was already pointset with the #VisuBox of @box.
 **/
gboolean visu_pointset_setTranslation(VisuPointset *self, float trans[3], gboolean withModulo)
{
  g_return_val_if_fail(VISU_IS_POINTSET(self), FALSE);

  return VISU_POINTSET_GET_INTERFACE(self)->set_translation(self, trans, withModulo);
}
/**
 * visu_pointset_setTranslationActive:
 * @self: a #VisuPointset object.
 * @status: a boolean.
 *
 * Set the if the translations are applied or not to @self.
 *
 * Since: 3.8
 *
 * Returns: TRUE if status is changed.
 **/
gboolean visu_pointset_setTranslationActive(VisuPointset *self, gboolean status)
{
  g_return_val_if_fail(VISU_IS_POINTSET(self), FALSE);

  return VISU_POINTSET_GET_INTERFACE(self)->set_translationActive(self, status);
}
/**
 * visu_pointset_shift:
 * @self: a #VisuPointset object.
 * @delta: (array fixed-size=3): a translation.
 * @withModulo: a boolean.
 *
 * Shift the translation by @delta.
 *
 * Since: 3.8
 *
 * Returns: TRUE if translation is changed.
 **/
gboolean visu_pointset_shift(VisuPointset *self, const gfloat delta[3],
                             gboolean withModulo)
{
  gfloat trans[3];

  visu_pointset_getTranslation(self, trans);
  trans[0] += delta[0];
  trans[1] += delta[1];
  trans[2] += delta[2];
  return visu_pointset_setTranslation(self, trans, withModulo);
}

/**
 * visu_pointset_setInTheBox:
 * @self: a #VisuPointset object.
 * @status: a boolean.
 *
 * Update all node positions inside @self to be constrained inside the
 * box (@status is TRUE), or release all previous position shift to
 * original position without constrain (@status is FALSE).
 *
 * Since: 3.8
 *
 * Returns: TRUE, if any node has changed position.
 **/
gboolean visu_pointset_setInTheBox(VisuPointset *self, gboolean status)
{
  g_return_val_if_fail(VISU_IS_POINTSET(self), FALSE);

  return VISU_POINTSET_GET_INTERFACE(self)->set_inTheBox(self, status);
}

/**
 * visu_pointset_setTranslationPeriodic:
 * @self: a #VisuPointset object ;
 * @trans: (in) (array fixed-size=3): an array of floating point
 * values.
 * @withModulo: a boolean.
 *
 * This sets the translations of the specified #VisuPointset only in
 * periodic boundary conditions. Points are also constrainted inside
 * the box in the periodic directions if @withModulo is TRUE.
 *
 * Returns: TRUE if any translations have been changed.
 */
gboolean visu_pointset_setTranslationPeriodic(VisuPointset* self, float trans[3],
                                              gboolean withModulo)
{
  VisuBoxBoundaries bc;
  float xyz_[3];

  g_return_val_if_fail(VISU_IS_POINTSET(self), FALSE);

  visu_pointset_getTranslation(self, xyz_);
  bc = visu_box_getBoundary(visu_boxed_getBox(VISU_BOXED(self)));
  if (bc & TOOL_XYZ_MASK_X)
    xyz_[0] = trans[0];
  if (bc & TOOL_XYZ_MASK_Y)
    xyz_[1] = trans[1];
  if (bc & TOOL_XYZ_MASK_Z)
    xyz_[2] = trans[2];

  return visu_pointset_setTranslation(self, xyz_, withModulo);
}
/**
 * visu_pointset_setBoxTranslation:
 * @self: a #VisuPointset object ;
 * @boxTrans: (in) (array fixed-size=3): an array of floating point
 * values.
 * @withModulo: a boolean.
 *
 * This sets the translations of the specified #VisuPointset only along
 * periodic axis. The translation @boxTrans gives normalized values
 * along all box axis. Points are also constrainted inside
 * the box in the periodic directions if @withModulo is TRUE.
 *
 * Since: 3.8
 *
 * Returns: TRUE if any translations have been changed.
 **/
gboolean visu_pointset_setBoxTranslation(VisuPointset* self, float boxTrans[3],
                                         gboolean withModulo)
{
  VisuBox *box;
  gfloat trans[3], orig[3];
  float zeros[3] = {0.f, 0.f, 0.f};

  box = visu_boxed_getBox(VISU_BOXED(self));
  visu_box_convertBoxCoordinatestoXYZ(box, orig, zeros);
  visu_box_convertBoxCoordinatestoXYZ(box, trans, boxTrans);
  trans[0] -= orig[0];
  trans[1] -= orig[1];
  trans[2] -= orig[2];
  return visu_pointset_setTranslationPeriodic(self, trans, withModulo);
}
/**
 * visu_pointset_getTranslationPeriodicStatus:
 * @self: a #VisuPointset object.
 *
 * Tests if any of the translation in the periodic boundary
 * conditions are not null.
 *
 * Since: 3.8
 *
 * Returns: TRUE if any of the translation in the periodic boundary
 * conditions are not null.
 **/
gboolean visu_pointset_getTranslationPeriodicStatus(VisuPointset *self)
{
  VisuBoxBoundaries bc;
  float xyz_[3];

  g_return_val_if_fail(VISU_IS_POINTSET(self), FALSE);

  visu_pointset_getTranslation(self, xyz_);
  bc = visu_box_getBoundary(visu_boxed_getBox(VISU_BOXED(self)));
  return ((bc & TOOL_XYZ_MASK_X) && xyz_[0] != 0.f) ||
    ((bc & TOOL_XYZ_MASK_Y) && xyz_[1] != 0.f) ||
    ((bc & TOOL_XYZ_MASK_Z) && xyz_[2] != 0.f);
}
/**
 * visu_pointset_applyTranslation:
 * @self: a #VisuPointset object.
 *
 * Apply all the translation (node and box) on each node coordinates
 * and reset both translations to zero.
 *
 * Since: 3.8
 **/
void visu_pointset_applyTranslation(VisuPointset *self)
{
  g_return_if_fail(VISU_IS_POINTSET(self));

  VISU_POINTSET_GET_INTERFACE(self)->apply_translation(self);
}
