/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2006)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2006)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "visu_openGL.h"
#include <visu_tools.h>

struct VisuPixmapContext_struct
{
  GdkGLConfig *glConfig;
  GdkGLContext *glContext;
  GdkGLPixmap *glPixmap;
  GdkPixmap   *pixmap;
  guint width, height;
};


GdkGLConfig* visu_gl_getGLConfig(GdkScreen *screen)
{
  GdkGLConfig *glconfig;
  int list[] = {
    GDK_GL_RGBA,
    GDK_GL_RED_SIZE, 1, GDK_GL_GREEN_SIZE, 1, GDK_GL_BLUE_SIZE, 1,
    GDK_GL_DEPTH_SIZE, 1,
    GDK_GL_DOUBLEBUFFER,
    GDK_GL_STEREO,
    GDK_GL_ATTRIB_LIST_NONE
  };
  
  /* Create the GdkGLConfig for this screen. */
  glconfig = gdk_gl_config_new_for_screen(screen, list);
  if ( !glconfig )
    {
      list[10] = (int)GDK_GL_ATTRIB_LIST_NONE; 
      glconfig = gdk_gl_config_new_for_screen(screen, list);
      if ( !glconfig )
	g_error("Cannot find a matching visual.");
    }

  return glconfig;
}

GLuint visu_gl_initFontList(guint size)
{
  PangoFont *font;
  GLuint BASE;
  gchar *str;
/*   GtkWidget *wd; */
        
  DBG_fprintf(stderr, "Visu GtkGlExt: initialise fonts.\n");

/*   wd = gtk_label_new("toto"); */
  BASE = 123;
  str = g_strdup_printf("sans %d", size);
  font = gdk_gl_font_use_pango_font(pango_font_description_from_string(str), 0, 128, BASE);
  g_free(str);
/*   font = gdk_gl_font_use_pango_font(gtk_widget_get_style(wd)->font_desc, */
/* 				    0, 128, BASE); */
  if (!font) 
    g_error("Font not available for OpenGLification.\n"); 
/*   g_object_unref(G_OBJECT(wd)); */
 
  return BASE; 
}

VisuPixmapContext* visu_pixmap_context_new(guint width, guint height)
{
  GdkScreen *screen;
  gboolean res;
  VisuPixmapContext *image;

  DBG_fprintf(stderr, "Visu GtkGlExt: creating a off-screen buffer (%dx%d).\n",
	      width, height);
  image            = g_malloc(sizeof(VisuPixmapContext));
  image->glConfig  = (GdkGLConfig*)0;
  image->glContext = (GdkGLContext*)0;
  image->pixmap    = (GdkPixmap*)0;
  image->glPixmap  = (GdkGLPixmap*)0;

  screen = gdk_screen_get_default();
  image->glConfig = visu_gl_getGLConfig(screen);

  image->width  = width;
  image->height = height;
  image->pixmap = gdk_pixmap_new((GdkDrawable*)0, (gint)width, (gint)height,
				 gdk_gl_config_get_depth(image->glConfig));
  if (!image->pixmap)
    {
      g_warning("Cannot allocate a GdkPixmap for the indirect rendering.");
      visu_pixmap_context_free(image);
      return (VisuPixmapContext*)0;
    }

  image->glPixmap = gdk_gl_pixmap_new(image->glConfig, image->pixmap, NULL);
  if (!image->glPixmap)
    {
      g_warning("Cannot allocate a GdkGLPixmap for the indirect rendering.");
      visu_pixmap_context_free(image);
      return (VisuPixmapContext*)0;
    }

  image->glContext = gdk_gl_context_new(GDK_GL_DRAWABLE(image->glPixmap),
					(GdkGLContext*)0, FALSE, GDK_GL_RGBA_TYPE);
  if (!image->glContext)
    {
      g_warning("Cannot create indirect context.");
      visu_pixmap_context_free(image);
      return (VisuPixmapContext*)0;
    }

  res = gdk_gl_drawable_make_current((GdkGLDrawable*)image->glPixmap, image->glContext);
  if (!res)
    {
      g_warning("Cannot make current the pixmap context.");
      visu_pixmap_context_free(image);
      return (VisuPixmapContext*)0;
    }

  return image;
}

void visu_pixmap_context_free(VisuPixmapContext *dumpData)
{
  if (!dumpData)
    return;

  if (dumpData->pixmap)
    g_object_unref(dumpData->pixmap);

  if (dumpData->glConfig)
    g_object_unref(dumpData->glConfig);

  if (dumpData->glPixmap)
    gdk_gl_pixmap_destroy(dumpData->glPixmap);

  if (dumpData->glContext)
    gdk_gl_context_destroy(dumpData->glContext);

  g_free(dumpData);
}
