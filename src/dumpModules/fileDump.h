/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_DUMP_DATA_H
#define VISU_DUMP_DATA_H

#include <glib.h>
#include <glib-object.h>

#include <visu_dump.h>
#include <visu_data.h>

G_BEGIN_DECLS

/***************/
/* Public part */
/***************/

/**
 * VISU_TYPE_DUMP_DATA:
 *
 * return the type of #VisuDumpData.
 */
#define VISU_TYPE_DUMP_DATA	     (visu_dump_data_get_type ())
/**
 * VISU_DUMP_DATA:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuDumpData type.
 */
#define VISU_DUMP_DATA(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_DUMP_DATA, VisuDumpData))
/**
 * VISU_DUMP_DATA_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuDumpDataClass.
 */
#define VISU_DUMP_DATA_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_DUMP_DATA, VisuDumpDataClass))
/**
 * VISU_IS_DUMP_DATA_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuDumpData object.
 */
#define VISU_IS_DUMP_DATA_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_DUMP_DATA))
/**
 * VISU_IS_DUMP_DATA_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuDumpDataClass class.
 */
#define VISU_IS_DUMP_DATA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_DUMP_DATA))
/**
 * VISU_DUMP_DATA_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_DUMP_DATA_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_DUMP_DATA, VisuDumpDataClass))

/**
 * VisuDumpDataPrivate:
 * 
 * Private data for #VisuDumpData objects.
 */
typedef struct _VisuDumpDataPrivate VisuDumpDataPrivate;

/**
 * VisuDumpData:
 * 
 * Common name to refer to a #_VisuDumpData.
 */
typedef struct _VisuDumpData VisuDumpData;
struct _VisuDumpData
{
  VisuDump parent;

  VisuDumpDataPrivate *priv;
};

/**
 * VisuDumpDataClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuDumpDataClass.
 */
typedef struct _VisuDumpDataClass VisuDumpDataClass;
struct _VisuDumpDataClass
{
  VisuDumpClass parent;
};

/**
 * visu_dump_data_get_type:
 *
 * This method returns the type of #VisuDumpData, use
 * VISU_TYPE_DUMP_DATA instead.
 *
 * Since: 3.7
 *
 * Returns: the type of #VisuDumpData.
 */
GType visu_dump_data_get_type(void);

/**
 * VisuDumpDataWriteFunc:
 * @format: a #VisuDumpData object ;
 * @fileName: a string that defined the file to write to ;
 * @dataObj: the #VisuData to be exported ;
 * @error: (allow-none): a location to store some error (not NULL) ;
 *
 * This is a prototype of a method implemented by a dumping extension that is called
 * when the current rendering must be dumped to a file.
 *
 * Returns: TRUE if everything went right.
 */
typedef gboolean (*VisuDumpDataWriteFunc) (VisuDumpData *format, const char* fileName,
                                           VisuData *dataObj, GError **error);

VisuDumpData* visu_dump_data_new(const gchar* descr, const gchar** patterns,
                                 VisuDumpDataWriteFunc method);

gboolean visu_dump_data_write(VisuDumpData *dump, const char* fileName,
                              VisuData *dataObj, GError **error);

G_END_DECLS

#endif
