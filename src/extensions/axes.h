/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef AXES_H
#define AXES_H

#include <visu_extension.h>
#include <visu_box.h>
#include <openGLFunctions/view.h>
#include "iface_lined.h"

/**
 * VISU_TYPE_GL_EXT_AXES:
 *
 * return the type of #VisuGlExtAxes.
 *
 * Since: 3.7
 */
#define VISU_TYPE_GL_EXT_AXES	     (visu_gl_ext_axes_get_type ())
/**
 * VISU_GL_EXT_AXES:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuGlExtAxes type.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_AXES(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL_EXT_AXES, VisuGlExtAxes))
/**
 * VISU_GL_EXT_AXES_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuGlExtAxesClass.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_AXES_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL_EXT_AXES, VisuGlExtAxesClass))
/**
 * VISU_IS_GL_EXT_AXES:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuGlExtAxes object.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_AXES(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL_EXT_AXES))
/**
 * VISU_IS_GL_EXT_AXES_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuGlExtAxesClass class.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_AXES_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL_EXT_AXES))
/**
 * VISU_GL_EXT_AXES_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_AXES_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL_EXT_AXES, VisuGlExtAxesClass))

typedef struct _VisuGlExtAxes        VisuGlExtAxes;
typedef struct _VisuGlExtAxesPrivate VisuGlExtAxesPrivate;
typedef struct _VisuGlExtAxesClass   VisuGlExtAxesClass;

/**
 * VisuGlExtAxes:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
struct _VisuGlExtAxes
{
  VisuGlExt parent;

  VisuGlExtAxesPrivate *priv;
};

/**
 * VisuGlExtAxesClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtAxesClass structure.
 *
 * Since: 3.7
 */
struct _VisuGlExtAxesClass
{
  VisuGlExtClass parent;
};

/**
 * VISU_GL_EXT_AXES_ID:
 *
 * The id used to identify this extension, see
 * visu_gl_ext_rebuild() for instance.
 */
#define VISU_GL_EXT_AXES_ID "Axes"

/**
 * visu_gl_ext_axes_get_type:
 *
 * This method returns the type of #VisuGlExtAxes, use
 * VISU_TYPE_GL_EXT_AXES instead.
 *
 * Since: 3.7
 *
 * Returns: the type of #VisuGlExtAxes.
 */
GType visu_gl_ext_axes_get_type(void);

VisuGlExtAxes* visu_gl_ext_axes_new(const gchar *name);

gboolean visu_gl_ext_axes_setBasisFromBox(VisuGlExtAxes *axes, VisuBox *box);
gboolean visu_gl_ext_axes_setBasis(VisuGlExtAxes *axes, double matrix[3][3]);
gboolean visu_gl_ext_axes_setPosition(VisuGlExtAxes *axes, float xpos, float ypos);

gboolean visu_gl_ext_axes_setLabel(VisuGlExtAxes *axes, const gchar *lbl, ToolXyzDir dir);
gboolean visu_gl_ext_axes_setLengthFactor(VisuGlExtAxes *axes, float factor);
gboolean visu_gl_ext_axes_useOrientation(VisuGlExtAxes *axes, gboolean use);
gboolean visu_gl_ext_axes_setOrientationTop(VisuGlExtAxes *axes,
                                            const gfloat top[3], int dir);

void visu_gl_ext_axes_getPosition(VisuGlExtAxes *axes, float *xpos, float *ypos);
float visu_gl_ext_axes_getLengthFactor(VisuGlExtAxes *axes);

#endif
