/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef SCALE_H
#define SCALE_H

#include "iface_lined.h"
#include <visu_extension.h>
#include <openGLFunctions/view.h>

/**
 * VISU_TYPE_GL_EXT_SCALE:
 *
 * return the type of #VisuGlExtScale.
 *
 * Since: 3.3
 */
#define VISU_TYPE_GL_EXT_SCALE	     (visu_gl_ext_scale_get_type ())
/**
 * VISU_GL_EXT_SCALE:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuGlExtScale type.
 *
 * Since: 3.3
 */
#define VISU_GL_EXT_SCALE(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL_EXT_SCALE, VisuGlExtScale))
/**
 * VISU_GL_EXT_SCALE_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuGlExtScaleClass.
 *
 * Since: 3.3
 */
#define VISU_GL_EXT_SCALE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL_EXT_SCALE, VisuGlExtScaleClass))
/**
 * VISU_IS_GL_EXT_SCALE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuGlExtScale object.
 *
 * Since: 3.3
 */
#define VISU_IS_GL_EXT_SCALE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL_EXT_SCALE))
/**
 * VISU_IS_GL_EXT_SCALE_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuGlExtScaleClass class.
 *
 * Since: 3.3
 */
#define VISU_IS_GL_EXT_SCALE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL_EXT_SCALE))
/**
 * VISU_GL_EXT_SCALE_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.3
 */
#define VISU_GL_EXT_SCALE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL_EXT_SCALE, VisuGlExtScaleClass))

typedef struct _VisuGlExtScale VisuGlExtScale;
typedef struct _VisuGlExtScalePrivate VisuGlExtScalePrivate;
typedef struct _VisuGlExtScaleClass VisuGlExtScaleClass;

/**
 * VisuGlExtScale:
 *
 * All fields are private, use the access routines.
 *
 * Since: 3.3
 */
struct _VisuGlExtScale
{
  VisuGlExt parent;

  VisuGlExtScalePrivate *priv;
};
/**
 * VisuGlExtScaleClass:
 * @parent: parent structure.
 *
 * An opaque structure.
 *
 * Since: 3.3
 */
struct _VisuGlExtScaleClass
{
  VisuGlExtClass parent;
};

/**
 * visu_gl_ext_scale_get_type:
 *
 * This method returns the type of #VisuGlExtScale, use %VISU_TYPE_GL_EXT_SCALE instead.
 *
 * Returns: the type of #VisuGlExtScale.
 *
 * Since: 3.3
 */
GType visu_gl_ext_scale_get_type(void);

VisuGlExtScale* visu_gl_ext_scale_new(const gchar *name);

guint visu_gl_ext_scale_add(VisuGlExtScale *scale, float origin[3], float orientation[3],
                            float length, const gchar *legend);

gboolean visu_gl_ext_scale_setOrigin(VisuGlExtScale *scale, guint i, float xyz[3], int mask);
gboolean visu_gl_ext_scale_setOrientation(VisuGlExtScale *scale, guint i, float xyz[3], int mask);
gboolean visu_gl_ext_scale_setLength(VisuGlExtScale *scale, guint i, float lg);
gboolean visu_gl_ext_scale_setLegend(VisuGlExtScale *scale, guint i, const gchar *value);

guint visu_gl_ext_scale_getNArrows(VisuGlExtScale *scale);
const gchar* visu_gl_ext_scale_getLegend(VisuGlExtScale *scale, guint i);
float visu_gl_ext_scale_getLength(VisuGlExtScale *scale, guint i);
float* visu_gl_ext_scale_getOrigin(VisuGlExtScale *scale, guint i);
float* visu_gl_ext_scale_getOrientation(VisuGlExtScale *scale, guint i);

#endif
