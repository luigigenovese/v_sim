/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#ifndef PAIRS_H
#define PAIRS_H

#include <GL/gl.h>

#include <visu_extension.h>
#include <visu_pairset.h>
#include <renderingMethods/iface_nodeArrayRenderer.h>
#include <pairsModeling/linkRenderer.h>

G_BEGIN_DECLS

/**
 * VISU_TYPE_GL_EXT_PAIRS:
 *
 * return the type of #VisuGlExtPairs.
 *
 * Since: 3.7
 */
#define VISU_TYPE_GL_EXT_PAIRS	     (visu_gl_ext_pairs_get_type ())
/**
 * VISU_GL_EXT_PAIRS:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuGlExtPairs type.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_PAIRS(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL_EXT_PAIRS, VisuGlExtPairs))
/**
 * VISU_GL_EXT_PAIRS_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuGlExtPairsClass.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_PAIRS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL_EXT_PAIRS, VisuGlExtPairsClass))
/**
 * VISU_IS_GL_EXT_PAIRS:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuGlExtPairs object.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_PAIRS(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL_EXT_PAIRS))
/**
 * VISU_IS_GL_EXT_PAIRS_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuGlExtPairsClass class.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_PAIRS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL_EXT_PAIRS))
/**
 * VISU_GL_EXT_PAIRS_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_PAIRS_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL_EXT_PAIRS, VisuGlExtPairsClass))

typedef struct _VisuGlExtPairs        VisuGlExtPairs;
typedef struct _VisuGlExtPairsPrivate VisuGlExtPairsPrivate;
typedef struct _VisuGlExtPairsClass   VisuGlExtPairsClass;

struct _VisuGlExtPairs
{
  VisuGlExt parent;

  VisuGlExtPairsPrivate *priv;
};

struct _VisuGlExtPairsClass
{
  VisuGlExtClass parent;
};

/**
 * visu_gl_ext_pairs_get_type:
 *
 * This method returns the type of #VisuGlExtPairs, use
 * VISU_TYPE_GL_EXT_PAIRS instead.
 *
 * Since: 3.7
 *
 * Returns: the type of #VisuGlExtPairs.
 */
GType visu_gl_ext_pairs_get_type(void);

/**
 * VISU_GL_EXT_PAIRS_ID:
 *
 * The id used to identify this extension, see
 * visu_gl_ext_rebuild() for instance.
 */
#define VISU_GL_EXT_PAIRS_ID "Pairs"

VisuGlExtPairs* visu_gl_ext_pairs_new(const gchar *name);

gboolean visu_gl_ext_pairs_setData(VisuGlExtPairs *pairs, VisuData *data);

gboolean visu_gl_ext_pairs_setDataRenderer(VisuGlExtPairs *pairs,
                                           VisuNodeArrayRenderer *renderer);
VisuNodeArrayRenderer* visu_gl_ext_pairs_getDataRenderer(VisuGlExtPairs *pairs);

VisuPairSet* visu_gl_ext_pairs_getSet(VisuGlExtPairs *pairs);

GList* visu_gl_ext_pairs_getAllLinkRenderer(VisuGlExtPairs *pairs);
VisuPairLinkRenderer* visu_gl_ext_pairs_getLinkRenderer(VisuGlExtPairs *pairs,
                                                        VisuPairLink *data);
gboolean visu_gl_ext_pairs_setLinkRenderer(VisuGlExtPairs *pairs, VisuPairLink *data,
                                           VisuPairLinkRenderer *renderer);

G_END_DECLS

#endif
