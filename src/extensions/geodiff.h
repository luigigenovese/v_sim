/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2013)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2013)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#ifndef GEODIFF_H
#define GEODIFF_H

#include "node_vectors.h"

/**
 * VISU_TYPE_GL_EXT_GEODIFF:
 *
 * return the type of #VisuGlExtGeodiff.
 *
 * Since: 3.8
 */
#define VISU_TYPE_GL_EXT_GEODIFF	     (visu_gl_ext_geodiff_get_type ())
/**
 * VISU_GL_EXT_GEODIFF:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuGlExtGeodiff type.
 *
 * Since: 3.8
 */
#define VISU_GL_EXT_GEODIFF(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL_EXT_GEODIFF, VisuGlExtGeodiff))
/**
 * VISU_GL_EXT_GEODIFF_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuGlExtGeodiffClass.
 *
 * Since: 3.8
 */
#define VISU_GL_EXT_GEODIFF_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL_EXT_GEODIFF, VisuGlExtGeodiffClass))
/**
 * VISU_IS_GL_EXT_GEODIFF:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuGlExtGeodiff object.
 *
 * Since: 3.8
 */
#define VISU_IS_GL_EXT_GEODIFF(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL_EXT_GEODIFF))
/**
 * VISU_IS_GL_EXT_GEODIFF_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuGlExtGeodiffClass class.
 *
 * Since: 3.8
 */
#define VISU_IS_GL_EXT_GEODIFF_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL_EXT_GEODIFF))
/**
 * VISU_GL_EXT_GEODIFF_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.8
 */
#define VISU_GL_EXT_GEODIFF_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL_EXT_GEODIFF, VisuGlExtGeodiffClass))

typedef struct _VisuGlExtGeodiff        VisuGlExtGeodiff;
typedef struct _VisuGlExtGeodiffClass   VisuGlExtGeodiffClass;

struct _VisuGlExtGeodiff
{
  VisuGlExtNodeVectors parent;
};

struct _VisuGlExtGeodiffClass
{
  VisuGlExtNodeVectorsClass parent;
};

/**
 * visu_gl_ext_geodiff_get_type:
 *
 * This method returns the type of #VisuGlExtGeodiff, use
 * VISU_TYPE_GL_EXT_GEODIFF instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuGlExtGeodiff.
 */
GType visu_gl_ext_geodiff_get_type(void);

VisuGlExtGeodiff* visu_gl_ext_geodiff_new(const gchar *name);

#endif
