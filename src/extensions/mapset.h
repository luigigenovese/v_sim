/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (20016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (20016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#ifndef MAPSET_H
#define MAPSET_H

#include "maps.h"
#include "shade.h"

/**
 * VISU_TYPE_GL_EXT_MAP_SET:
 *
 * return the type of #VisuGlExtMapSet.
 *
 * Since: 3.7
 */
#define VISU_TYPE_GL_EXT_MAP_SET	     (visu_gl_ext_map_set_get_type ())
/**
 * VISU_GL_EXT_MAP_SET:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuGlExtMapSet type.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_MAP_SET(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL_EXT_MAP_SET, VisuGlExtMapSet))
/**
 * VISU_GL_EXT_MAP_SET_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuGlExtMapSetClass.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_MAP_SET_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL_EXT_MAP_SET, VisuGlExtMapSetClass))
/**
 * VISU_IS_GL_EXT_MAP_SET:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuGlExtMapSet object.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_MAP_SET(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL_EXT_MAP_SET))
/**
 * VISU_IS_GL_EXT_MAP_SET_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuGlExtMapSetClass class.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_MAP_SET_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL_EXT_MAP_SET))
/**
 * VISU_GL_EXT_MAP_SET_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_MAP_SET_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL_EXT_MAP_SET, VisuGlExtMapSetClass))

typedef struct _VisuGlExtMapSet        VisuGlExtMapSet;
typedef struct _VisuGlExtMapSetPrivate VisuGlExtMapSetPrivate;
typedef struct _VisuGlExtMapSetClass   VisuGlExtMapSetClass;

struct _VisuGlExtMapSet
{
  VisuGlExtMaps parent;

  VisuGlExtMapSetPrivate *priv;
};

struct _VisuGlExtMapSetClass
{
  VisuGlExtMapsClass parent;
};

/**
 * visu_gl_ext_map_set_get_type:
 *
 * This method returns the type of #VisuGlExtMapSet, use
 * VISU_TYPE_GL_EXT_MAP_SET instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuGlExtMapSet.
 */
GType visu_gl_ext_map_set_get_type(void);

VisuGlExtMapSet* visu_gl_ext_map_set_new(const gchar *name);
VisuGlExtShade* visu_gl_ext_map_set_getLegend(VisuGlExtMapSet *mapSet);

void visu_gl_ext_map_set_setField(VisuGlExtMapSet *mapSet, VisuScalarField *field);

VisuMap* visu_gl_ext_map_set_addFromPlane(VisuGlExtMapSet *mapSet, VisuPlane *plane);

void visu_gl_ext_map_set_setPlane(VisuGlExtMapSet *mapSet, VisuMap *map, VisuPlane *plane);
VisuPlane* visu_gl_ext_map_set_getPlane(VisuGlExtMapSet *mapSet, VisuMap *map);

gboolean visu_gl_ext_map_set_setPrecision(VisuGlExtMapSet *mapSet, float prec);
gboolean visu_gl_ext_map_set_setShade(VisuGlExtMapSet *mapSet, ToolShade *shade);
gboolean visu_gl_ext_map_set_setLineColor(VisuGlExtMapSet *mapSet,
                                          const ToolColor *color);
gboolean visu_gl_ext_map_set_setTransparent(VisuGlExtMapSet *mapSet, gboolean alpha);

float visu_gl_ext_map_set_getPrecision(const VisuGlExtMapSet *mapSet);
gboolean visu_gl_ext_map_set_getTransparent(const VisuGlExtMapSet *mapSet);

gboolean visu_gl_ext_map_set_setLines(VisuGlExtMapSet *mapSet, guint nLines);
gboolean visu_gl_ext_map_set_setScaling(VisuGlExtMapSet *mapSet, ToolMatrixScalingFlag scale);
gboolean visu_gl_ext_map_set_setScalingRange(VisuGlExtMapSet *mapSet, const float minMax[2]);

gboolean visu_gl_ext_map_set_export(VisuGlExtMapSet *mapSet, VisuMap *map,
                                    const gchar *filename, VisuMapExportFormat format,
                                    GError **error);

#endif
