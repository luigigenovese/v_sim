/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2011-2013)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2011-2013)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "geodiff.h"

#include <extraFunctions/geometry.h>

#include <visu_configFile.h>


#define FLAG_RESOURCE_ARROW "nodeDisplacement_arrow"
#define DESC_RESOURCE_ARROW "Describe the arrow to be drawn ; four floats (tail lg. tail rd. head lg. head rd., negative values means auto)"
static float arrow[4] = {4.f, .2f, .5f, .3f};

#define FLAG_RESOURCE_RATIO_MIN "nodeDisplacement_minThreshold"
#define DESC_RESOURCE_RATIO_MIN "Choose the minimum value for drawn arrows in geometry differences ; float (ratio threshold if between -1 and 0)"
#define DEFT_RESOURCE_RATIO_MIN -0.1f
static float ratioMin = DEFT_RESOURCE_RATIO_MIN;

#define FLAG_RESOURCE_RATIO_STR "nodeDisplacement_lblThreshold"
#define DESC_RESOURCE_RATIO_STR "Choose the minimum value for labels in geometry differences ; float (ratio threshold if between -1 and 0)"
#define DEFT_RESOURCE_RATIO_STR -0.9f
static float ratioStr = DEFT_RESOURCE_RATIO_STR;

#define FLAG_RESOURCE_MULT "nodeDisplacement_factor"
#define DESC_RESOURCE_MULT "Choose the factor to draw arrows in geometry differences ; float (negative means auto)"
#define DEFT_RESOURCE_MULT -1.f
static float mult     = DEFT_RESOURCE_MULT;

#define DEFT_RESOURCE_SCAL -4.f
static void exportResourcesDiff(GString *data, VisuData *dataObj);


/**
 * SECTION:geodiff
 * @short_description: Draw arrows at each node to represent geodiff.
 *
 * <para>A specialised #VisuGlExtNodeVectors to represent geodiff on nodes.</para>
 */

/**
 * VisuGlExtGeodiffClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtGeodiffClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtGeodiff:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */

#include <extraFunctions/geometry.h>
static float scal     = DEFT_RESOURCE_SCAL;

/* Local callbacks. */
static void onEntryArrow(VisuGlExtGeodiff *geodiff, VisuConfigFileEntry *entry, VisuConfigFile *obj);
static void onEntryRatioMin(VisuGlExtGeodiff *geodiff, VisuConfigFileEntry *entry, VisuConfigFile *obj);
static void onEntryRatioStr(VisuGlExtGeodiff *geodiff, VisuConfigFileEntry *entry, VisuConfigFile *obj);
static void onEntryMult(VisuGlExtGeodiff *geodiff, VisuConfigFileEntry *entry, VisuConfigFile *obj);

G_DEFINE_TYPE(VisuGlExtGeodiff, visu_gl_ext_geodiff, VISU_TYPE_GL_EXT_NODE_VECTORS)

static void visu_gl_ext_geodiff_class_init(VisuGlExtGeodiffClass *klass _U_)
{
  float rgRatio[2] = {-1.f, G_MAXFLOAT};
  VisuConfigFileEntry *resourceEntry;

  DBG_fprintf(stderr, "Visu GlExt Geodiff: creating the class of the object.\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  /* Resources for geodiff extensions. */
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCE_ARROW,
                                                      DESC_RESOURCE_ARROW,
                                                      4, arrow, rgRatio, FALSE);
  visu_config_file_entry_setVersion(resourceEntry, 3.5f);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCE_RATIO_MIN,
                                                      DESC_RESOURCE_RATIO_MIN,
                                                      1, &ratioMin, rgRatio, FALSE);
  visu_config_file_entry_setVersion(resourceEntry, 3.5f);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCE_RATIO_STR,
                                                      DESC_RESOURCE_RATIO_STR,
                                                      1, &ratioStr, rgRatio, FALSE);
  visu_config_file_entry_setVersion(resourceEntry, 3.5f);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCE_MULT,
                                                      DESC_RESOURCE_MULT,
                                                      1, &mult, rgRatio, FALSE);
  visu_config_file_entry_setVersion(resourceEntry, 3.5f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
                                     exportResourcesDiff);
}

static void visu_gl_ext_geodiff_init(VisuGlExtGeodiff *obj)
{
  DBG_fprintf(stderr, "Visu GlExt Geodiff: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  /* Private data. */
  g_signal_connect_object(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_ARROW,
                          G_CALLBACK(onEntryArrow), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_RATIO_MIN,
                          G_CALLBACK(onEntryRatioMin), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_RATIO_STR,
                          G_CALLBACK(onEntryRatioStr), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_MULT,
                          G_CALLBACK(onEntryMult), (gpointer)obj, G_CONNECT_SWAPPED);
}

/**
 * visu_gl_ext_geodiff_new:
 * @name: (allow-none): the name to give to the extension.
 *
 * Creates a new #VisuGlExt to draw geodiff.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtGeodiff* visu_gl_ext_geodiff_new(const gchar *name)
{
  char *name_ = "Geodiff";
  char *description = _("Draw geodiff with vectors.");
  VisuGlExtNodeVectors *geodiff;

  DBG_fprintf(stderr,"Visu GlExt Geodiff: new object.\n");
  geodiff = VISU_GL_EXT_NODE_VECTORS(g_object_new(VISU_TYPE_GL_EXT_GEODIFF,
                                                 "name", (name)?name:name_, "label", _(name),
                                                 "description", description, "nGlObj", 1,
                                                 NULL));
  visu_gl_ext_node_vectors_setCentering(geodiff, VISU_GL_ARROW_CENTERED);
  visu_gl_ext_node_vectors_setColor(geodiff, VISU_COLOR_BRIGHT);

  visu_gl_ext_node_vectors_setRenderedSize(geodiff, scal);
  visu_gl_ext_node_vectors_setNormalisation(geodiff, mult);
  visu_gl_ext_node_vectors_setArrow(geodiff, arrow[0], arrow[1], 10,
                                    arrow[2], arrow[3], 10);
  visu_gl_ext_node_vectors_setVectorThreshold(geodiff, ratioMin);
  visu_gl_ext_node_vectors_setLabelThreshold(geodiff, ratioStr);
  
  return VISU_GL_EXT_GEODIFF(geodiff);
}

/* Callbacks. */
static void onEntryArrow(VisuGlExtGeodiff *geodiff, VisuConfigFileEntry *entry _U_, VisuConfigFile *obj _U_)
{
  float vals[4];

  vals[0] = (arrow[0] > 0.f)?arrow[0]:4.f;
  vals[1] = (arrow[1] > 0.f)?arrow[1]:.2f;
  vals[2] = (arrow[2] > 0.f)?arrow[2]:.5f;
  vals[3] = (arrow[3] > 0.f)?arrow[3]:.3f;

  visu_gl_ext_node_vectors_setArrow(VISU_GL_EXT_NODE_VECTORS(geodiff),
                                    vals[0], vals[1], 10,
                                    vals[2], vals[3], 10);

  visu_gl_ext_node_vectors_setRenderedSize(VISU_GL_EXT_NODE_VECTORS(geodiff),
                                           (arrow[0] > 0.f)?arrow[0]:-4.f);
}
static void onEntryRatioMin(VisuGlExtGeodiff *geodiff, VisuConfigFileEntry *entry _U_, VisuConfigFile *obj _U_)
{
  visu_gl_ext_node_vectors_setVectorThreshold(VISU_GL_EXT_NODE_VECTORS(geodiff),
                                              ratioMin);
}
static void onEntryRatioStr(VisuGlExtGeodiff *geodiff, VisuConfigFileEntry *entry _U_, VisuConfigFile *obj _U_)
{
  visu_gl_ext_node_vectors_setLabelThreshold(VISU_GL_EXT_NODE_VECTORS(geodiff),
                                             ratioStr);
}
static void onEntryMult(VisuGlExtGeodiff *geodiff, VisuConfigFileEntry *entry _U_, VisuConfigFile *obj _U_)
{
  visu_gl_ext_node_vectors_setNormalisation(VISU_GL_EXT_NODE_VECTORS(geodiff),
                                            mult);
}

/* Resources. */
static void exportResourcesDiff(GString *data, VisuData *dataObj _U_)
{
  visu_config_file_exportComment(data, DESC_RESOURCE_ARROW);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_ARROW, NULL,
                               "%f %f %f %f", arrow[0], arrow[1], arrow[2], arrow[3]);

  visu_config_file_exportComment(data, DESC_RESOURCE_MULT);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_MULT, NULL,
                               "%f", mult);

  visu_config_file_exportComment(data, DESC_RESOURCE_RATIO_MIN);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_RATIO_MIN, NULL,
                               "%f", ratioMin);

  visu_config_file_exportComment(data, DESC_RESOURCE_RATIO_STR);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_RATIO_STR, NULL,
                               "%f", ratioStr);

  visu_config_file_exportComment(data, "");
}
