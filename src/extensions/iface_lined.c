/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2014)

	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2014)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "iface_lined.h"

#include <config.h>
#include <coreTools/toolColor.h>

/**
 * SECTION:iface_lined
 * @short_description: Defines a common interface for #VisuGlExt
 * objects with line characteristics.
 * @See_also: #VisuGlExtAxes, #VisuGlExtScale or #VisuGlExtBox
 *
 * <para></para>
 * 
 * Since: 3.8
 */


/* enum */
/*   { */
/*     SET_BOX_SIGNAL, */
/*     NB_SIGNAL */
/*   }; */
/* static guint signals[NB_SIGNAL] = { 0 }; */

enum
  {
    PROP_0,
    COLOR_PROP,
    WIDTH_PROP,
    STIPPLE_PROP,
    N_PROP
  };
static GParamSpec *properties[N_PROP];

/* GlExtLined interface. */
G_DEFINE_INTERFACE(VisuGlExtLined, visu_gl_ext_lined, G_TYPE_OBJECT)

static void visu_gl_ext_lined_default_init(VisuGlExtLinedInterface *iface)
{
  /**
   * VisuGlExtAxes::color:
   *
   * Store the color of the axes.
   *
   * Since: 3.8
   */
  properties[COLOR_PROP] = g_param_spec_boxed("color", "color", "rendering color",
                                              TOOL_TYPE_COLOR, G_PARAM_READWRITE);
  g_object_interface_install_property(iface, properties[COLOR_PROP]);
  /**
   * VisuGlExtAxes::width:
   *
   * Store the line width of the axes.
   *
   * Since: 3.8
   */
  properties[WIDTH_PROP] = g_param_spec_float("width", "line width",
                                              "rendering line width",
                                              0., 10., 1.,
                                              G_PARAM_READWRITE);
  g_object_interface_install_property(iface, properties[WIDTH_PROP]);
  /**
   * VisuGlExtAxes::stipple:
   *
   * Store the line stipple pattern of the axes.
   *
   * Since: 3.8
   */
  properties[STIPPLE_PROP] = g_param_spec_uint("stipple", "line stipple",
                                               "rendering line stipple pattern",
                                               0, 65535, 65535,
                                               G_PARAM_READWRITE);
  g_object_interface_install_property(iface, properties[STIPPLE_PROP]);
}

/**
 * visu_gl_ext_lined_getWidth:
 * @self: the #VisuGlExtLined object to inquire.
 *
 * Read the line width used to draw lines.
 *
 * Since: 3.8
 *
 * Returns: the value of current line width.
 */
gfloat  visu_gl_ext_lined_getWidth(const VisuGlExtLined *self)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_LINED(self), 0.f);
  
  return VISU_GL_EXT_LINED_GET_INTERFACE(self)->get_width(self);
}
/**
 * visu_gl_ext_lined_getStipple:
 * @self: the #VisuGlExtLined object to inquire.
 *
 * Read the line pattern used to draw lines.
 *
 * Since: 3.8
 *
 * Returns: the value of current line pattern.
 */
guint16 visu_gl_ext_lined_getStipple(const VisuGlExtLined *self)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_LINED(self), 0);
  
  return VISU_GL_EXT_LINED_GET_INTERFACE(self)->get_stipple(self);
}
/**
 * visu_gl_ext_lined_getRGBA:
 * @self: the #VisuGlExtLined object to inquire.
 *
 * Read all the colour components of the line (in [0;1]). 
 *
 * Since: 3.8
 *
 * Returns: (array fixed-size=4) (transfer none): four RGB values,
 * private from V_Sim, read only.
 */
gfloat* visu_gl_ext_lined_getRGBA(const VisuGlExtLined *self)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_LINED(self), (gfloat*)0);
  
  return VISU_GL_EXT_LINED_GET_INTERFACE(self)->get_rgba(self);
}

/**
 * visu_gl_ext_lined_setWidth:
 * @self: the #VisuGlExtLined object to modify.
 * @value: value of the desired line width.
 *
 * Method used to change the value of the line width.
 *
 * Since: 3.8
 *
 * Returns: TRUE if modified.
 */
gboolean visu_gl_ext_lined_setWidth(VisuGlExtLined *self, gfloat value)
{
  gboolean ret;

  g_return_val_if_fail(VISU_IS_GL_EXT_LINED(self), FALSE);

  value = CLAMP(value, 0.01f, 10.f);
  if (value == VISU_GL_EXT_LINED_GET_INTERFACE(self)->get_width(self))
    return FALSE;

  ret = VISU_GL_EXT_LINED_GET_INTERFACE(self)->set_width(self, value);
  g_object_notify_by_pspec(G_OBJECT(self), properties[WIDTH_PROP]);
  return ret;
}
/**
 * visu_gl_ext_lined_setStipple:
 * @self: the #VisuGlExtLined object to modify.
 * @value: value of the desired pattern.
 *
 * Method used to change the value of the line stipple.
 *
 * Since: 3.8
 *
 * Returns: TRUE if modified.
 */
gboolean visu_gl_ext_lined_setStipple(VisuGlExtLined *self, guint16 value)
{
  gboolean ret;

  g_return_val_if_fail(VISU_IS_GL_EXT_LINED(self), FALSE);

  if (value == VISU_GL_EXT_LINED_GET_INTERFACE(self)->get_stipple(self))
    return FALSE;

  ret = VISU_GL_EXT_LINED_GET_INTERFACE(self)->set_stipple(self, value);
  g_object_notify_by_pspec(G_OBJECT(self), properties[STIPPLE_PROP]);
  return ret;
}
/**
 * visu_gl_ext_lined_setRGBA:
 * @self: the #VisuGlExtLined object to modify.
 * @values: (array fixed-size=4): a four floats array with values (0
 * <= values <= 1) for the red, the green and the blue color. Only
 * values specified by the mask are really relevant.
 * @mask: use #TOOL_COLOR_MASK_R, #TOOL_COLOR_MASK_G,
 * #TOOL_COLOR_MASK_B, #TOOL_COLOR_MASK_A or a combinaison to
 * indicate what values in the rgb array must be taken into account.
 *
 * Method used to change the value of the color used to draw the line.
 *
 * Since: 3.8
 *
 * Returns: TRUE if modified.
 */
gboolean visu_gl_ext_lined_setRGBA(VisuGlExtLined *self, gfloat values[4], gint mask)
{
  gboolean ret;
  gfloat *rgba;

  g_return_val_if_fail(VISU_IS_GL_EXT_LINED(self), FALSE);

  rgba = VISU_GL_EXT_LINED_GET_INTERFACE(self)->get_rgba(self);
  g_return_val_if_fail(rgba, FALSE);

  ret = FALSE;
  if ((mask & TOOL_COLOR_MASK_R && rgba[0] != values[0]) ||
      (mask & TOOL_COLOR_MASK_G && rgba[1] != values[1]) ||
      (mask & TOOL_COLOR_MASK_B && rgba[2] != values[2]) ||
      (mask & TOOL_COLOR_MASK_A && rgba[3] != values[3]))
    {
      ret = VISU_GL_EXT_LINED_GET_INTERFACE(self)->set_rgba(self, values, mask);
      g_object_notify_by_pspec(G_OBJECT(self), properties[COLOR_PROP]);
    }
  return ret;
}
