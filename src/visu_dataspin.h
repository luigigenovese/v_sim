/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_DATASPIN_H
#define VISU_DATASPIN_H

#include <glib.h>
#include <glib-object.h>

#include "visu_dataatomic.h"

G_BEGIN_DECLS

/**
 * VISU_TYPE_DATA_SPIN:
 *
 * return the type of #VisuDataSpin.
 */
#define VISU_TYPE_DATA_SPIN	     (visu_data_spin_get_type ())
/**
 * VISU_DATA_SPIN:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuDataSpin type.
 */
#define VISU_DATA_SPIN(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_DATA_SPIN, VisuDataSpin))
/**
 * VISU_DATA_SPIN_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuDataSpinClass.
 */
#define VISU_DATA_SPIN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_DATA_SPIN, VisuDataSpinClass))
/**
 * VISU_IS_DATA_SPIN:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuDataSpin object.
 */
#define VISU_IS_DATA_SPIN(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_DATA_SPIN))
/**
 * VISU_IS_DATA_SPIN_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuDataSpinClass class.
 */
#define VISU_IS_DATA_SPIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_DATA_SPIN))
/**
 * VISU_DATA_SPIN_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_DATA_SPIN_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_DATA_SPIN, VisuDataSpinClass))

typedef struct _VisuDataSpinPrivate VisuDataSpinPrivate;
typedef struct _VisuDataSpin VisuDataSpin;

/**
 * VisuDataSpin:
 *
 * Structure used to define #VisuDataSpin objects.
 *
 * Since: 3.8
 */
struct _VisuDataSpin
{
  VisuDataAtomic parent;

  VisuDataSpinPrivate *priv;
};

/**
 * VisuDataSpinClass:
 * @parent: the parent class.
 *
 * A short way to identify #_VisuDataSpinClass structure.
 */
typedef struct _VisuDataSpinClass VisuDataSpinClass;
struct _VisuDataSpinClass
{
  VisuDataAtomicClass parent;
};

/**
 * visu_data_spin_get_type:
 *
 * This method returns the type of #VisuDataSpin, use VISU_TYPE_DATA_SPIN instead.
 *
 * Returns: the type of #VisuDataSpin.
 */
GType visu_data_spin_get_type(void);

VisuDataSpin* visu_data_spin_new(void);
VisuDataSpin* visu_data_spin_new_withFiles(const gchar *atomic, const gchar *spin);

const gchar* visu_data_spin_getFile(VisuDataSpin *data, VisuDataLoader **format);
void visu_data_spin_setFile(VisuDataSpin *data, const gchar *filename,
                            VisuDataLoader *format);

void visu_data_spin_class_addLoader(VisuDataLoader *loader);
GList* visu_data_spin_class_getLoaders(void);
const gchar* visu_data_spin_class_getFileDescription(void);

void visu_data_spin_setAt(VisuDataSpin *dataObj,
                          const VisuNode *node, const gfloat vals[3]);
void visu_data_spin_setAtSpherical(VisuDataSpin *dataObj,
                                   const VisuNode *node, const gfloat vals[3]);
gfloat visu_data_spin_getMaxModulus(const VisuDataSpin *dataObj, guint iElement);

const VisuNodeValuesVector* visu_data_spin_get(const VisuDataSpin *dataObj);

void visu_data_spin_class_finalize(void);

G_END_DECLS

#endif
