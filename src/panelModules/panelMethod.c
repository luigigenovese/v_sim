/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "panelMethod.h"

#include <support.h>
#include <visu_tools.h>
#include <visu_dataatomic.h>
#include <visu_dataspin.h>
#include <gtk_main.h>
#include <extraGtkFunctions/gtk_toolPanelWidget.h>

#include "gtkAtomic.h"
#include "gtkSpin.h"

/**
 * SECTION: panelMethod
 * @short_description: The tab where drawing method is chosen (atomic
 * or spin).
 *
 * <para>This tab has an area to display custom widgets according to
 * the selected rendering method, see visu_ui_panel_method_set().</para>
 */

/* Local variables. */
/* Store the toolpanel object, can be interesting to
   fetch the current visuData object for instance. */
static GtkWidget* panelMethod;
/* A flag to control if the GTK part of the panel
   has been initialated or not. */
static gboolean isPanelInitialised;
/* Widgets that can be accessed during the run. */
static GtkWidget *descrLabel;
static GtkWidget *labelMethod;
static GtkWidget *viewport;
static GType currentMethod;

/* Callbacks */
static void onMethodPanelEnter(VisuUiMain *ui);
static void onDataNotified(VisuGlNodeScene *scene, GParamSpec *pspec, gpointer data);

VisuUiPanel* visu_ui_panel_method_init(VisuUiMain *ui)
{
  char *cl = _("Rendering method");
  char *tl = _("Draw");

  /* Now that everything has a value, we create the panel. */
  panelMethod = visu_ui_panel_newWithIconFromPath("Panel_method", cl, tl,
                                                  "stock-method_20.png");
  if (!panelMethod)
    return (VisuUiPanel*)0;
  visu_ui_panel_setDockable(VISU_UI_PANEL(panelMethod), TRUE);

  /* Initialisation of loacl variables. */
  isPanelInitialised = FALSE;
  currentMethod = 0;

  /* Set global callbacks. */
  g_signal_connect_swapped(G_OBJECT(panelMethod), "page-entered",
		           G_CALLBACK(onMethodPanelEnter), (gpointer)ui);

  return VISU_UI_PANEL(panelMethod);
}

static void onMethodPanelEnter(VisuUiMain *ui)
{
  GtkWidget *label, *vbox, *scrollView, *hbox;
  VisuGlNodeScene *scene;
  
  /* On page enter, we just create the GTK objects if required. */
  if (isPanelInitialised)
    return;

  vbox = gtk_vbox_new(FALSE, 0);
  
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 2);
  
  label = gtk_label_new(_("<b>Input method:</b>"));
  gtk_label_set_xalign(GTK_LABEL(label), 0.);
  gtk_widget_set_margin_start(label, 5);
  gtk_widget_set_name(label, "label_head");
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  
  labelMethod = gtk_label_new("");
  gtk_box_pack_start(GTK_BOX(hbox), labelMethod, TRUE, TRUE, 0);
  
  descrLabel = gtk_label_new("");
  gtk_widget_set_name(descrLabel, "label_info");
  gtk_widget_set_margin_start(descrLabel, 15);
  gtk_box_pack_start(GTK_BOX(vbox), descrLabel, FALSE, FALSE, 2);
  gtk_label_set_use_markup(GTK_LABEL(descrLabel), TRUE);
  gtk_label_set_line_wrap(GTK_LABEL(descrLabel), TRUE);
  gtk_label_set_justify(GTK_LABEL(descrLabel), GTK_JUSTIFY_FILL);
  
  label = gtk_label_new(_("<b>Options:</b>"));
  gtk_label_set_xalign(GTK_LABEL(label), 0.);
  gtk_widget_set_margin_start(label, 5);
  gtk_widget_set_name(label, "label_head");
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);

  scrollView = gtk_scrolled_window_new((GtkAdjustment*)0,
				       (GtkAdjustment*)0);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW (scrollView),
                                 GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW (scrollView), GTK_SHADOW_NONE);
  gtk_box_pack_start(GTK_BOX(vbox), scrollView, TRUE, TRUE, 2);

  viewport = gtk_viewport_new (NULL, NULL);
  gtk_container_add(GTK_CONTAINER(scrollView), viewport);
  
  isPanelInitialised = TRUE;

  gtk_widget_show_all(vbox);

  scene = visu_ui_rendering_window_getGlScene(visu_ui_main_getRendering(ui));
  g_signal_connect(G_OBJECT(scene), "notify::data",
                   G_CALLBACK(onDataNotified), (gpointer)0);  
  onDataNotified(scene, (GParamSpec*)0, (gpointer)0);

  gtk_container_add(GTK_CONTAINER(panelMethod), vbox);
}

static void onDataNotified(VisuGlNodeScene *scene, GParamSpec *pspec _U_, gpointer data _U_)
{
  VisuData *dataObj;
  gchar *markup;
  GtkWidget *wd;
  GType type;

  dataObj = visu_gl_node_scene_getData(scene);
  type = dataObj ? G_OBJECT_TYPE(dataObj) : 0;
  if (type == currentMethod)
    return;

  currentMethod = type;

  /* Remove the current widget. */
  wd = gtk_bin_get_child(GTK_BIN(viewport));
  if (wd)
    gtk_widget_destroy(wd);
  
  if (VISU_IS_DATA_SPIN(dataObj))
    {
      gtk_label_set_text(GTK_LABEL(labelMethod), _("Spin visualisation"));
      markup = g_markup_printf_escaped
        ("<span size=\"smaller\">%s</span>",
         _("It draws arrows at given positions to represent an atom "
           "and its spin."));
      gtk_label_set_markup(GTK_LABEL(descrLabel), markup);
      g_free(markup);
      gtk_container_add(GTK_CONTAINER(viewport), visu_ui_panel_method_spin_create(scene));
    }
  else if (VISU_IS_DATA_ATOMIC(dataObj))
    {
      gtk_label_set_text(GTK_LABEL(labelMethod), _("Atom visualisation"));
      markup = g_markup_printf_escaped
        ("<span size=\"smaller\">%s</span>",
         _("It draws spheres at specified positions to represent atoms."
           " The radius of the sphere can vary."));
      gtk_label_set_markup(GTK_LABEL(descrLabel), markup);
      g_free(markup);
      gtk_container_add(GTK_CONTAINER(viewport), visu_ui_panel_method_atomic_create(scene));
    }
  else
    {
      gtk_label_set_text(GTK_LABEL(labelMethod), "");
      gtk_label_set_markup(GTK_LABEL(descrLabel), "");
    }
  gtk_widget_show_all(viewport);
}
