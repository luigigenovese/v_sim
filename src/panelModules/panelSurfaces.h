/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresses mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef PANELSURFACES_H
#define PANELSURFACES_H

#include <gtk/gtk.h>
#include <visu_data.h>
#include <coreTools/toolOptions.h>
#include <extraFunctions/planeset.h>
#include <extraFunctions/surfaces.h>
#include <extraFunctions/sfielddata.h>
#include <extraGtkFunctions/gtk_toolPanelWidget.h>

VisuUiPanel* visu_ui_panel_surfaces_init();

gboolean visu_ui_panel_surfaces_loadFile(const char* file_name,
                                         GtkTreeIter *iter,
                                         GHashTable *table,
                                         VisuScalarFieldMethod *meth);
void visu_ui_panel_surfaces_addSurfaces(GList *surfs, const gchar *name,
                                        GtkTreeIter *iter);
void visu_ui_panel_surfaces_addSurface(VisuSurface *surf,
                                       GtkTreeIter *root, GtkTreeIter *iter);

gboolean visu_ui_panel_surfaces_parseXMLFile(const gchar *filename, GError **error);
gboolean visu_ui_panel_surfaces_exportXMLFile(const gchar *filename, GError **error);
gboolean visu_ui_panel_surfaces_showAll(gboolean show);
void visu_ui_panel_surfaces_editProperties(GtkTreeIter *iter);

/**
 * VisuUiSurfacesFieldId:
 * @VISU_UI_SURFACES_FIELD_LABEL: a string, the description of the scalar field.
 * @VISU_UI_SURFACES_FIELD_ROW: a column storing internal pointers.
 * @VISU_UI_SURFACES_FIELD_N_COLUMNS: the number of columns.
 *
 * Thesse are the description of the columns stored in the #GtkListStore
 * of this panel. See visu_ui_panel_surfaces_getFields() to access this liststore.
 */
typedef enum
  {
    VISU_UI_SURFACES_FIELD_LABEL,
    VISU_UI_SURFACES_FIELD_ROW,
    VISU_UI_SURFACES_FIELD_N_COLUMNS
  } VisuUiSurfacesFieldId;

GtkListStore* visu_ui_panel_surfaces_getFields();
VisuScalarField* visu_ui_panel_surfaces_fieldsAt(GtkTreeModel *model, GtkTreeIter *iter);

/**
 * VISU_UI_SURFACE_NAME_STR:
 *
 * The default string used to name surfaces that are not associated to
 * any public surface ressource.
 */
#define VISU_UI_SURFACE_NAME_STR    "Choose an id name"
/**
 * VISU_UI_SURFACE_NAME_CHOOSE:
 *
 * The string used in the tree view to represent the surfaces that
 * don't share surface resources.
 */
#define VISU_UI_SURFACE_NAME_CHOOSE "<span size=\"smaller\"><i>"VISU_UI_SURFACE_NAME_STR"</i></span>"

GtkWidget* visu_ui_panel_surfaces_generateValues(int *nbValues, float **values,
					      gchar **name, float minVal,
					      float maxVal);

#endif
