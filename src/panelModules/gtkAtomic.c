/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "gtkAtomic.h"

#include <extraGtkFunctions/gtk_numericalEntryWidget.h>
#include <gtk_main.h>
#include <visu_gtk.h>
#include <support.h>
#include <extensions/forces.h>

/**
 * SECTION:gtkAtomic
 * @short_description: The gtk interface elements that are specific to
 * the atomic rendering method.
 *
 * <para>This part gathers all the routine specific to the widgets
 * related to the atomic rendering method. For the method itself,
 * there is no specific widgets. For the #VisuElement part, one can
 * tune the shape, the radius and the elipsoid orientation (when
 * selected). There is no specific #GtkFileChooser widget.</para>
 */

/* the spin button to control the radius. */
static GtkWidget *checkForces = NULL;
static GtkWidget *radioScaleAuto = NULL;
static GtkWidget *entryScale = NULL;
static VisuNodeValuesVector *model = NULL;
static gulong chg_sig;

/***************/
/* Public part */
/***************/

static void updateLabel(GtkWidget *label, VisuNode *node _U_, VisuNodeValuesVector *vect)
{
  float max;
  gchar *str;

  if (!vect)
    gtk_label_set_text(GTK_LABEL(label), _("(No force data)"));
  else
    {
      max = visu_node_values_farray_max(VISU_NODE_VALUES_FARRAY(vect));
      
      str = g_strdup_printf(_("(max. force is %.4g)"), max);
      gtk_label_set_text(GTK_LABEL(label), str);
      g_free(str);
    }
}
static void _setModel(GtkWidget *label, VisuNodeValuesVector *vect)
{
  if (model)
    {
      g_signal_handler_disconnect(model, chg_sig);
      g_object_unref(model);
    }
  model = vect;
  if (vect)
    {
      g_object_ref(vect);
      chg_sig = g_signal_connect_swapped(vect, "changed",
                                         G_CALLBACK(updateLabel), label);
    }
  updateLabel(label, (VisuNode*)0, vect);
}
static void modelNotified(GtkWidget *label, GParamSpec *pspec _U_, VisuGlExtNodeVectors *forces)
{
  _setModel(label, visu_gl_ext_node_vectors_get(forces));
}
static gboolean toAutoRadio(GBinding *bind _U_, const GValue *source_value,
                            GValue *target_value, gpointer data _U_)
{
  g_value_set_boolean(target_value, (g_value_get_float(source_value) <= 0.f));
  return TRUE;
}
static gboolean fromAutoRadio(GBinding *bind _U_, const GValue *source_value,
                              GValue *target_value, gpointer data _U_)
{
  if (!g_value_get_boolean(source_value))
    return FALSE;

  g_value_set_float(target_value, 0.f);
  return TRUE;
}
static gboolean toManualRadio(GBinding *bind _U_, const GValue *source_value,
                              GValue *target_value, gpointer data _U_)
{
  g_value_set_boolean(target_value, (g_value_get_float(source_value) > 0.f));
  return TRUE;
}
static gboolean fromManualRadio(GBinding *bind _U_, const GValue *source_value,
                                GValue *target_value, gpointer data _U_)
{
  if (!g_value_get_boolean(source_value))
    return FALSE;

  g_value_set_float(target_value, visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryScale)));
  return TRUE;
}
static gboolean toEntry(GBinding *bind _U_, const GValue *source_value,
                        GValue *target_value, gpointer data _U_)
{
  if (g_value_get_float(source_value) <= 0.f)
    return FALSE;
  
  g_value_set_double(target_value, g_value_get_float(source_value));
  return TRUE;
}
static gboolean fromEntry(GBinding *bind, const GValue *source_value,
                          GValue *target_value, gpointer data _U_)
{
  if (visu_gl_ext_node_vectors_getNormalisation(VISU_GL_EXT_NODE_VECTORS(g_binding_get_source(bind))) <= 0.f)
    return FALSE;

  g_value_set_float(target_value, g_value_get_double(source_value));
  return TRUE;
}
static void onDestroy(GtkWidget *wd _U_, gpointer data _U_)
{
  if (model)
    {
      g_signal_handler_disconnect(model, chg_sig);
      g_object_unref(model);
    }
}
/* Creates the vbox displayed in the config panel */
/**
 * visu_ui_panel_method_atomic_create:
 * @scene: a #VisuGlNodeScene object.
 *
 * Creates a widget with the options for atomic rendering.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly create #GtkVBox object.
 **/
GtkWidget* visu_ui_panel_method_atomic_create(VisuGlNodeScene *scene)
{
  GtkWidget *vbox, *wd, *hbox, *radio, *vbox2, *label;
  GSList *lst;
  VisuGlExtForces *forces;

  DBG_fprintf(stderr, "Gtk Atomic: building specific widgets for method.\n");
  forces = visu_gl_node_scene_getForces(scene);

  vbox = gtk_vbox_new(FALSE, 0);
  
  checkForces = gtk_check_button_new_with_mnemonic
    (_("Display _forces (if available)"));
  g_object_bind_property(forces, "active", checkForces, "active",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
  g_object_bind_property(forces, "model", checkForces, "sensitive", G_BINDING_SYNC_CREATE);
  gtk_box_pack_start(GTK_BOX(vbox), checkForces, FALSE, FALSE, 0);
  vbox2 = gtk_vbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), vbox2, FALSE, FALSE, 0);
  wd = gtk_label_new(_("Policy to scale arrows:"));
  gtk_label_set_xalign(GTK_LABEL(wd), 0.);
  gtk_widget_set_margin_start(wd, 10);
  gtk_box_pack_start(GTK_BOX(vbox2), wd, FALSE, FALSE, 0);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox2), hbox, FALSE, FALSE, 0);
  radioScaleAuto = gtk_radio_button_new_with_label(NULL, _("automatic"));
  g_object_bind_property_full(forces, "normalisation", radioScaleAuto, "active",
                              G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                              toAutoRadio, fromAutoRadio, (gpointer)0, (GDestroyNotify)0);
  gtk_radio_button_set_group(GTK_RADIO_BUTTON(radioScaleAuto), (GSList*)0);
  lst = gtk_radio_button_get_group(GTK_RADIO_BUTTON(radioScaleAuto));
  gtk_box_pack_start(GTK_BOX(hbox), radioScaleAuto, TRUE, TRUE, 15);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox2), hbox, FALSE, FALSE, 0);
  radio = gtk_radio_button_new_with_label(NULL, _("manual"));
  gtk_radio_button_set_group(GTK_RADIO_BUTTON(radio), lst);
  g_object_bind_property_full(forces, "normalisation", radio, "active",
                              G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                              toManualRadio, fromManualRadio, (gpointer)0, (GDestroyNotify)0);

  gtk_box_pack_start(GTK_BOX(hbox), radio, FALSE, FALSE, 15);
  entryScale = visu_ui_numerical_entry_new(1e-2);
  gtk_entry_set_width_chars(GTK_ENTRY(entryScale), 6);
  g_object_bind_property_full(forces, "normalisation", entryScale, "value",
                              G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                              toEntry, fromEntry, (gpointer)0, (GDestroyNotify)0);

  gtk_box_pack_start(GTK_BOX(hbox), entryScale, FALSE, FALSE, 0);
  label = gtk_label_new("");
  gtk_label_set_xalign(GTK_LABEL(label), 0.);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 5);

  g_signal_connect_object(forces, "notify::model", G_CALLBACK(modelNotified),
                          label, G_CONNECT_SWAPPED);
  _setModel(label, visu_gl_ext_node_vectors_get(VISU_GL_EXT_NODE_VECTORS(forces)));
  
  gtk_widget_show_all(vbox);

  g_signal_connect(vbox, "destroy", G_CALLBACK(onDestroy), (gpointer)0);

  return vbox;
}
