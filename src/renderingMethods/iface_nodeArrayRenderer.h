/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef IFACE_NODE_ARRAY_RENDERER_H
#define IFACE_NODE_ARRAY_RENDERER_H

#include <glib.h>
#include <glib-object.h>

#include <visu_nodes.h>
#include "elementRenderer.h"
#include <extraFunctions/colorizer.h>

G_BEGIN_DECLS

/* NodeArrayRenderer interface. */
#define VISU_TYPE_NODE_ARRAY_RENDERER                (visu_node_array_renderer_get_type())
#define VISU_NODE_ARRAY_RENDERER(obj)                (G_TYPE_CHECK_INSTANCE_CAST((obj), VISU_TYPE_NODE_ARRAY_RENDERER, VisuNodeArrayRenderer))
#define VISU_IS_NODE_ARRAY_RENDERER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE((obj), VISU_TYPE_NODE_ARRAY_RENDERER))
#define VISU_NODE_ARRAY_RENDERER_GET_INTERFACE(inst) (G_TYPE_INSTANCE_GET_INTERFACE((inst), VISU_TYPE_NODE_ARRAY_RENDERER, VisuNodeArrayRendererInterface))

typedef struct _VisuNodeArrayRenderer VisuNodeArrayRenderer; /* dummy object */
typedef struct _VisuNodeArrayRendererInterface VisuNodeArrayRendererInterface;

/**
 * VisuNodeArrayRenderer:
 *
 * Interface object.
 *
 * Since: 3.8
 */

/**
 * VisuNodeArrayRendererInterface:
 * @parent: its parent.
 * @getNodeArray: a method to get the #VisuNodeArray this renderer is
 * working on.
 * @setNodeArray: a method to change the #VisuNodeArray this renderer
 * is working on.
 * @getElement: a method to get the #VisuElementRenderer of the given
 * #VisuElement.
 * @getExtent: a method to get the maximum element size.
 * @pushColorizer: a method used to change the current #VisuNode
 * colorizer.
 * @removeColorizer: a method to remove a given colorizer.
 * @getColorizer: a method used to get the current #VisuNode colorizer.
 *
 * Interface for class that can represent #VisuNodeArray.
 *
 * Since: 3.8
 */
struct _VisuNodeArrayRendererInterface
{
  GTypeInterface parent;

  VisuNodeArray*       (*getNodeArray)(VisuNodeArrayRenderer *self);
  gboolean             (*setNodeArray)(VisuNodeArrayRenderer *self, VisuNodeArray *array);
  VisuElementRenderer* (*getElement)(VisuNodeArrayRenderer *self, const VisuElement *element);
  gfloat               (*getExtent)(VisuNodeArrayRenderer *self, guint *n);
  gboolean             (*pushColorizer)(VisuNodeArrayRenderer *self, VisuDataColorizer *colorizer);
  gboolean             (*removeColorizer)(VisuNodeArrayRenderer *self, VisuDataColorizer *colorizer);
  VisuDataColorizer *  (*getColorizer)(VisuNodeArrayRenderer *self);
};

GType visu_node_array_renderer_get_type (void);

VisuNodeArray* visu_node_array_renderer_getNodeArray(VisuNodeArrayRenderer *self);
gboolean visu_node_array_renderer_setNodeArray(VisuNodeArrayRenderer *self, VisuNodeArray *array);

VisuElementRenderer* visu_node_array_renderer_get(VisuNodeArrayRenderer *node_array,
                                                         const VisuElement *element);
gfloat visu_node_array_renderer_getMaxElementSize(VisuNodeArrayRenderer *node_array, guint *n);
gboolean visu_node_array_renderer_pushColorizer(VisuNodeArrayRenderer *self,
                                                VisuDataColorizer *colorizer);
gboolean visu_node_array_renderer_removeColorizer(VisuNodeArrayRenderer *self,
                                                  VisuDataColorizer *colorizer);
VisuDataColorizer* visu_node_array_renderer_getColorizer(VisuNodeArrayRenderer *self);

typedef struct _VisuNodeArrayRendererIter VisuNodeArrayRendererIter;
struct _VisuNodeArrayRendererIter
{
  VisuNodeArrayRenderer *self;
  VisuNodeArrayIter parent;

  VisuElement *element;
  VisuElementRenderer *renderer;
  guint nStoredNodes;

  /*< private >*/
  gboolean physical;
};

gboolean visu_node_array_renderer_iter_new(VisuNodeArrayRenderer *self,
                                           VisuNodeArrayRendererIter *iter,
                                           gboolean physical);
gboolean visu_node_array_renderer_iter_next(VisuNodeArrayRendererIter *iter);

G_END_DECLS

#endif
