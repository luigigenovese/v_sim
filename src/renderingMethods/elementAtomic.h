/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef ELEMENT_ATOMIC_H
#define ELEMENT_ATOMIC_H

#include <glib-object.h>

#include "elementRenderer.h"
#include <coreTools/toolPhysic.h>

G_BEGIN_DECLS

/**
 * VisuElementAtomicShapeId:
 * @VISU_ELEMENT_ATOMIC_SPHERE: draw sphere ;
 * @VISU_ELEMENT_ATOMIC_CUBE: draw cube ;
 * @VISU_ELEMENT_ATOMIC_ELLIPSOID: draw elipsoid ;
 * @VISU_ELEMENT_ATOMIC_POINT: draw square dot ;
 * @VISU_ELEMENT_ATOMIC_TORUS: draw torus ;
 * @VISU_ELEMENT_ATOMIC_N_SHAPES: number of shapes.
 *
 * This enum is used as identifier for shapes managed by the
 * attomic element method.
 */
typedef enum 
{
  VISU_ELEMENT_ATOMIC_SPHERE,
  VISU_ELEMENT_ATOMIC_CUBE,
  VISU_ELEMENT_ATOMIC_ELLIPSOID,
  VISU_ELEMENT_ATOMIC_POINT,
  VISU_ELEMENT_ATOMIC_TORUS,
  VISU_ELEMENT_ATOMIC_N_SHAPES
} VisuElementAtomicShapeId;

/**
 * VISU_TYPE_ELEMENT_ATOMIC:
 *
 * return the type of #VisuElementAtomic.
 */
#define VISU_TYPE_ELEMENT_ATOMIC	     (visu_element_atomic_get_type ())
/**
 * VISU_ELEMENT_ATOMIC:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuElementAtomic type.
 */
#define VISU_ELEMENT_ATOMIC(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_ELEMENT_ATOMIC, VisuElementAtomic))
/**
 * VISU_ELEMENT_ATOMIC_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuElementAtomicClass.
 */
#define VISU_ELEMENT_ATOMIC_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_ELEMENT_ATOMIC, VisuElementAtomicClass))
/**
 * VISU_IS_ELEMENT_ATOMIC:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuElementAtomic object.
 */
#define VISU_IS_ELEMENT_ATOMIC(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_ELEMENT_ATOMIC))
/**
 * VISU_IS_ELEMENT_ATOMIC_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuElementAtomicClass class.
 */
#define VISU_IS_ELEMENT_ATOMIC_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_ELEMENT_ATOMIC))
/**
 * VISU_ELEMENT_ATOMIC_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_ELEMENT_ATOMIC_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_ELEMENT_ATOMIC, VisuElementAtomicClass))

typedef struct _VisuElementAtomicClass VisuElementAtomicClass;
typedef struct _VisuElementAtomic VisuElementAtomic;
typedef struct _VisuElementAtomicPrivate VisuElementAtomicPrivate;

/**
 * visu_element_atomic_get_type:
 *
 * This method returns the type of #VisuElementAtomic, use VISU_TYPE_ELEMENT_ATOMIC instead.
 *
 * Returns: the type of #VisuElementAtomic.
 */
GType visu_element_atomic_get_type(void);

/**
 * _VisuElementAtomic:
 * @parent: parent.
 * @priv: private data.
 *
 * This structure describes a set of pairs.
 */

struct _VisuElementAtomic
{
  VisuElementRenderer parent;

  VisuElementAtomicPrivate *priv;
};

struct _VisuElementAtomicClass
{
  VisuElementRendererClass parent;
};

VisuElementAtomic* visu_element_atomic_new(VisuElement *element);
VisuElementAtomic* visu_element_atomic_getFromPool(VisuElement *element);
void visu_element_atomic_bindToPool(VisuElementAtomic *atomic);

gfloat     visu_element_atomic_getRadius(const VisuElementAtomic *self);
gboolean   visu_element_atomic_setRadius(VisuElementAtomic *self, gfloat val);
ToolUnits  visu_element_atomic_getUnits (const VisuElementAtomic *self);
gboolean   visu_element_atomic_setUnits (VisuElementAtomic *self, ToolUnits val);
VisuElementAtomicShapeId visu_element_atomic_getShape (const VisuElementAtomic *self);
gboolean   visu_element_atomic_setShape (VisuElementAtomic *self,
                                         VisuElementAtomicShapeId val);
gfloat     visu_element_atomic_getElipsoidRatio(const VisuElementAtomic *self);
gboolean   visu_element_atomic_setElipsoidRatio(VisuElementAtomic *self, gfloat val);
gfloat     visu_element_atomic_getElipsoidPhi  (const VisuElementAtomic *self);
gboolean   visu_element_atomic_setElipsoidPhi  (VisuElementAtomic *self, gfloat val);
gfloat     visu_element_atomic_getElipsoidTheta(const VisuElementAtomic *self);
gboolean   visu_element_atomic_setElipsoidTheta(VisuElementAtomic *self, gfloat val);

const gchar ** visu_element_atomic_getShapeNames(gboolean asLabel);

void visu_element_atomic_pool_finalize(void);

G_END_DECLS

#endif
