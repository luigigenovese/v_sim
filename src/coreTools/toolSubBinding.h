/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef SUBBINDING_H
#define SUBBINDING_H

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

/**
 * TOOL_TYPE_SUB_BINDING:
 *
 * return the type of #ToolDataColorizerFragment.
 */
#define TOOL_TYPE_SUB_BINDING	     (tool_sub_binding_get_type ())
/**
 * TOOL_SUB_BINDING:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #ToolSubBinding type.
 */
#define TOOL_SUB_BINDING(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, TOOL_TYPE_SUB_BINDING, ToolSubBinding))
/**
 * TOOL_SUB_BINDING_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #ToolSubBindingClass.
 */
#define TOOL_SUB_BINDING_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, TOOL_TYPE_SUB_BINDING, ToolSubBindingClass))
/**
 * TOOL_IS_SUB_BINDING_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #ToolSubBinding object.
 */
#define TOOL_IS_SUB_BINDING_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, TOOL_TYPE_SUB_BINDING))
/**
 * TOOL_IS_SUB_BINDING_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #ToolSubBindingClass class.
 */
#define TOOL_IS_SUB_BINDING_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, TOOL_TYPE_SUB_BINDING))
/**
 * TOOL_SUB_BINDING_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define TOOL_SUB_BINDING_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, TOOL_TYPE_SUB_BINDING, ToolSubBindingClass))

/**
 * ToolSubBindingPrivate:
 * 
 * Private data for #ToolSubBinding objects.
 */
typedef struct _ToolSubBindingPrivate ToolSubBindingPrivate;

/**
 * ToolSubBinding:
 * 
 * Common name to refer to a #_ToolSubBinding.
 */
typedef struct _ToolSubBinding ToolSubBinding;
struct _ToolSubBinding
{
  GObject parent;

  ToolSubBindingPrivate *priv;
};

typedef struct _ToolSubBindingClass ToolSubBindingClass;
struct _ToolSubBindingClass
{
  GObjectClass parent;
};

/**
 * tool_sub_binding_get_type:
 *
 * This method returns the type of #ToolSubBinding, use
 * TOOL_TYPE_SUB_BINDING instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #ToolSubBinding.
 */
GType tool_sub_binding_get_type(void);

ToolSubBinding* tool_sub_binding_new(gpointer source, const gchar *property, const gchar *subProperty,
                                     gpointer target, const gchar *destination,
                                     GBindingFlags flags);

ToolSubBinding* tool_sub_binding_new_full(gpointer source, const gchar *property, const gchar *subProperty,
                                          gpointer target, const gchar *destination,
                                          GBindingFlags flags,
                                          GBindingTransformFunc transform_to,
                                          GBindingTransformFunc transform_from,
                                          gpointer user_data,
                                          GDestroyNotify notify);

G_END_DECLS

#endif
