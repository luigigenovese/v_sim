/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef TOOLCOLOR_H
#define TOOLCOLOR_H

#include <glib.h>
#include <glib-object.h>

#include "toolPool.h"

G_BEGIN_DECLS

/**
 * TOOL_COLOR_MASK_R:
 *
 * This value can be used to create a mask for methods that
 * require one for reading rgb color array. This value actually
 * correspond to red.
 */
#define TOOL_COLOR_MASK_R (1 << 0)
/**
 * TOOL_COLOR_MASK_G:
 *
 * This value can be used to create a mask for methods that
 * require one for reading rgb color array. This value actually
 * correspond to green.
 */
#define TOOL_COLOR_MASK_G (1 << 1)
/**
 * TOOL_COLOR_MASK_B:
 *
 * This value can be used to create a mask for methods that
 * require one for reading rgb color array. This value actually
 * correspond to blue.
 */
#define TOOL_COLOR_MASK_B (1 << 2)
/**
 * TOOL_COLOR_MASK_A:
 *
 * This value can be used to create a mask for methods that
 * require one for reading rgb color array. This value actually
 * correspond to the alpha channel.
 */
#define TOOL_COLOR_MASK_A (1 << 3)
/**
 * TOOL_COLOR_MASK_RGBA:
 *
 * This value can be used to create a mask for methods that
 * require one for reading rgb color array. This value is a
 * shortcut for #TOOL_COLOR_MASK_R | #TOOL_COLOR_MASK_G | #TOOL_COLOR_MASK_B.
 */
#define TOOL_COLOR_MASK_RGBA (15)

/******************/
/* Storing colors */
/******************/
/**
 * ToolColor:
 * @rgba: the coding of color in Red, Green, Blue, Alpha format,
 *        floating point numbers between 0 and 1 ;
 * @repr: store the representation of the colour as "#rrggbbaa".
 * @userData: unused.
 *
 * A structure to store colors. @repr is not set before
 * tool_color_asStr() is called. Any changes in @rgba are not
 * transfered to @repr and tool_color_asStr() should be called again.
 */
typedef struct _ToolColor ToolColor;
struct _ToolColor
{
  float rgba[4];
  gchar repr[10];
  gpointer userData;
};

#define    TOOL_TYPE_COLOR (tool_color_get_type())
GType      tool_color_get_type(void);
ToolColor* tool_color_new(const float rgba[4]);
const ToolColor* tool_color_new_bright(guint id);
gboolean   tool_color_equal(const ToolColor *color1, const ToolColor *color2);
void       tool_color_copy(ToolColor *color, const ToolColor *color_old);

ToolPool*  tool_color_getStorage();
ToolColor* tool_color_getByValues(int *pos, float red, float green, float blue, float alpha);
ToolColor* tool_color_addFloatRGBA(const float rgba[4], int *position);
ToolColor* tool_color_addIntRGBA(int rgba[4]);

const ToolColor* tool_color_fromStr(const gchar *str, int *pos);
const gchar* tool_color_asStr(ToolColor *color);

const ToolColor* tool_color_fromName(const gchar *name, int *pos);

void tool_color_invertRGBA(float inv[4], const float rgba[4]);
void tool_color_convertHSVtoRGB(float* rgb, const float* hsv);
void tool_color_convertHSLtoRGB(float *rgb, const float *hsl);
void tool_color_convertRGBtoHSL(float *hsl, const float *rgb);

/**
 * ToolMaterialIds:
 * @TOOL_MATERIAL_AMB: the ambient identifier ;
 * @TOOL_MATERIAL_DIF: the diffuse identifier ;
 * @TOOL_MATERIAL_SHI: the shiningness identifier ;
 * @TOOL_MATERIAL_SPE: the specular identifier ;
 * @TOOL_MATERIAL_EMI: the emissivity identifier ;
 * @TOOL_MATERIAL_N_VALUES: number of used material identifiers.
 *
 * This enum is used to address the OpenGL parameters for light rendering.
 */
typedef enum
  {
    TOOL_MATERIAL_AMB,
    TOOL_MATERIAL_DIF,
    TOOL_MATERIAL_SHI,
    TOOL_MATERIAL_SPE,
    TOOL_MATERIAL_EMI,
    TOOL_MATERIAL_N_VALUES
  } ToolMaterialIds;

/**
 * ToolMaterial:
 *
 * A type used to exchange five floats defining material values between
 * #GObject properties.
 */
#define    TOOL_TYPE_MATERIAL (tool_material_get_type())
GType      tool_material_get_type(void);

G_END_DECLS

#endif
