/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "toolSubBinding.h"
#include "visu_tools.h"

/**
 * SECTION:toolSubBinding
 * @short_description: .
 *
 * <para></para>
 */

/**
 * ToolSubBindingClass:
 * @parent: its parent.
 *
 * Interface for class that can represent #ToolSubBinding.
 *
 * Since: 3.8
 */
struct _ToolSubBindingPrivate
{
  gboolean dispose_has_run;

  GObject *source;
  gulong notify;

  GObject *interm;
  GBinding *bind;

  GObject *target;
  gchar *sub, *dest;
  GBindingFlags flags;
  GBindingTransformFunc transform_to;
  GBindingTransformFunc transform_from;
  gpointer user_data;
  GDestroyNotify destroy;
};

static void tool_sub_binding_dispose(GObject* obj);
static void tool_sub_binding_finalize(GObject* obj);

G_DEFINE_TYPE_WITH_CODE(ToolSubBinding, tool_sub_binding, G_TYPE_OBJECT,
                        G_ADD_PRIVATE(ToolSubBinding))

static void tool_sub_binding_class_init(ToolSubBindingClass *klass)
{
  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose = tool_sub_binding_dispose;
  G_OBJECT_CLASS(klass)->finalize = tool_sub_binding_finalize;
}

static void tool_sub_binding_init(ToolSubBinding *obj)
{
  obj->priv = tool_sub_binding_get_instance_private(obj);
  obj->priv->dispose_has_run = FALSE;
  obj->priv->source = (GObject*)0;
  obj->priv->interm = (GObject*)0;
  obj->priv->bind = (GBinding*)0;

  obj->priv->target = (GObject*)0;
  obj->priv->sub = (gchar*)0;
  obj->priv->dest = (gchar*)0;
  obj->priv->transform_to = (GBindingTransformFunc)0;
  obj->priv->transform_from = (GBindingTransformFunc)0;
  obj->priv->user_data = (gpointer)0;
  obj->priv->destroy = (GDestroyNotify)0;
}
static void tool_sub_binding_dispose(GObject* obj)
{
  ToolSubBinding *bind;

  g_return_if_fail(obj);

  bind = TOOL_SUB_BINDING(obj);

  if (bind->priv->dispose_has_run)
    return;

  if (bind->priv->source)
    {
      g_signal_handler_disconnect(bind->priv->source, bind->priv->notify);
      g_object_unref(bind->priv->source);
    }
  if (bind->priv->target)
    g_object_unref(bind->priv->target);
  if (bind->priv->bind)
    g_object_unref(bind->priv->bind);
  if (bind->priv->interm)
    g_object_unref(bind->priv->interm);
  if (bind->priv->user_data && bind->priv->destroy)
    bind->priv->destroy(bind->priv->user_data);

  G_OBJECT_CLASS(tool_sub_binding_parent_class)->finalize(obj);
}
static void tool_sub_binding_finalize(GObject* obj)
{
  ToolSubBinding *bind;

  g_return_if_fail(obj);

  bind = TOOL_SUB_BINDING(obj);
  g_free(bind->priv->sub);
  g_free(bind->priv->dest);

  G_OBJECT_CLASS(tool_sub_binding_parent_class)->finalize(obj);
}
static void setDefault(ToolSubBinding *bind, GParamSpec *pspec)
{
  GParamSpec *ptarget;
  GValue from = G_VALUE_INIT;
  GValue to = G_VALUE_INIT;
  gboolean valid;

  DBG_fprintf(stderr, "'%s' %ld\n", pspec->name, pspec->value_type);
  DBG_fprintf(stderr, "%p\n", g_type_class_peek(pspec->value_type));
  ptarget = g_object_class_find_property(G_OBJECT_CLASS(g_type_class_peek(pspec->value_type)),
                                         bind->priv->sub);
  g_value_init(&from, ptarget->value_type);
  g_param_value_set_default(ptarget, &from);
  ptarget = g_object_class_find_property(G_OBJECT_GET_CLASS(bind->priv->target),
                                         bind->priv->dest);
  g_value_init(&to, ptarget->value_type);
  if (bind->priv->transform_to)
    valid = bind->priv->transform_to((GBinding*)0, &from, &to, bind->priv->user_data);
  else
    valid = g_value_transform(&from, &to);
  if (valid)
    g_object_set_property(bind->priv->target, bind->priv->dest, &to);
}
static void setProperty(ToolSubBinding *bind, GParamSpec *pspec, GObject *source)
{
  GObject *sub;

  g_return_if_fail(G_IS_PARAM_SPEC_OBJECT(pspec));

  g_object_get(source, g_param_spec_get_name(pspec), &sub, NULL);
  if (sub == bind->priv->interm)
    {
      if (sub)
        g_object_unref(sub);
      return;
    }

  g_clear_object(&bind->priv->bind);
  if (bind->priv->interm)
    g_object_unref(bind->priv->interm);
  bind->priv->interm = sub;

  if (sub)
    bind->priv->bind = g_object_bind_property_full(sub, bind->priv->sub,
                                                   bind->priv->target, bind->priv->dest,
                                                   bind->priv->flags,
                                                   bind->priv->transform_to,
                                                   bind->priv->transform_from,
                                                   bind->priv->user_data,
                                                   (GDestroyNotify)0);
  else
    setDefault(bind, pspec);
}

/**
 * tool_sub_binding_new:
 * @source: (transfer full): a #GObject object.
 * @property: a property of @source.
 * @subProperty: a property of the @property of @source.
 * @target: (transfer full): a #GObject object.
 * @destination: a property of @target.
 * @flags: flags, see #GBindingFlags.
 *
 * Creates a binding between the property @subProperty of an object
 * retrieve as the @property of @source. If @property is null,
 * @destination of @target is set to the default value of @property in
 * @property class. Contrary to g_object_bind_property(), the returned
 * object owns a reference on @source or @target.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly allocated #ToolSubBinding object.
 **/
ToolSubBinding* tool_sub_binding_new(gpointer source, const gchar *property, const gchar *subProperty,
                                     gpointer target, const gchar *destination,
                                     GBindingFlags flags)
{
  return tool_sub_binding_new_full(source, property, subProperty, target, destination,
                                   flags, (GBindingTransformFunc)0,
                                   (GBindingTransformFunc)0,
                                   (gpointer)0, (GDestroyNotify)0);
}

/**
 * tool_sub_binding_new_full:
 * @source: (transfer full): a #GObject object.
 * @property: a property of @source.
 * @subProperty: a property of the @property of @source.
 * @target: (transfer full): a #GObject object.
 * @destination: a property of @target.
 * @flags: flags, see #GBindingFlags.
 * @transform_to: (allow-none) (scope notified): a GBindingTransformFunc func or %NULL.
 * @transform_from: (allow-none) (scope notified): a GBindingTransformFunc func or %NULL.
 * @user_data: some user data or %NULL.
 * @notify: a #GDestroyNotify function.
 *
 * As tool_sub_binding_new() but with transformation functions.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly allocated #ToolSubBinding object.
 **/
ToolSubBinding* tool_sub_binding_new_full(gpointer source, const gchar *property, const gchar *subProperty,
                                          gpointer target, const gchar *destination,
                                          GBindingFlags flags,
                                          GBindingTransformFunc transform_to,
                                          GBindingTransformFunc transform_from,
                                          gpointer user_data,
                                          GDestroyNotify notify)
{
  ToolSubBinding *bind;
  gchar *sig;
  GParamSpec *pspec;

  bind = TOOL_SUB_BINDING(g_object_new(TOOL_TYPE_SUB_BINDING, NULL));

  g_object_ref(source);
  bind->priv->source = G_OBJECT(source);
  bind->priv->sub = g_strdup(subProperty);
  bind->priv->dest = g_strdup(destination);
  g_object_ref(target);
  bind->priv->target = G_OBJECT(target);
  bind->priv->flags = flags;
  bind->priv->transform_to = transform_to;
  bind->priv->transform_from = transform_from;
  bind->priv->user_data = user_data;
  bind->priv->destroy = notify;

  sig = g_strdup_printf("notify::%s", property);
  bind->priv->notify = g_signal_connect_swapped
    (source, sig, G_CALLBACK(setProperty), bind);
  g_free(sig);
  pspec = g_object_class_find_property(G_OBJECT_GET_CLASS(source),
                                       property);
  setProperty(bind, pspec, G_OBJECT(source));
  if (!bind->priv->bind && (flags & G_BINDING_SYNC_CREATE))
    setDefault(bind, pspec);

  return bind;
}
