/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "toolOptions.h"

#include <visu_tools.h>

/**
 * SECTION:toolOptions
 * @short_description: A convienent wrapper around GHashTable that can support types.
 *
 * <para>This wrapper is a simple way to store integers, floating
 * point values or booleans in a table, accessing with a string key
 * and remembering their types.</para>
 */

struct _ToolOption
{
  /* The name of the option, copied on creation and
     used as key in a table of options. */
  gchar *name;

  /* A short description of the option. */
  gchar *label;

  /* Value. */
  GValue *value;
};


/**
 * tool_option_new:
 * @name: a string identifying the option ;
 * @label: a string describing shortly the option (must be in UTF-8).
 * @g_type: the type of option to create.
 *
 * Create a new #Option using the name as identifier.
 *
 * Returns: (transfer none): a newly created option, use tool_option_free() to free it.
 */
ToolOption* tool_option_new(const gchar *name, const gchar *label, GType g_type)
{
  ToolOption *option;

  g_return_val_if_fail(name && name[0] && label, (ToolOption*)0);
  g_return_val_if_fail(G_TYPE_IS_VALUE(g_type), (ToolOption*)0);

  option = g_malloc(sizeof(ToolOption));
  option->name = g_strdup(name);
  option->label = g_strdup(label);
  option->value = g_slice_new0(GValue);
  g_value_init(option->value, g_type);
  return option;
}

/**
 * tool_option_free:
 * @option: the #Option to free.
 *
 * Free the memory used by the @data.
 */
void tool_option_free(ToolOption *option)
{
  g_return_if_fail(option);

  g_free(option->name);
  g_free(option->label);
  g_value_unset(option->value);
  g_slice_free(GValue, option->value);
  g_free(option);
}

/**
 * tool_option_copy:
 * @from: an existing #ToolOption.
 *
 * Create a new #ToolOption using the values from option @from.
 *
 * Returns: (transfer none): a newly created option, use tool_option_free() to free it.
 */
ToolOption* tool_option_copy(const ToolOption *from)
{
  ToolOption *to;

  g_return_val_if_fail(from, (ToolOption*)0);
  to = tool_option_new(from->name, from->label, G_VALUE_TYPE(from->value));
  g_value_copy(from->value, to->value);
  
  return to;
}

/**
 * tool_option_getName:
 * @option: the #Option to get the name of.
 *
 * Get the name of the option.
 *
 * Returns: a string owned by V_Sim, should not be freed.
 */
const gchar* tool_option_getName(ToolOption *option)
{
  g_return_val_if_fail(option, (gchar*)0);

  return option->name;
}
/**
 * tool_option_getLabel:
 * @option: the #Option to get the label of.
 *
 * Get the label of the option.
 *
 * Returns: a string owned by V_Sim, should not be freed.
 */
const gchar* tool_option_getLabel(ToolOption *option)
{
  g_return_val_if_fail(option, (gchar*)0);

  return option->label;
}
/**
 * tool_option_getType:
 * @option: the #Option to get the type of.
 *
 * Get the type of the option.
 *
 * Returns: a #OptionTypes value.
 */
GType tool_option_getType(ToolOption *option)
{
  g_return_val_if_fail(option, 0);

  return G_VALUE_TYPE(option->value);
}

/**
 * tool_option_getValue:
 * @option: a #Option object.
 *
 * Get the location of the storage for the option.
 *
 * Returns: the #GValue storing the option value.
 */
GValue* tool_option_getValue(ToolOption *option)
{
  g_return_val_if_fail(option, (GValue*)0);

  return option->value;
}

/**
 * tool_option_getValueAndLabel:
 * @option: the #Option to get the value from.
 *
 * This method returns a string with the value followed by the label in parenthesis
 * and with Pango markup for smaller font.
 *
 * Returns: a newly created markup string.
 */
gchar* tool_option_getValueAndLabel(ToolOption *option)
{
  gchar *markup, *tmp;
  
  g_return_val_if_fail(option, (gchar*)0);

  if (G_VALUE_TYPE(option->value) != G_TYPE_NONE)
    {
      tmp = g_strdup_value_contents(option->value);
      markup = g_markup_printf_escaped("%s (<span size=\"smaller\">%s</span>)",
				       tmp, option->label);
      g_free(tmp);
    }
  else
    markup = (gchar*)0;
  return markup;
}
