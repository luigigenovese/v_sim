/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_PAIRS
#define VISU_PAIRS

#include <glib-object.h>

#include "visu_data.h"
#include "pairsModeling/link.h"

G_BEGIN_DECLS

/**
 * VISU_TYPE_PAIR:
 *
 * return the type of #VisuPair.
 */
#define VISU_TYPE_PAIR	     (visu_pair_get_type ())
/**
 * VISU_PAIR:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuPair type.
 */
#define VISU_PAIR(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_PAIR, VisuPair))
/**
 * VISU_PAIR_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuPairClass.
 */
#define VISU_PAIR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_PAIR, VisuPairClass))
/**
 * VISU_IS_PAIR:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuPair object.
 */
#define VISU_IS_PAIR(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_PAIR))
/**
 * VISU_IS_PAIR_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuPairClass class.
 */
#define VISU_IS_PAIR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_PAIR))
/**
 * VISU_PAIR_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_PAIR_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_PAIR, VisuPairClass))

typedef struct _VisuPairClass VisuPairClass;
typedef struct _VisuPair VisuPair;
typedef struct _VisuPairPrivate VisuPairPrivate;

/**
 * visu_pair_get_type:
 *
 * This method returns the type of #VisuPair, use VISU_TYPE_PAIR instead.
 *
 * Returns: the type of #VisuPair.
 */
GType visu_pair_get_type(void);

/**
 * _VisuPair:
 * @parent: parent.
 * @priv: private data.
 *
 * This structure describes a pair.
 */

struct _VisuPair
{
  VisuObject parent;

  VisuPairPrivate *priv;
};

struct _VisuPairClass
{
  VisuObjectClass parent;
};

VisuPair* visu_pair_new(VisuElement *ele1, VisuElement *ele2);

void visu_pair_getElements(const VisuPair *pair, VisuElement **ele1, VisuElement **ele2);

VisuPairLink* visu_pair_addLink(VisuPair *pair, const float minMax[2]);
gboolean visu_pair_removeLink(VisuPair *pair, VisuPairLink *data);
GList* visu_pair_getLinks(VisuPair *pair);
VisuPairLink* visu_pair_getNthLink(VisuPair *pair, guint pos);
gboolean visu_pair_contains(const VisuPair *pair, const VisuPairLink *link);

gboolean visu_pair_getBondDistance(VisuPair *pair, VisuData *dataObj, gfloat *from, gfloat *to);

/**
 * VisuPairForeachFunc:
 * @pair: a #VisuPair object.
 * @data: a #VisuPairLink object ;
 * @user_data: some user defined data.
 *
 * Prototype of functions called with the foreach method apply to each
 * links in a pair.
 */
typedef void (*VisuPairForeachFunc)(VisuPair *pair, VisuPairLink *data, gpointer user_data);
void visu_pair_foreach(VisuPair *pair, VisuPairForeachFunc whatToDo, gpointer user_data);

/**
 * VisuPairDistribution:
 * @ele1: one #VisuElement.
 * @ele2: one #VisuElement.
 * @histo: an array containing the distribution ;
 * @nValues: the size of the array ;
 * @initValue: the initial distance value (usualy 0) ;
 * @stepValue: the step increase in distance at each value ;
 * @nNodesEle1: the number of nodes used during the computation ;
 * @nNodesEle2: idem for #VisuElement 2.
 *
 * This structure stores for a given pair, the distance distribution
 * on a given range [@initValue;@nValues * @stepValue[.
 */
typedef struct _VisuPairDistribution VisuPairDistribution;
struct _VisuPairDistribution
{
  VisuElement *ele1, *ele2;
  guint *histo;
  guint nValues;
  float initValue, stepValue;
  guint nNodesEle1, nNodesEle2;
};

VisuPairDistribution* visu_pair_getDistanceDistribution(VisuPair *pair,
                                                        VisuData *dataObj,
                                                        float step,
                                                        float min, float max);
gboolean visu_pair_distribution_getNextPick(VisuPairDistribution *dd,
                                            guint startStopId[2], guint *integral,
                                            guint *max, guint *posMax);

G_END_DECLS

#endif
