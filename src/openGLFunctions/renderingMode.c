/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "renderingMode.h"

#include <visu_tools.h>

#include <GL/gl.h>
#include <GL/glu.h>

#include <string.h>

/**
 * SECTION:renderingMode
 * @short_description: Controls the way OpenGL renders objects.
 * 
 * <para>
 * This modules creates an interface to access to the way OpenGL
 * renders the objects (glPolygonMode() and glToolShadeModel()
 * functions). There are then three rendering modes available in
 * V_Sim: wireframe, flat and smooth. They are controls by an enum
 * #RenderingModeId. When visu_gl_rendering_applyMode() is called, the
 * current rendering mode is changed for all future drawing calls that
 * uses polygons.
 * </para>
 */

static const char *renderingStrings[VISU_GL_RENDERING_N_MODES + 1] =
  {"Wireframe", "Flat", "Smooth", "SmoothAndEdge", (const char*)0};
static const char *renderingStringsI18n[VISU_GL_RENDERING_N_MODES + 1];


/**
 * visu_gl_rendering_init: (skip)
 *
 * This method is used by opengl.c to initialise this module (declare config file
 * options...). It should not be called elsewhere.
 */
void visu_gl_rendering_init(void)
{
  DBG_fprintf(stderr, "OpenGl RenderingMode : initialization.\n");

  renderingStringsI18n[VISU_GL_RENDERING_WIREFRAME      ] = _("Wireframe");
  renderingStringsI18n[VISU_GL_RENDERING_FLAT           ] = _("Flat");
  renderingStringsI18n[VISU_GL_RENDERING_SMOOTH         ] = _("Smooth");
  renderingStringsI18n[VISU_GL_RENDERING_SMOOTH_AND_EDGE] = _("Smooth & edge");
  renderingStringsI18n[VISU_GL_RENDERING_N_MODES] = (const char*)0;
}

/**
 * visu_gl_rendering_applyMode:
 * @mode: an integer.
 *
 * Change the rendering mode of current OpenGL context.
 */
void visu_gl_rendering_applyMode(VisuGlRenderingMode mode)
{
  switch (mode)
    {
    case VISU_GL_RENDERING_WIREFRAME:
      glShadeModel(GL_FLAT);
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glLineWidth(1);
      break;
    case VISU_GL_RENDERING_FLAT:
      glShadeModel(GL_FLAT);
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      break;
    case VISU_GL_RENDERING_SMOOTH:
    case VISU_GL_RENDERING_SMOOTH_AND_EDGE:
      glShadeModel(GL_SMOOTH);
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      break;
    default:
      g_warning("Wrong value for parameter 'mode' in a call"
		" to 'visu_gl_rendering_applyMode'.");
      return;
    }
  DBG_fprintf(stderr, "Rendering Mode : switch rendering mode to '%s'.\n",
	      renderingStrings[mode]);
}
/**
 * visu_gl_rendering_getAllModeLabels:
 *
 * This function retrieve al the names (translated) of available rendering modes.
 *
 * Returns: (transfer none): an array of string, NULL terminated that
 * is private (not to be freed).
 */
const char** visu_gl_rendering_getAllModeLabels(void)
{
  return renderingStringsI18n;
}
/**
 * visu_gl_rendering_getAllModes:
 *
 * This function retrieve al the names of available rendering modes.
 *
 * Returns: (transfer none): an array of string, NULL terminated that
 * is private (not to be freed).
 */
const char** visu_gl_rendering_getAllModes(void)
{
  return renderingStrings;
}
/**
 * visu_gl_rendering_getModeFromName:
 * @name: a string ;
 * @id: a location to store the resulting id.
 *
 * This function retrieve the rendering mode id associated to the name.
 *
 * Returns: TRUE if the name exists.
 */
gboolean visu_gl_rendering_getModeFromName(const char* name, VisuGlRenderingMode *id)
{
  g_return_val_if_fail(name && id, FALSE);

  *id = 0;
  while (*id < VISU_GL_RENDERING_N_MODES && strcmp(name, renderingStrings[*id]))
    *id += 1;
  return (*id < VISU_GL_RENDERING_N_MODES);  
}
