/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_ACTIONINTERFACE_H
#define VISU_ACTIONINTERFACE_H

#include <glib.h>

/**
 * SECTION:visu_actionInterface
 * @short_description: Interface for defining actions and events.
 *
 * <para>These definitions are used to give a library and plateform
 * independent simplified event handlers.</para>
 */

/**
 * ToolButtonActionId:
 * @TOOL_BUTTON_TYPE_NONE: not a button event
 * @TOOL_BUTTON_TYPE_PRESS: a press button event
 * @TOOL_BUTTON_TYPE_RELEASE: a release button event
 *
 * Value that can be put into field buttonType of structure #ToolSimplifiedEvents.
 */
typedef enum {
  TOOL_BUTTON_TYPE_NONE,
  TOOL_BUTTON_TYPE_PRESS,
  TOOL_BUTTON_TYPE_RELEASE
} ToolButtonActionId;

/**
 * ToolSpecialKeyStroke:
 * @Key_None: no key pressed ;
 * @Key_Page_Up: key up ;
 * @Key_Page_Down: key down ;
 * @Key_Arrow_Left: key left ;
 * @Key_Arrow_Right: key right ;
 * @Key_Arrow_Up: key up ;
 * @Key_Arrow_Down: key down ;
 * @Key_Menu: key menu.
 *
 * Possible non ascii keys used in #ToolSimplifiedEvents.
 */
typedef enum
  {
    Key_None,
    Key_Page_Up,
    Key_Page_Down,
    Key_Arrow_Left,
    Key_Arrow_Right,
    Key_Arrow_Up,
    Key_Arrow_Down,
    Key_Menu
  } ToolSpecialKeyStroke;

/**
 * ToolSimplifiedEvents:
 * @x: the position x (on parent) for the event ;
 * @y: the position y (on parent) for the event ;
 * @root_x: the position x (in root window) for the event ;
 * @root_y: the position y (in root window) for the event ;
 * @button: the number of the button, 0 if not a button event ;
 * @buttonType: #TOOL_BUTTON_TYPE_PRESS or #TOOL_BUTTON_TYPE_RELEASE ;
 * @shiftMod: TRUE if Shift key is pressed during the event ;
 * @controlMod: TRUE if Control key is pressed during the event ;
 * @motion: TRUE if the event is a motion ;
 * @letter: The value of the letter if the event is a key stroke '\0' if not ;
 * @specialKey: the value of a special key if the event is a key stroke
 *              but not with an ascii letter.
 *
 * This structure is a common interface for events (inspired from X). We don't
 * use the one introduced by GDK because we don't want this dependency be a
 * limitation.
 */
typedef struct _ToolSimplifiedEvents ToolSimplifiedEvents;
struct _ToolSimplifiedEvents
{
  int x, y;
  int root_x, root_y;
  guint button;
  ToolButtonActionId buttonType;
  gboolean shiftMod, controlMod;
  gboolean motion;
  char letter;
  ToolSpecialKeyStroke specialKey;
};

#ifdef V_SIM_GDK
#include <gdk/gdk.h>

gboolean tool_simplified_events_new_fromGdk(ToolSimplifiedEvents *ev, const GdkEvent *event);
#endif

#endif
