/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_ELEMENTS_H
#define VISU_ELEMENTS_H

#include <glib.h>
#include <glib-object.h>

#include "visu_tools.h"

G_BEGIN_DECLS

/**
 * VISU_TYPE_ELEMENT:
 *
 * return the type of #VisuElement.
 */
#define VISU_TYPE_ELEMENT	     (visu_element_get_type ())
/**
 * VISU_ELEMENT:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuElement type.
 */
#define VISU_ELEMENT(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_ELEMENT, VisuElement))
/**
 * VISU_ELEMENT_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuElementClass.
 */
#define VISU_ELEMENT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_ELEMENT, VisuElementClass))
/**
 * VISU_IS_ELEMENT_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuElement object.
 */
#define VISU_IS_ELEMENT_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_ELEMENT))
/**
 * VISU_IS_ELEMENT_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuElementClass class.
 */
#define VISU_IS_ELEMENT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_ELEMENT))
/**
 * VISU_ELEMENT_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_ELEMENT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_ELEMENT, VisuElementClass))

typedef struct _VisuElementClass VisuElementClass;
typedef struct _VisuElement VisuElement;

/**
 * visu_element_get_type:
 *
 * This method returns the type of #VisuElement, use VISU_TYPE_ELEMENT instead.
 *
 * Returns: the type of #VisuElement.
 */
GType visu_element_get_type(void);

struct _VisuElementClass
{
  VisuObjectClass parent;
};

struct _VisuElement
{
  VisuObject parent;

  /* Name of the key used in the hashtable to find
     this element. The int variable is the number
     of this type. */
  char* name;
  /* An integer unique for each VisuElement, it is
     used as a name for the opengl material associated
     with it. */
  int typeNumber;

  /* If the element should appear in the display list of elements or
     if this element is an internal substitute. */
  gboolean physical;

  gboolean _rendered, _maskable, _colorizable;
};

const GList *visu_element_getAllElements(void);
VisuElement *visu_element_retrieveFromName(const gchar *name, gboolean *nw);
VisuElement *visu_element_lookup(const gchar *name);

VisuElement *visu_element_new(const char *key);

gboolean visu_element_getRendered(const VisuElement *self);
gboolean visu_element_setRendered(VisuElement *self, gboolean val);

gboolean visu_element_getMaskable(const VisuElement *self);
gboolean visu_element_setMaskable(VisuElement *self, gboolean val);

gboolean visu_element_getColorizable(const VisuElement *self);
gboolean visu_element_setColorizable(VisuElement *self, gboolean val);


const gchar* visu_element_getName(const VisuElement *ele);
gboolean visu_element_getPhysical(VisuElement *ele);

void visu_element_pool_finalize(void);

G_END_DECLS

#endif
