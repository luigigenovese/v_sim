#!/usr/bin/python

def dump(scene):
  scene.dump(v_sim.Dump.png_getStatic(), "out.win.png", 600, 600, None, None)
  v_sim.UiMainClass.getDefaultRendering().setCurrent(True)
  v_sim.UiMainClass.getCurrentPanel().quit(True)

def clickAt(inter, view, x, y, button = 1, shiftMod = False, controlMod = False):
  ev = v_sim.SimplifiedEvents()
  ev.x = x
  ev.y = y
  ev.button = button
  ev.buttonType = v_sim.ButtonActionId.PRESS
  inter.handleEvent(view, ev)
  ev.button = button
  ev.shiftMod = shiftMod
  ev.controlMod = controlMod
  ev.buttonType = v_sim.ButtonActionId.RELEASE
  inter.handleEvent(view, ev)

def test(scene):
  marks = scene.getMarks()
  view = scene.getGlView()
  inter = marks.get_property("interactive")
  inter.highlight(23)
  clickAt(inter, view, 420, 429, 3, shiftMod = True)
  clickAt(inter, view, 351, 392, 3)
  scene.getData().reload(None)
  GLib.idle_add(dump, scene)

from gi.repository import GLib
  
scene = v_sim.UiMainClass.getDefaultRendering().getGlScene()
GLib.idle_add(test, scene)
