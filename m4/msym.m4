AC_DEFUN([AC_CHECK_MSYM],
[
  AC_MSG_CHECKING([for libmsym support])
  AC_ARG_WITH([msym],
            [AS_HELP_STRING([--with-msym=ARG],[MSYM directory (with include and lib subdirs)])],
            [MSYM_PATH=$withval], 
            [MSYM_PATH="no"])

  AC_ARG_WITH([msym_inc],
            [AS_HELP_STRING([--with-msym-inc=ARG],[specific MSYM include directive])],
            [MSYM_INC=$withval], 
            [MSYM_INC=""])

  AC_ARG_WITH([msym_lib],
            [AS_HELP_STRING([--with-msym-lib=ARG],[specific MSYM library directive])],
            [MSYM_LIB=$withval], 
            [MSYM_LIB=""])
  AC_MSG_RESULT($MSYM_PATH)

  ac_msym="no"
  if test "z$MSYM_PATH" != "zno" ; then
    AS_IF([test "z$MSYM_PATH" = "zyes"],[MSYM_PATH="/usr"])
    AS_IF([test "z$MSYM_LIB" = "z"],[MSYM_LIB="-L$MSYM_PATH/lib -lmsym"])
    AS_IF([test "z$MSYM_INC" = "z"],[MSYM_INC="-I$MSYM_PATH/include"])
    ac_msym="yes"
  fi
  if test "z$MSYM_PATH" == "zbuiltin" ; then
    ac_msym="builtin"
    AC_PATH_PROG([CMAKE], [cmake], [ac_msym="no"])
  fi

  dnl Builtin case
  if test "z$ac_msym" == "zbuiltin" ; then
    MSYM_CPPFLAGS="-I\$(srcdir)/libmsym/src -Ibuiltin"
    MSYM_LIBS="builtin/libmsym.a -lm"
  fi

  dnl First thing is to test the header files.
  if test "z$ac_msym" == "zyes" ; then
    CPPFLAGS_SVG=$CPPFLAGS
    CPPFLAGS="$CPPFLAGS $MSYM_INC"
    AC_CHECK_HEADER([msym.h],
                    [MSYM_CPPFLAGS="$MSYM_INC"],
                    [ac_msym="no"])
    CPPFLAGS=$CPPFLAGS_SVG
  fi
    
  dnl Look for the context in libmsym.
  if test "z$ac_msym" == "zyes" ; then
    AC_LANG_PUSH([C])
    LIBS_SVG=$LIBS
    LIBS="$LIBS $MSYM_LIB"
    AC_CHECK_LIB(msym, msymCreateContext,
                 [MSYM_LIBS="$MSYM_LIB -lm"],
                 [ac_msym="no"], [-lm])
    LIBS=$LIBS_SVG
    AC_LANG_POP([C])
  fi
])
