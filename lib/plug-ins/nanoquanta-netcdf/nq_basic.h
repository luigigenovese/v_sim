/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef NQ_BASIC_H
#define NQ_BASIC_H

#include <glib.h>
#include <netcdf.h>

#define NQ_ERROR nqError_quark()
GQuark nqError_quark(void);
enum
  {
    NQ_ERROR_FILE_OPEN,     /* Error when opening. */
    NQ_ERROR_FILE_UNKNOWN,  /* Wrong or unknown headers. */
    NQ_ERROR_FILE_HEADER,   /* Wrong header value. */
    NQ_ERROR_FILE_FORMAT    /* Wrong syntax (missing variables... */
  };

/**
 * nqOpen_netcdfFile:
 * @filename: a path to the file to load ;
 * @netcdfId: a pointer to store the id returned by netcdf ;
 * @error: a pointer to store possible error.
 *
 * Open a file supposed to be a NETCDF file following the Nanoquanta
 * specifications. The @netcdfId argument will store the integer id used by
 * netcdf in future calls if the given @filename has a right header.
 * 
 * Returns: TRUE if the file is a valid NETCDF file, if FALSE, no file
 *          is opened.
 */
gboolean nqOpen_netcdfFile(const char* filename, int *netcdfId, GError **error);

/**
 * nqClose_netcdfFile:
 * @netcdfId: a netcdf identifier ;
 *
 * Close the file that is currently opened by Netcdf.
 *
 * Returns: TRUE if the file is succesfully closed.
 */
gboolean nqClose_netcdfFile(int netcdfId);

/**
 * nqGetDim:
 * @netcdfId: a netcdf identifier ;
 * @error: a location to store an error (target should NULL on enter) ;
 * @name: the name of the dimension to get the value from ;
 * @varId: a location to store the id used to identify this dimension ;
 * @value: a location to store the returned value.
 *
 * Inquire the given @netcdfId file to read the value of the given dimension.
 *
 * Returns: TRUE if the dimension exists and the value is readable.
 */
gboolean nqGetDim(int netcdfId, GError **error, char *name, int *varId, size_t *value);
/**
 * nqCheckVar:
 * @netcdfId: a netcdf identifier ;
 * @error: a location to store an error (target should NULL on enter) ;
 * @name: the name of the value to check the definition of ;
 * @varId: a location to store the id used to identify this variable ;
 * @ncType: the supposed type of the variable ;
 * @nbDims: the supposed number of dimenions of the variable ;
 * @nbEleDims: an array of size @nbDims with the supposed size of each
 * dimenion;
 *
 * Inquire the given @netcdfId file to read the variable definition
 * and check that it matches with the given arguments.
 *
 * Returns: TRUE if the variable exists and match the given definition.
 */
gboolean nqCheckVar(int netcdfId, GError **error, char *name, int *varId,
		    nc_type ncType, int nbDims, size_t *nbEleDims);


#endif
