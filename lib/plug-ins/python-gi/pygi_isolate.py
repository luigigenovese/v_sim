#!/usr/bin/env python
from gi.repository import v_sim

# Load the file to test.
data=v_sim.Data.new()
data.addFile("diff.ascii",0,None)
v_sim.visuBasicLoad_dataFromFile(data,None,0)

# Parse the configuration files.
v_sim.visuBasicParse_configFiles()

def prox(data, c1, c2, rad):
  r1 = data.convertXYZToReduced(c1)
  r2 = data.convertXYZToReduced(c2)
  for dx in range(-1, 2):
    for dy in range(-1, 2):
      for dz in range(-1, 2):
        r2_trans = v_sim.Coord.__new__(v_sim.Coord)
	r2_trans.x = r2.x + dx
	r2_trans.y = r2.y + dy
	r2_trans.z = r2.z + dz
	c2_trans = data.convertReducedToXYZ(r2_trans)
	dist2 = (c1.x-c2_trans.x)**2 + (c1.y-c2_trans.y)**2 + (c1.z-c2_trans.z)**2
	if dist2 <= (rad) ** 2:
	  return True
  return False

##RAD = input('Choose a radius: ')
##NODE = input('Choose a node id: ')
RAD = 5.
NODE = 216
print "All nodes further than ",RAD," from node ",NODE," will be hidden."

coord0 = data.getNodeCoordinates(data.getNodeFromNumber(NODE))

dataIter = data.iterNew()
data.iterStart(dataIter)
while (dataIter.node is not None):
	if prox(data, coord0, data.getNodeCoordinates(dataIter.node), RAD):
		print "Node ",dataIter.node.number," is visible"
		dataIter.node.setVisibility(True)
	else:
		print "Node ",dataIter.node.number," is hidden"
		dataIter.node.setVisibility(False)
	data.iterNext(dataIter)

#----------------------DUMP----------------------#
print '\n\t---DUMP dans du resultat dans diffOut.ascii---\n'

outFilename = "diffOut.png"
##outFilename = "diffOut.ascii"

dumps = v_sim.visuDumpGet_allModules()
for dump in dumps:
	if dump.fileType.match(outFilename):
		if dump.bitmap:
			ctx = v_sim.PixmapContext.new(450, 450)
			v_sim.openGLInit_context();

			view = data.getOpenGLView()
			view.camera.length0 = data.getBoxLengths()[0];

			v_sim.openGLModelize(view.camera);
			data.setSizeOfView(450, 450)
			v_sim.openGLProject(view.window, view.camera,
					    data.getBoxLengths()[1])

			v_sim.visuExtensions_rebuildAllLists(data)
			v_sim.openGL_reDraw(None, data)

			image = v_sim.visuOpenGLGet_pixmapData(450, 450, True)
		else:
			image = None

		# The exportation routine.
		dump.callWriteFunc(outFilename, 450, 450, data, image, None, None)
		
		if dump.bitmap:
			ctx.free()
		break
