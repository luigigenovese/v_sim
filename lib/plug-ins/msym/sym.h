/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_MOL_SYMMETRY_H
#define VISU_MOL_SYMMETRY_H

#include <glib.h>
#include <glib-object.h>

#include <visu_tools.h>
#include <visu_data.h>
#include <extraFunctions/plane.h>

/***************/
/* Public part */
/***************/

/**
 * VISU_TYPE_MOL_SYMMETRY:
 *
 * return the type of #VisuMolSymmetry.
 */
#define VISU_TYPE_MOL_SYMMETRY	     (visu_mol_symmetry_get_type ())
/**
 * VISU_MOL_SYMMETRY:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuMolSymmetry type.
 */
#define VISU_MOL_SYMMETRY(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_MOL_SYMMETRY, VisuMolSymmetry))
/**
 * VISU_MOL_SYMMETRY_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuMolSymmetryClass.
 */
#define VISU_MOL_SYMMETRY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_MOL_SYMMETRY, VisuMolSymmetryClass))
/**
 * VISU_IS_MOL_SYMMETRY_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuMolSymmetry object.
 */
#define VISU_IS_MOL_SYMMETRY_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_MOL_SYMMETRY))
/**
 * VISU_IS_MOL_SYMMETRY_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuMolSymmetryClass class.
 */
#define VISU_IS_MOL_SYMMETRY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_MOL_SYMMETRY))
/**
 * VISU_MOL_SYMMETRY_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_MOL_SYMMETRY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_MOL_SYMMETRY, VisuMolSymmetryClass))


/**
 * VisuMolSymmetryPrivate:
 * 
 * Private data for #VisuMolSymmetry objects.
 */
typedef struct _VisuMolSymmetryPrivate VisuMolSymmetryPrivate;

/**
 * VisuMolSymmetry:
 * 
 * Common name to refer to a #_VisuMolSymmetry.
 */
typedef struct _VisuMolSymmetry VisuMolSymmetry;
struct _VisuMolSymmetry
{
  VisuObject parent;

  VisuMolSymmetryPrivate *priv;
};

/**
 * VisuMolSymmetryClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuMolSymmetryClass.
 */
typedef struct _VisuMolSymmetryClass VisuMolSymmetryClass;
struct _VisuMolSymmetryClass
{
  VisuObjectClass parent;
};

/**
 * visu_mol_symmetry_get_type:
 *
 * This method returns the type of #VisuMolSymmetry, use
 * VISU_TYPE_MOL_SYMMETRY instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuMolSymmetry.
 */
GType visu_mol_symmetry_get_type(void);

VisuMolSymmetry* visu_mol_symmetry_new();

gboolean visu_mol_symmetry_setData(VisuMolSymmetry *symmetry, VisuData *data);
gboolean visu_mol_symmetry_setNodes(VisuMolSymmetry *symmetry, GArray *nodes);
gboolean visu_mol_symmetry_lock(VisuMolSymmetry *symmetry, gboolean status);


/**
 * VISU_TYPE_MOL_SYMMETRY_OP:
 *
 * return the type of #VisuMolSymmetryOp.
 */
#define VISU_TYPE_MOL_SYMMETRY_OP	     (visu_mol_symmetry_op_get_type ())
/**
 * VISU_MOL_SYMMETRY_OP:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuMolSymmetryOp type.
 */
#define VISU_MOL_SYMMETRY_OP(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_MOL_SYMMETRY_OP, VisuMolSymmetryOp))
/**
 * VISU_MOL_SYMMETRY_OP_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuMolSymmetryOpClass.
 */
#define VISU_MOL_SYMMETRY_OP_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_MOL_SYMMETRY_OP, VisuMolSymmetryOpClass))
/**
 * VISU_IS_MOL_SYMMETRY_OP_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuMolSymmetryOp object.
 */
#define VISU_IS_MOL_SYMMETRY_OP_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_MOL_SYMMETRY_OP))
/**
 * VISU_IS_MOL_SYMMETRY_OP_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuMolSymmetryOpClass class.
 */
#define VISU_IS_MOL_SYMMETRY_OP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_MOL_SYMMETRY_OP))
/**
 * VISU_MOL_SYMMETRY_OP_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_MOL_SYMMETRY_OP_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_MOL_SYMMETRY_OP, VisuMolSymmetryOpClass))

typedef struct _VisuMolSymmetryOpPrivate VisuMolSymmetryOpPrivate;

/**
 * VisuMolSymmetryOp:
 * 
 * Common name to refer to a #_VisuMolSymmetryOp.
 */
typedef struct _VisuMolSymmetryOp VisuMolSymmetryOp;
struct _VisuMolSymmetryOp
{
  VisuObject parent;

  VisuMolSymmetryOpPrivate *priv;
};

/**
 * VisuMolSymmetryOpClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuMolSymmetryOpClass.
 */
typedef struct _VisuMolSymmetryOpClass VisuMolSymmetryOpClass;
struct _VisuMolSymmetryOpClass
{
  VisuObjectClass parent;
};
GType visu_mol_symmetry_op_get_type(void);

typedef enum _VisuMolSymmetryOpTypes
  {
    OPERATION_IDENTITY,
    OPERATION_ROTATION,
    OPERATION_ROTATION_REFLEXION,
    OPERATION_REFLEXION,
    OPERATION_INVERSION
  } VisuMolSymmetryOpTypes;
typedef enum _VisuMolSymmetryOpOrientations
  {
    ORIENTATION_NONE,
    ORIENTATION_HORIZONTAL,
    ORIENTATION_VERTICAL,
    ORIENTATION_DIHEDRAL
  } VisuMolSymmetryOpOrientations;

VisuMolSymmetryOpTypes visu_mol_symmetry_op_getType(const VisuMolSymmetryOp *op);
VisuPlane* visu_mol_symmetry_op_getReflexion(const VisuMolSymmetryOp *op);
gboolean visu_mol_symmetry_op_getRotation(const VisuMolSymmetryOp *op,
                                          float *angle, float axis[3],
                                          float origin[3]);

#endif
